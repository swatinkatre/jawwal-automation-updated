*** Settings ***
Library           FakerLibrary
Library           DateTime
Library           ../CustomLibrary/UpdateExcelSheet.py
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library

*** Keywords ***
OpenAccHistoryFromSubsPage
    ClickMoreCommand
    Wait And Click Element    ${OpenAccountHistoryButtonSubs}
    \    ${FilePath}

OpenCallDetailStatement
    ClickMoreCommand
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.CallDetailStatement.Button

SubmitCorrectionOCCWithoutApproval
    [Arguments]    ${Name}    ${Amount}
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer4
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=jwl_name_d
    send text to element    id=jwl_name_i    ${Name}
    Wait And Click Element    id=jwl_amount
    send text to element    id=jwl_amount_i    ${Amount}
    Wait And Click Element    id=jwl_startdateofcorrection
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    id=DateInput    ${date1}
    Wait And Click Element    id=jwl_noofcycle
    send text to element    id=jwl_noofcycle_i    1
    Wait And Click Element    id=jwl_type
    Wait And Click Element    Xpath=.//*[@id='jwl_type_i']/option[2]
    Wait And Click Element    id=jwl_typereason
    Wait And Click Element    Xpath=.//*[@id='jwl_typereason_i']/option[2]
    Wait And Click Element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    Unselect Frame
    wait for element    //li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']
    click on element    id=jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button
    Comment    wait for element    //li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']
    Comment    Sleep    15s
    Comment    Wait And Click Element    Xpath=//li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']/span
    Comment    Sleep    25s
    Comment    Confirm Action
    #Element Should Contain    Xpath=.//*[@id='header_statuscode']/div[1]/span    Status Reason Submitted Successfully

SubmitCorrectionOCCWithApproval
    [Arguments]    ${Amount}    ${screenshot}
    wait for element    id=jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button    40    1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ##Enetring Data##
    Screenshots    ${screenshot}    CorrectionOCC Page
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    wait for element    id=jwl_name_cl
    Comment    Wait And Click Element    id=jwl_name_d
    Comment    send text to element    id=jwl_name_i    ${Name}
    Wait And Click Element    id=jwl_amount
    Comment    Wait And Click Element    id=jwl_amount_i
    Run Keyword And Ignore Error    send text to element    id=jwl_amount    ${Amount}
    Run Keyword And Ignore Error    Wait And Click Element    id=jwl_startdateofcorrection
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    Run Keyword And Ignore Error    send text to element    id=DateInput    ${date1}
    Run Keyword And Ignore Error    Wait And Click Element    id=jwl_noofcycle
    Run Keyword And Ignore Error    send text to element    id=jwl_noofcycle_i    1
    Run Keyword And Ignore Error    Wait And Click Element    id=jwl_type
    Run Keyword And Ignore Error    Wait And Click Element    Xpath=.//*[@id='jwl_type_i']/option[2]
    Run Keyword And Ignore Error    Wait And Click Element    id=jwl_typereason
    Run Keyword And Ignore Error    Wait And Click Element    Xpath=.//*[@id='jwl_typereason_i']/option[2]
    ##saving and submitting##
    Run Keyword And Ignore Error    Wait And Click Element    id=savefooter_statuscontrol
    Run Keyword And Ignore Error    wait for element    id=savefooter_statuscontrol
    Run Keyword And Ignore Error    Unselect Frame
    Run Keyword And Ignore Error    Sleep    5s
    Run Keyword And Ignore Error    Wait And Click Element    Xpath=//li[@id='jwl_bi_applycorrectionocc|NoRelationship|Form|jwl.jwl_bi_applycorrectionocc.Submit.Button']/span
    Comment    Choose Ok On Next Confirmation
    Comment    sleep    5s
    Comment    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Log To Console    entered all the values
    Sleep    20s
    ${OCCSubSumitmessage}    Confirm Action
    Run Keyword If    '${OCCSubSumitmessage}' == 'BI Apply Correction OCC Approval flow started!'    Log    OCC approval flow started successfully
    ...    ELSE    Log    OCC approval flow not started successfully
    Run Keyword If    '${OCCSubSumitmessage}' == 'BI Apply Correction OCC Approval flow started!'    Add Result    PASS    UC5.28    Correction OCC flow should be started successfully.    OCC approval flow started successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC5.28    Correction OCC flow should be started successfully.    OCC approval flow not started and error message is : ${OCCSubSumitmessage}
    ...    Verification completed
    sleep    1m
    ##Approval Level1##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    Xpath=//table[@id='gridBodyTable']//a[@title='Open Approval Step 1']    60    1
    Double Click Element    Xpath=//table[@id='gridBodyTable']//a[@title='Open Approval Step 1']
    Unselect Frame
    wait for element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span    60    1
    Screenshots    ${screenshot}    Approval Level1 page
    Focus    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    click on element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    Comment    Choose Ok On Next Confirmation
    Comment    sleep    4s
    Comment    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    10s
    ${OCCapproval1message}    Confirm Action
    log    ${OCCapproval1message}
    Run Keyword If    '${OCCapproval1message}' == 'Approve request is done'    Log    OCC approval-1 completed successfully.
    ...    ELSE    Log    OCC approval-1 not completed successfully.
    Run Keyword If    '${OCCapproval1message}' == 'Approve request is done'    Add Result    PASS    UC5.28    OCC approval-1 should be completed successfully.    OCC approval-1 completed successfully.Success message is : ${OCCapproval1message}
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC5.28    Balance Refill BI approval flow should be started successfully.    OCC approval-1 not completed successfully and error message is : ${OCCapproval1message}
    ...    Verification completed
    wait for element    id=jwl_approvalstep|NoRelationship|Form|jwl.ApplicationRibbon.jwl_approvalstep.InboundCall.Button    40    1
    Wait And Click Element    id=closeButton
    ##Approval Level2##
    wait for element    id=jwl_bi_applycorrectionocc|NoRelationship|Form|Mscrm.Form.jwl_bi_applycorrectionocc.Save    60    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    5s
    wait for element    Xpath=//table[@id='gridBodyTable']//a[@title='Open Approval Step 2']
    Double Click Element    Xpath=//table[@id='gridBodyTable']//a[@title='Open Approval Step 2']//span[contains(text(),'Approval Step 2')]
    Unselect Frame
    wait for element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span    60    1
    Screenshots    ${screenshot}    Approval Level2 page
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    5s
    Wait And Click Element    xpath=//li[@id='jwl_approvalstep|NoRelationship|Form|jwl.jwl_approvalstep.ApprovalControllerApprove.Button']/span
    Comment    Choose Ok On Next Confirmation
    Comment    Sleep    5s
    Comment    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    10s
    ${OCCapproval1message}    Confirm Action
    log    ${OCCapproval1message}
    Run Keyword If    '${OCCapproval1message}' == 'Approve request is done'    Log    OCC approval-1 completed successfully.
    ...    ELSE    Log    OCC approval-1 not completed successfully.
    Run Keyword If    '${OCCapproval1message}' == 'Approve request is done'    Add Result    PASS    UC5.28    OCC approval-2 should be completed successfully.    OCC approval-2 completed successfully.Success message is : Approve request is done
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC5.28    OCC approval-2 should be completed successfully.    OCC approval-2 not completed successfully and error message is : ${OCCapproval1message}
    ...    Verification completed
    wait for element    id=jwl_approvalstep|NoRelationship|Form|jwl.ApplicationRibbon.jwl_approvalstep.InboundCall.Button    40    1
    Wait And Click Element    id=closeButton
    ##Submit Verification##
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='header_statuscode']    40s
    ${StatusReasonOCC}    Get Text    xpath=//div[@id="header_statuscode"]//div//span
    ${match}    Set Variable    ${StatusReasonOCC}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Apply OCC correction is succesfull :- ${StatusReasonOCC}
    ...    ELSE    Log    Apply OCC correction is not succesfull :- ${StatusReasonOCC}
    Screenshots    ${screenshot}    CorrectionOCC Page after submission
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC5.28    Correction OCC BI should be successfull    Correction OCC BI is successfull header status is : ${StatusReasonOCC}
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC5.28    Correction OCC BI should be successfull    Correction OCC BI is not successfull header status is : ${StatusReasonOCC}
    ...    Verification completed
    Unselect Frame

ModifySubscription
    [Arguments]    ${service}    ${Screenshotpath}
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Modify Subscription Page
    Comment    Sleep    25s
    wait for element    //div[@id='existingOfferingsGridBody']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    5s
    ${servicetobeadded}    Get Text    //table[@id='configDataGridBody']//tbody//tr[1]//td[1]//div
    log    ${servicetobeadded}
    Wait And Click Element    //table[@id='configDataGridBody']//tbody//tr[1]
    Comment    sleep    5s
    Comment    Wait And Click Element    //div[@id='productStatusChangeWidget']//button[contains(text(),'Done')]
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Sleep    10s
    Double Click Element    //div[@id='existingproductsTitle']//span[contains(text(),'Add new Products')]
    Sleep    15s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //table[@id='configDataGridBody']//tbody//tr//td[contains(string(),'${service}')]
    Comment    ${servicetobeadded}    Get Text    //table[@id='configDataGridBody']//tbody//tr//td[contains(string(),'${service}')]
    Comment    ${servicetobeadded}    Get Text    //table[@id='configDataGridBody']//tbody//tr[1]//td[1]//div
    Comment    log    ${servicetobeadded}
    Comment    Wait And Click Element    //table[@id='configDataGridBody']//tbody//tr[1]
    Wait And Click Element    //div[@id='newProductsArea']//span[contains(text(),'ADD TO BASKET')]
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${countforloop}    Evaluate    ${countofrows}+3
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${offerfromorderbasket}    Get text    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]//div//span[${INDEX}]
    \    ${StatusInOrder}    Run keyword if    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'    Get Text    //div[@id='orderBasket']//table//tbody//td[3]//div//span[@title='Deactivation']
    \    Run Keyword If    '${StatusInOrder}'=='Deactivation'    log    Service is deactivated succesfully
    \    ...    ELSE    log    Service is not deactivated succesfully
    \    Exit For Loop If    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'
    ${countofrowsfornewoffer}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    ${countforloop1}    Evaluate    ${countofrowsfornewoffer}+3
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${offerfromorderbasket1}    Get text    //div[@id='orderBasket']//table//tbody//td[1]//div//span[@title='${servicetobeadded}']
    \    Run Keyword If    '${offerfromorderbasket1}'=='${servicetobeadded}'    log    correct offer has been added in order box
    \    ...    ELSE    log    Correct offer is not added, the added offer is :- ${servicetobeadded}
    \    Exit For Loop If    '${offerfromorderbasket1}'=='${servicetobeadded}'
    Screenshots    ${Screenshotpath}    New modified sub added to basket
    Comment    Wait And Click Element    //table[@id='msdp_rbtServicesDataGridBody']//span[text(),'Eschools']
    Comment    Wait And Click Element    //div[@id='msdp_rbtProductsArea']//span[text(),'ADD TO BASKET']
    Comment    Sleep    40s
    Comment    ${countofrows}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    Comment    ${countforloop}    Evaluate    ${countofrows}+3
    Comment    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    Comment    \    ${offerfromorderbasket}    Get text    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]//div//span[${INDEX}]
    Comment    \    ${StatusInOrder}    Run keyword if    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'    Get Text    //div[@id='orderBasket']//table//tbody//td[3]//div//span[@title='Deactivation']
    Comment    \    Run Keyword If    '${StatusInOrder}'=='Deactivation'    log    Service is deactivated succesfully
    ...    ELSE    log    Service is not deactivated succesfully
    Comment    \    Exit For Loop If    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'
    Comment    ${countofrows}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    Comment    ${countforloop}    Evaluate    ${countofrows}+3
    Comment    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    Comment    \    ${offerfromorderbasket}    Get text    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]//div//span[${INDEX}]
    Comment    \    ${StatusInOrder}    Run keyword if    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'    Get Text    //div[@id='orderBasket']//table//tbody//td[3]//div//span[@title='Deactivation']
    Comment    \    Run Keyword If    '${StatusInOrder}'=='Deactivation'    log    Service is deactivated succesfully
    ...    ELSE    log    Service is not deactivated succesfully
    Comment    \    Exit For Loop If    '${DeactServiCeFromWeb}'=='${offerfromorderbasket}'
    Comment    ${countofrowsfornewoffer}    Get Matching Xpath Count    //div[@id='orderBasket']//div[@class='dataGridBodyScroll']//table//tbody//td[1]
    Comment    ${countforloop1}    Evaluate    ${countofrowsfornewoffer}+3
    Comment    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    Comment    \    ${offerfromorderbasket1}    Get text    //div[@id='orderBasket']//table//tbody//td[1]//div//span[@title='${service}']
    Comment    \    Run Keyword If    '${offerfromorderbasket1}'=='${service}'    log    correct offer has been added in order box
    ...    ELSE    log    Correct offer is not added, the added offer is :- ${servicetobeadded}
    Comment    \    Exit For Loop If    '${offerfromorderbasket1}'=='${service}'
    Screenshots    ${Screenshotpath}    Service added in basket
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Configuration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Configuration page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Resources##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Resources page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Addon Expiration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Add On Expiration Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Guarantees##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Guarantee Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Payment##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Payment Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Summary##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='summaryResourcesContainer']//table//tbody//td//input
    ${SubDate}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    //div[@id='summaryResourcesContainer']//table//tbody//td//input    ${SubDate}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    10s
    Screenshots    ${Screenshotpath}    Summary Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    sleep    50s
    ${SumMesg}    Confirm Action
    log    ${SumMesg}
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    Screenshots    ${Screenshotpath}    Status of Modified subscription
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Add/Remove submitted succesfully and status is ${StatusReasonNewSub}
    ...    ELSE    Log    Add/Remove not submitted succesfully and status is ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC7.5    Add/Remove submitted succesfully    Add/Remove submitted succesfully and status is ${StatusReasonNewSub}
    ...    Modification completed
    ...    ELSE    Add Result    FAILED    UC7.5    Add/Remove submitted succesfully    Add/Remove not submitted succesfully and status is ${StatusReasonNewSub}
    ...    Modification completed

OpenNewSubAndSelectProduct
    [Arguments]    ${Offer}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' Order Capture ')]
    sleep    10s
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' New Subscription')]
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=newCustomerSearchButtonGroup
    Wait And Click Element    xpath=//div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'SEARCH')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@class='dataGridWrapper']
    Wait And Click Element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]
    wait and click element    xpath=//span[@class='cmdBarBtnSpan ng-binding'][@ng-click='!rootScopeData.currentStage.isActive || scopeData.planSelection.addBasket()']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=orderBasket
    ${alloffers}    Get Text    xpath=//div[@id='orderBasket']//div[@class='dataGridBodyScroll']//tr[@class='dataGridBodyRow ng-scope']//td[1]//span[@class='lowerCellSection ng-binding']
    log    ${alloffers}
    Run Keyword If    '${alloffers}'=='${Offer}'    Log    Correct offer is added in Basket
    ...    ELSE    log    Offer is not added in bascket. Offer in bascket is = ${alloffers}
    Comment    ${a}    Get Text    xpath=//div[@id='right']//div[@id='orderBasket']//div[@class='dataGridBodyScroll']//tr[@class='dataGridBodyRow ng-scope']//td[1]//span[@class='lowerCellSection ng-binding'][@title='Hala 150']
    Comment    log    ${a}
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    wait and click element    xpath=//div[@id='stageAdvanceActionContainer']/div
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubConfigurationPage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## wait and click on "Save" ##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## wait and click on "Next Stage" ##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubResoursePage
    [Arguments]    ${MSISDN}    ${SIM}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock
    wait for element    //input[@id='txtMSISDN']
    click on element    //input[@id='txtMSISDN']
    send text to element    //input[@id='txtMSISDN']    ${MSISDN}
    Write Excel    ${FilePath}/InputData.xls    1    1    2    ${MSISDN}
    click on element    //input[@id='txtSIM']
    wait for element    //button[@id='btnAssignMSISDN']
    click on element    //button[@id='btnAssignMSISDN']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${MSISDNAssignmessage}    Confirm Action
    log    ${MSISDNAssignmessage}
    Log To Console    ${MSISDNAssignmessage}
    Run Keyword If    '${MSISDNAssignmessage}'=='MSISDN Reserve completed successfully'    Log    MSISDN Reservered successfully
    ...    ELSE    Log    We got error while reserving the MSISDN. Error detail is =${MSISDNAssignmessage}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ### SIM Reservation ###
    wait for element    //input[@id='txtSIM']
    send text to element    //input[@id='txtSIM']    ${SIM}
    Write Excel    ${FilePath}/InputData.xls    1    1    3    ${SIM}
    click on element    //input[@id='txtMSISDN']
    Comment    click on element    //button[@id='btnAssignSIM']
    Comment    sleep    40s
    Comment    ${SIMAssignmessage}    Confirm Action
    Comment    log    ${SIMAssignmessage}
    Comment    Log To Console    ${SIMAssignmessage}
    Comment    Run Keyword If    '${SIMAssignmessage}'=='Sim has reserved successfully'    Log    SIM Reservered successfully
    ...    ELSE    Log    We got error while reserving the SIM. Error detail is =${SIMAssignmessage}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    #### Save changes
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ### Click on "Next Stage"##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubAddOnPage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubUserProfilePage
    [Arguments]    ${User_profile_Language}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    ## Select language in user profile page###
    wait for element    //div[@id='left']//select[@name='language']
    wait and click element    //div[@id='left']//select[@name='language']//option[contains(text(),'${User_profile_Language}')]
    Unselect Frame
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## Save changes##
    Screenshots    ${Screenshotpath}    UserProfilePage
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## Click on "Next Stage"###
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubGuaranteePage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## wait and click on "Save" ##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## wait and click on "Next Stage" ##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubFinancialAndPaymentPage
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## wait and click on "Save" ##
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## wait and click on "Next Stage" ##
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

NewSubSummaryPage
    [Arguments]    ${Summary_Billing_Lan}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    ## Select Billing language in Summary page###
    wait for element    //table[@id='formField_noSubscriptions']//select//option[@label='English']
    wait and click element    //table[@id='formField_noSubscriptions']//select//option[@label='${Summary_Billing_Lan}']
    Unselect Frame
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ## Save changes##
    Screenshots    ${Screenshotpath}    SummaryPage
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    ## Click on "Next Stage"###
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

ChangeOwnership-JoinAccount
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@ng-change='changeCustomerType()']    60    2
    Wait And Click Element    //select[@ng-change='changeCustomerType()']//option[contains(text(),'Individual')]
    Wait And Click Element    id=selectSearchTypesId
    Wait And Click Element    //select[@id='selectSearchTypesId']//option[contains(text(),'National Id')]
    Wait And Click Element    id=param1Id
    send text to element    id=param1Id    TC123123
    Comment    Wait And Click Element    id=param2Id
    Comment    send text to element    id=param2Id    Tufan
    Wait And Click Element    id=btn_searchCustomer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    10s
    Wait And Click Element    //div[@id='existingOfferingsGridBody']//input[@type='checkbox']
    Sleep    10s
    Wait Until Page Contains Element    //div[@id='processControlCollapsibleArea']//span[contains(text(),'TC123123')]
    ${msisdn}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${msisdn}
    ${name}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText']
    Log    ${name}
    Comment    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Unselect Frame
    ##Open Invoices##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@class='wizard-body ng-scope']//p[starts-with(.,'Subscription')]    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Guarantee##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=existingproductsTitle    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Payments#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##User Profile##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@name='language']
    Wait And Click Element    //select[@name='language']//option[contains(text(),'English')]
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait for element    //div[@class='wizard-body ng-scope']
    Wait And Click Element    //div[@class='wizard-body ng-scope']//select[contains(@ng-model,'selectReasons')]
    Wait And Click Element    //div[@class='wizard-body ng-scope']//option[contains(text(),'Join account')]
    sleep    5s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    #Submit Button#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    16s
    Confirm Action
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=header_jwl_status    40    1
    Sleep    5s
    ${a}    Get Text    //div[@id='header_jwl_status']/div[1]/span
    Run Keyword If    '${a}' == \ 'Submitted'    log    Order Submitted successfully
    Unselect Frame
    wait for element    id=crmRibbonManager    60    2
    wait for element    //span[@id='TabSearch']//input[@id='search']
    send text to element    //span[@id='TabSearch']//input[@id='search']    ${msisdn}
    Wait And Click Element    //span[@id='TabSearch']//img[@id='findCriteriaImg']
    wait for element    id=contentIFrame0    40    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='contentResult']    40    2
    Wait And Click Element    //div[@id='contentResult']//div[starts-with(@id,'Record_')]    40    2
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select Frame    id=contentIFrame1
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    ${n}    Get Text    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    log    ${n}
    Pass Execution If    '${name}' == '${n}'    Change ownership successfull
    Unselect Frame

ReactivateTemporaryDeactivatedSubscription
    [Arguments]    ${CUSTOMER_Id}    ${Screenshotpath}    ${Subscription_Id}
    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.SaveAndClose    60    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    wait for element    //table[@class='formSectionTable']//textarea[@name='notesTextArea']
    send text to element    //table[@class='formSectionTable']//textarea[@name='notesTextArea']    testing123
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Reactivation Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    30s
    Comment    Confirm Action
    Unselect Frame
    #verification of order submitted successfully#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}'=='Submitted'    Log    Order submitted successfully
    ...    ELSE    Log    Order not submitted successfully
    Run Keyword If    '${status}'=='Submitted'    Add Result    PASS    UC7.23_Reconnect after temporary deactivation    Order should be submitted successfully    Order submitted successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.23_Reconnect after temporary deactivation    UC7.23_Reconnect after temporary deactivation    Order should be submitted successfully
    ...    Order not submitted successfully    Verification completed
    Sleep    20s
    #verification of subscription status change to Active
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']    60    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //h2[@id='subscriptions_header_h2']
    Focus    id=subscriptions_header_image_div
    wait for element    id=subscriptions_header_image_div
    click on element    id=subscriptions_header_image_div
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[5]
    \    Log    ${a}
    \    ${b}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    log    ${b}
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Suspend'    Exit For Loop
    ##### For loop ends ####
    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    log    Contract ${Subscription_Id} is reactivated sucessfully
    ...    ELSE    Log    Contract is not reactivated sucessfully
    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Add Result    PASS    UC7.23_Reconnect after temporary deactivation    Contract should be reactivated successfully    Contract is reactivated sucessfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.23_Reconnect after temporary deactivation    Contract should be reactivated successfully    Contract is not reactivated.
    ...    Verification completed
    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Suspend'    Add Result    FAILD    UC7.23_Reconnect after temporary deactivation    Contract should be reactivated successfully    Contract is not reactivated.
    ...    Verification completed
    Screenshots    ${Screenshotpath}    Subscription status
    Unselect Frame

GetContractID
    [Arguments]    ${CUSTOMER_ID}    ${SIM}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to get contract id for customer ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${SIMFromWeb}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[6]
    \    log    ${SIMFromWeb}
    \    ${Contract_ID}    Run Keyword If    '${SIM}'=='${SIMFromWeb}'    get text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${Contract_ID}
    \    Exit For Loop If    '${SIM}'=='${SIMFromWeb}'
    ##### For loop ends ####
    Unselect Frame
    Write Excel    ${FilePath}/InputData.xls    1    1    6    ${Contract_ID}

VerifyOCCSubmit
    sleep    40s
    ${OCCSubSumitmessage}    Confirm Action
    log    ${OCCSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonOCC}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonOCC}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Apply OCC correction is succesfull :-    ${StatusReasonOCC}
    ...    ELSE    Log    Apply OCC correction is not succesfull ${StatusReasonOCC}

VerifySubmitBIOrderAndGetConID
    [Arguments]    ${CUSTOMER_ID}    ${SIM}
    Sleep    40s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    ...    ELSE    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    GetContractID    ${CUSTOMER_ID}    ${SIM}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${NewSubSumitmessage}

VerifyBIOrderSubmit
    [Arguments]    ${Successmsg}    ${Failuremsg}
    Sleep    40s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    ${Displaymsginlogpass}    Catenate    ${Successmsg}    ${StatusReasonNewSub}
    ${Displaymsginlogfail}    Catenate    ${Failuremsg}    ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    ${Displaymsginlogpass}
    ...    ELSE    Log    ${Displaymsginlogfail}
    [Return]    ${NewSubSumitmessage}

VerifyTemDeaSubStatus
    [Arguments]    ${CUSTOMER_ID}    ${Subscription_Id}
    Wait And Click Element    id=HomeTabLink
    Wait And Click Element    id=CS
    Wait And Click Element    id=nav_contacts
    Comment    GoToIndividualCustomersList
    OpenIndCustomerWithCustID    ${CUSTOMER_ID}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to get contract id for customer ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    ${StatusFromWeb}    Run Keyword If    '${c}'=='${d}'    get text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[3]
    \    log    ${StatusFromWeb}
    \    Exit For Loop If    '${c}'=='${d}'
    \    Run Keyword If    '${StatusFromWeb}'=='Suspend'    log    Contract ${Subscription_Id} is temporary deactivated sucessfully
    \    ...    ELSE    Contract is not temporarily deactivated sucessfylly
    ##### For loop ends ####
    Unselect Frame

SubmitAndVerifyTempDeaOrder
    [Arguments]    ${CUSTOMER_ID}    ${Subscription_Id}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select Frame    id=contentIFrame0
    wait for element    //div[@id='stageAdvanceActionContainer']
    click on element    //div[@id='stageAdvanceActionContainer']
    Unselect Frame
    wait for element    //li[@id='jwl_bi_temporarydeactivation|NoRelationship|Form|jwl.jwl_bi_temporarydeactivation.Submit.Button']
    sleep    2s
    Screenshots    ${Screenshotpath}    Submit Page
    click on element    //li[@id='jwl_bi_temporarydeactivation|NoRelationship|Form|jwl.jwl_bi_temporarydeactivation.Submit.Button']
    Sleep    30s
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    log    The message after order submit is :- ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    id=header_statuscode    40s
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id='header_statuscode']//div//span
    log    ${StatusReasonNewSub}
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Screenshots    ${Screenshotpath}    After submission
    Unselect Frame
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC7.17_Temporary Disconnect Subscription    Contract should be temporary deactivated successfully    Contract ${Subscription_Id} is temporary deactivated sucessfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.17_Temporary Disconnect Subscription    Contract should be temporary deactivated successfully    Contract ${Subscription_Id} is not temporary deactivated.Failure message is : ${NewSubSumitmessage}
    ...    Verification completed
    Run Keyword If    '${RETURNVALUE}'=='True'    VerifyTemDeaSubStatus    ${CUSTOMER_ID}    ${Subscription_Id}    ${ScreenshotpathW2UAT_UC15}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${NewSubSumitmessage}

VerifyUpdateAcc
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    wait for element    id=containerLoadingProgress
    wait for element    id=processControlScrollPane
    wait for element    id=processSteps_3
    wait for element    //div[@id='header_process_jwl_subscriptionid3']
    Focus    //div[@id='header_process_jwl_subscriptionid3']
    Double Click Element    //div[@id='header_process_jwl_subscriptionid3']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Customer360Page
    wait for element    id=subscriptionprofiles_header_h2
    click on element    id=subscriptionprofiles_header_h2
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField')]
    click on element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField')]
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='userprofilegrid_divDataArea']
    wait for element    //table[@id='gridBodyTable']
    ${Email_Text}    Get Text    //table[@id='gridBodyTable']//tbody//tr//td[5]
    log    ${Email_Text}
    Run Keyword If    '${UP_Email}'=='${Email_Text}'    log    PASS
    ...    ELSE    log    FAIL
    Run Keyword If    '${UP_Email}'=='${Email_Text}'    Add Result    PASS    1.3    Account should be updated successfully    Message same as expected
    ...    Verification Complete    FAILED    1.3    Error message indicating that Account cannot be updated successfully    Error Message same as expected    Verification Failed
    Unselect Frame

GetInvoiceVerification
    [Arguments]    ${INDEX}    ${Amount_for_verification}
    ${Open_Amount_Invoice}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[5]
    log    ${Open_Amount_Invoice}
    Comment    ${Document_Amount}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[4]
    Run Keyword If    '${Amount_for_verification}'=='${Open_Amount_Invoice}'    log    Invoice adjustment done
    ...    ELSE    log    Invoice adjustment is not correctly performed
    Comment    ${new_open_amt}    Evaluate    ${Open_Amount_Invoice}-1
    Comment    Run Keyword If    '${new_open_amt}'=='${Open_Amount_Invoice}'    log    Invoice adjustment done

InvoiceAdjustmentAndVerify
    [Arguments]    ${MSISDN}    ${Ad_Type}    ${Ad_Reason}    ${Ad_Amount}    ${Customer_Name}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_invoicegrid
    Screenshots    ${Screenshotpath}    Invoice Adjustment page
    wait for element    //div[@id='grid']
    wait for element    //input[@id='searchSubs']
    click on element    //input[@id='searchSubs']
    send text to element    //input[@id='searchSubs']    ${MSISDN}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Click Element    //button[@id='searchbtn']
    #### Get count of the webtable ###
    sleep    2s
    Wait Until Keyword Succeeds    30s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Select Subscription
    Comment    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[6]
    Comment    log    ${functionBlockCount}
    Comment    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    Comment    : FOR    ${INDEX}    IN RANGE    1    ${count}
    Comment    ${Open_Amount}=    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX} ]//td[6]
    Comment    log    ${Open_Amount}
    Comment    Run Keyword If    '${Open_Amount}'>0    Select Checkbox    //div[@id='grid']//table//tbody//tr//td[1]//input
    Comment    sleep    1s
    Comment    Exit For Loop If    '${Open_Amount}'>0
    Wait And Click Element    //input[@type='checkbox']
    wait for element    //button[@id='applytop']
    Double Click Element    //button[@id='applytop']
    sleep    30s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_adjustmenttype
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_adjustmenttype']/div
    Run Keyword And Ignore Error    Select From List    //select[@id='jwl_adjustmenttype_i']    Debit
    wait for element    id=jwl_adjustmentreason
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_debitadjustmentreason']/div
    Run Keyword And Ignore Error    Select From List    //select[@id='jwl_debitadjustmentreason_i']    Others Debit
    Run Keyword And Ignore Error    wait for element    id=jwl_amount_i
    Run Keyword And Ignore Error    click on element    id=jwl_amount
    Run Keyword And Ignore Error    click on element    id=jwl_amount_i
    Run Keyword And Ignore Error    Input Text    //input[@id='jwl_amount_i']    ${Ad_Amount}
    Comment    ${Amount_for_verification}    Evaluate    ${Open_Amount}-1
    Sleep    30s
    Unselect Frame
    wait for element    //li[@id='jwl_bi_invoiceadjustment|NoRelationship|Form|jwl.jwl_bi_invoiceadjustment.Submit.Button']
    click on element    //li[@id='jwl_bi_invoiceadjustment|NoRelationship|Form|jwl.jwl_bi_invoiceadjustment.Submit.Button']
    sleep    60s
    ${BISubmitMsg}    Confirm Action
    Unselect Frame
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Status of Invoice
    ${status}    Get Text    //div[@id='header_statuscode']/div/span
    Run Keyword If    '${status}' == 'SubmittedSuccessfully'    Add Result    PASS    UC5.8_InvAdj    BI Invoice Adjustment Submitted Successfully    BI Invoice Adjustment is sucessful
    ...    BI Invoice Adjustment is done
    ...    ELSE    Add Result    FAIL    UC5.8_InvAdj    BI Invoice Adjustment Submitted Successfully    BI Invoice Adjustment is not sucessful
    ...    BI Invoice Adjustment is not done due to ${BISubmitMsg}

VerifyInvoiceAdjustment
    [Arguments]    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    FrameSelect
    wait for element    //div[@id='header_jwl_individualcustomerid']//label[@id='Individual Customer']
    Double Click Element    //div[@id='header_jwl_individualcustomerid']//div//span[contains(text(),'${Customer_Name}')]
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    FrameSelect
    sleep    5s
    Run Keyword And Ignore Error    click element    id=tab_13_header_h2
    sleep    2s
    wait for element    //h2[@id='tab_10_header_h2']
    click on element    //h2[@id='tab_10_header_h2']
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Select IFrame    id=WebResource_individual_invoiceGrid
    wait for element    //div[@id='grid']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Invoice_MSISDN}    Get Text    //div[@id='grid']//table//tbody//tr[${INDEX}]//td[2]
    \    Run Keyword If    '${Invoice_MSISDN}'=='${MSISDN}'    GetInvoiceVerification    ${INDEX}    ${Amount_for_verification}
    \    Exit For Loop If    '${Invoice_MSISDN}'=='${MSISDN}'

FreezeSubscription
    [Arguments]    ${Subscription_Id}    ${Notes}
    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //textarea[@name='notesTextArea']    60    2
    send text to element    //textarea[@name='notesTextArea']    ${Notes}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Installmment#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=existingOfferingsGridHeader    60    2
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Comment    #Documents#
    Comment    wait for element    id=contentIFrame0
    Comment    Select IFrame    id=contentIFrame0
    Comment    Comment    Unselect Frame
    Comment    Comment    wait for element    id=contentIFrame1
    Comment    Comment    Select IFrame    id=contentIFrame1
    Comment    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    select Iframe    id=WebResource_processapp
    Comment    wait for element    id=dowloadDocumentsUrlGridHeader    60    2
    Comment    Wait And Click Element    id=stageAdvanceActionContainer
    Comment    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@title="Payment method"]    60    2
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    30s
    ${Submitmessage}    Confirm Action
    Log    Message after submission is : ${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyFreezeSubscriptionStatus    ${Subscription_Id}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyFreezeSubscriptionStatus    ${Subscription_Id}
    ...    ELSE    log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Submitted'    Add Result    PASS    UC7.32    Freeze Subscription order should be submitted successfull    Freeze Subscription order is submitted successfully. Submit message is : ${Submitmessage}
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.32    Freeze Subscription order should be submitted successfull    Freeze Subscription order is not submitted successfully. Submit message is : ${Submitmessage}
    ...    Verification completed
    Run Keyword If    '${status}' == 'Closed'    Add Result    PASS    UC7.32    Freeze Subscription order should be submitted successfull    Freeze Subscription order is submitted successfully. Submit message is : ${Submitmessage}
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.32    Freeze Subscription order should be submitted successfull    Freeze Subscription order is not submitted successfully. Submit message is : ${Submitmessage}
    ...    Verification completed

VerifyFreezeSubscriptionStatus
    [Arguments]    ${Subscription_Id}
    Wait And Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=tab_16_header_h2
    Focus    //h2[@id='subscriptions_header_h2']
    click on element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']    40    2
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Suspended'    Add Result    PASS    UC7.32    Freeze Subscription order should be verified/completed successfull
    \    ...    Freeze Subscription completed successfully    Verification completed
    \    ...    ELSE    Add Result    FAILD    UC7.32    Freeze Subscription order should be verified/completed successfully
    \    ...    Freeze Subscription faild    Verification completed
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Suspended'    Exit For Loop    Log    Freeze Subscription completed successfully
    \    ...    ELSE    log    Freeze Subscription not completed successfully

InvoicePaymentAndVerify
    [Arguments]    ${MSISDN}    ${Payment_Method}    ${Amount}    ${Customer_Name}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_InvoicePayment
    wait for element    //div[@id='grid']
    wait for element    //input[@id='searchSubs']
    click on element    //input[@id='searchSubs']
    send text to element    //input[@id='searchSubs']    ${MSISDN}
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Click Element    //button[@id='searchbtn']
    #### Get count of the webtable ###
    sleep    2s
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[6]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Open_Amount}=    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX} ]//td[6]
    \    log    ${Open_Amount}
    \    Run Keyword If    '${Open_Amount}'>0    Select Checkbox    //div[@id='grid']//table//tbody//tr//td[1]//input
    \    sleep    1s
    \    Exit For Loop If    '${Open_Amount}'>0
    wait for element    //button[@id='applytop']
    Double Click Element    //button[@id='applytop']
    sleep    2s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_paymentmethod
    click on element    id=jwl_paymentmethod
    click on element    //select[@id='jwl_paymentmethod_i']//option[@title='${Payment_Method}']
    wait for element    id=jwl_freeamount
    click on element    id=jwl_freeamount
    #click on element    //div[@id='jwl_adjustmentreason']//select[@id='jwl_adjustmentreason_i']//option[@title='${Ad_Reason}']
    #wait for element    id=jwl_amount
    #click on element    id=jwl_amount
    send text to element    //input[@id='jwl_freeamount_i']    ${Amount}
    wait for element    id=jwl_collectorid
    click on element    id=jwl_collectorid_i
    click on element    //li[@id='item0']//a[@class='ms-crm-IL-MenuItem-Anchor ms-crm-IL-MenuItem-Anchor-Rest']
    ${Amount_for_verification}    Evaluate    ${Open_Amount}-${Amount}
    Unselect Frame
    wait for element    //li[@id='jwl_bi_payinvoice|NoRelationship|Form|jwl.jwl_bi_payinvoice.Submit.Button']
    click on element    //li[@id='jwl_bi_payinvoice|NoRelationship|Form|jwl.jwl_bi_payinvoice.Submit.Button']
    sleep    30s
    ${BISubmitMsg}    Confirm Action
    Run Keyword If    '${BISubmitMsg}'=='BI Invoice Payment Submitted Successfully.'    VerifyInvoicePayment    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}    ESLE
    ...    log    BI is got error. Error details is :- ${BISubmitMsg}

VerifyInvoicePayment
    [Arguments]    ${Customer_Name}    ${MSISDN}    ${Amount_for_verification}    ${Screenshotpath}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    FrameSelect
    wait for element    //div[@id='header_jwl_individualcustomerid']
    Double Click Element    //div[@id='header_jwl_individualcustomerid']//div//span[contains(text(),'${Customer_Name}')]
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Cust 360 page
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    //h2[@id='tab_10_header_h2']
    Focus    //h2[@id='tab_10_header_h2']
    click on element    //h2[@id='tab_10_header_h2']
    Comment    sleep    10s
    Wait Until Keyword Succeeds    30s    1s    Element Should Not Be Visible    //img[@id='loading']
    Select IFrame    id=WebResource_individual_invoiceGrid
    wait for element    //div[@id='grid']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='grid']//table//tbody//tr//td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    Screenshots    ${Screenshotpath}    Invoice tab
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${Invoice_MSISDN}    Get Text    //div[@id='grid']//table//tbody//tr[${INDEX}]//td[2]
    \    Run Keyword If    '${Invoice_MSISDN}'=='${MSISDN}'    GetInvoicePaymentVerification    ${INDEX}    ${Amount_for_verification}
    \    Exit For Loop If    '${Invoice_MSISDN}'=='${MSISDN}'

GetInvoicePaymentVerification
    [Arguments]    ${INDEX}    ${Amount_for_verification}
    ${Open_Amount_Invoice}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[5]
    log    ${Open_Amount_Invoice}
    Run Keyword If    '${Amount_for_verification}'=='${Open_Amount_Invoice}'    log    Invoice payment done
    ...    ELSE    log    Invoice payment is not correctly performed
    Run Keyword If    '${Amount_for_verification}'=='${Open_Amount_Invoice}'    Add Result    PASS    UC5.25_InvPay    Invoice payment should be succesful    Invoice payment sucessful
    ...    Invoice payment verification for amount is done
    ...    ELSE    Add Result    FAIL    UC5.25_InvPay    Invoice payment should be succesful    Invoice payment is not sucessful
    ...    Invoice payment verification for amount is done

BalanceRefillAndVerify
    [Arguments]    ${Refill_Amount}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=etel_birefill|NoRelationship|Form|Mscrm.Form.etel_birefill.Save    40    1
    Screenshots    ${Screenshotpath}    Balance Refill Form
    Comment    FrameSelect
    wait for element    id=contentIFrame0
    Select Frame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    wait for element    //div[@id='jwl_refillamount']
    click on element    //div[@id='jwl_refillamount']
    Comment    click on element    //input[@id='jwl_refillamount_i']
    send text to element    //input[@id='jwl_refillamount_i']    ${Refill_Amount}
    wait for element    //div[@id='jwl_refillprofileid']
    Focus    //div[@id='jwl_refillprofileid']
    click on element    //div[@id='jwl_refillprofileid']
    Click Element    id=jwl_refillprofileid_i
    sleep    5s
    Click Element    //ul[@id='jwl_refillprofileid_i_IMenu']//a[@title='E-Topup Ranges']
    Comment    Run Keyword And Ignore Error    click on element    //div[@id='etel_refillprofile1']//select[@id='etel_refillprofile1_i']//option[@title='Top Up']
    Screenshots    ${Screenshotpath}    Balance Refill form filled
    Comment    ${Reason}    Get Text    //div[@id='etel_refillreason2']
    Comment    log    ${Reason}
    Comment    Run Keyword If    '${Reason}'=='Refill'    log    Refill reason is correct
    ...    ELSE    log    Refill reason is incorrect. The available reason is :- ${Reason}
    Comment    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    Add Result    PASS    UC03    Reason should be selected as Refill
    ...    Refill reason is correct    Verification completed
    ...    ELSE    Add Result    FAILD    UC03    Reason should be selected as Refill    Refill reason is incorrect. The available reason is :- ${Reason}
    ...    Verification completed
    sleep    5s
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    //div[@id='jwl_paymentid']/div/span
    Focus    //div[@id='jwl_paymentid']/div/span
    Double Click Element    //div[@id='jwl_paymentid']/div/span
    Unselect Frame
    #Wait And Click Element    //div[@id='jwl_paymentid']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Screenshots    ${Screenshotpath}    Payment form
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //input[@id='jwl_collectionmethod_ledit']
    Wait And Click Element    //img[@id='jwl_collectionmethod_i']
    Wait And Click Element    //ul[@id='jwl_collectionmethod_i_IMenu']//li[1]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Payment details filled
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    10s
    wait for element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']    70    1
    Focus    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    #Choose Ok On Next Confirmation
    click on element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    sleep    30s
    Comment    Screenshots    ${Screenshotpath}    BI Submit
    #sleep    5s
    #Wait Until Keyword Succeeds    30s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${BISubMsg}    Confirm Action
    log    ${BISubMsg}
    ${match}    Set Variable    ${BISubMsg}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Balance Refill BI status is ${BISubMsg}
    ...    ELSE    Log    Balance Refill BI status is ${BISubMsg}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC5.23    Balance Refill BI should be submitted successfully    Balance Refill BI is submitted successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAIL    UC5.23    Balance Refill BI should be submitted successfull    Balance Refill BI is not submitted successfully.The Error detail is :- ${BISubMsg}
    ...    Verification completed

EnterActivationCode
    [Arguments]    ${ActSerialNum}    ${Screenshotpath}
    wait for element    //div[@id='jwl_activationcode']
    click on element    //div[@id='jwl_activationcode']
    send text to element    //input[@id='jwl_activationcode_i']    ${ActSerialNum}
    Screenshots    ${Screenshotpath}    Activation code detail

EnterSerialNumber
    [Arguments]    ${ActSerialNum}    ${Screenshotpath}
    wait for element    //div[@id='jwl_serialno']
    click on element    //div[@id='jwl_serialno']
    send text to element    //input[@id='jwl_serialno_i']    ${ActSerialNum}
    Screenshots    ${Screenshotpath}    Serial number details

VoucherRefillAndVerify
    [Arguments]    ${Proceed_Code}    ${ActiCode_Serial_num}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Voucher Refill form
    wait for element    //div[@id='jwl_proceedwith']
    click on element    //div[@id='jwl_proceedwith']
    wait for element    //div[@id='jwl_proceedwith']//select[@id='jwl_proceedwith_i']//option[@title='${Proceed_Code}']
    click on element    //div[@id='jwl_proceedwith']//select[@id='jwl_proceedwith_i']//option[@title='${Proceed_Code}']
    run keyword if    '${Proceed_Code}'=='Activation Code'    EnterActivationCode    ${ActiCode_Serial_num}    ${Screenshotpath}
    ...    ELSE    EnterSerialNumber    ${ActiCode_Serial_num}    ${Screenshotpath}
    wait for element    id=WebResource_bipayment
    Select IFrame    id=WebResource_bipayment
    Comment    wait for element    id=WebResource_voucherrefillhandler
    Comment    Select IFrame    id=WebResource_voucherrefillhandler
    wait for element    //button
    click on element    //button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Wait And Click Element    id=etel_birefill|NoRelationship|Form|Mscrm.Form.etel_birefill.Save
    wait for element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    click on element    //li[@id='etel_birefill|NoRelationship|Form|etel.etel_birefill.Submit.Command']
    sleep    30s
    ${BISubMsg}    Confirm Action
    log    ${BISubMsg}
    sleep    5s
    ${validationmsg}    Set Variable    ${BISubMsg}
    ${value}    Set Variable    Submitted
    Comment    ${value1}    Set Variable    Closed
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${validationmsg}    ${value}
    Comment    ${match1}    ${value1}    Run Keyword And Ignore Error    Should Contain    ${validationmsg}    ${value1}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    Comment    ${RETURNVALUE1}    Set Variable If    '${match1}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Voucher Refill BI status is ${BISubMsg}
    ...    ELSE    Log    Voucher Refill BI status is ${BISubMsg}
    Comment    Run Keyword If    Run Keyword If    Log    Voucher Refill BI status is ${BISubMsg}
    ...    ELSE    Log    Voucher Refill BI status is ${BISubMsg}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC5.22_VoucherRefill    Voucher Refill should be submitted successful    Voucher Refill is submitted successful
    ...    BI Submit verification done
    ...    ELSE    Add Result    FAILED    UC5.22_VoucherRefill    Voucher Refill should be submitted successful    Voucher Refill is not submitted successful
    ...    BI Submit verification done

SKFPSubmitAndVerify
    [Arguments]    ${SKFP_num}    ${User_Profile_Lang}    ${Summary_Lan}    ${Screenshotpath}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='left']//input
    click on element    //div[@id='left']//input
    send text to element    //div[@id='left']//input    ${SKFP_num}
    wait for element    //button[@id='btn_SearchSKFP']
    click on element    //button[@id='btn_SearchSKFP']
    ${MSISDN}    Get Text    //div[@id='left']//table//tbody//tr//td//div[@class='formFieldValue ng-binding']
    log    ${MSISDN}
    Add Result    PASS    UC7.2    MSISDN should be populated    MSISDN populated    MSISDN verification completed
    wait for element    //button[@id='btn_saveResultSkfp']
    click on element    //button[@id='btn_saveResultSkfp']
    Screenshots    ${Screenshotpath}    SKFPPage
    wait for element    id=stageAdvanceActionContainer
    click on element    id=stageAdvanceActionContainer
    Unselect Frame
    NewSubUserProfilePage    ${User_Profile_Lang}    ${Screenshotpath}
    ###Eligibility
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    select Iframe    id=WebResource_processapp
    #wait for element    id=btnConfigure    60    1
    Screenshots    ${Screenshotpath}    EligibilityPage
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ###Payment
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    select Iframe    id=WebResource_processapp
    #wait for element    id=btnConfigure    60    1
    Screenshots    ${Screenshotpath}    PaymentPage
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ###Summary
    NewSubSummaryPage    ${Summary_Lan}    ${Screenshotpath}
    Sleep    40s
    Screenshots    ${Screenshotpath}    SubmitMsg
    ${NewSubSumitmessage}    Confirm Action
    log    ${NewSubSumitmessage}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='header_jwl_status']    40s
    Screenshots    ${Screenshotpath}    OrderStatus
    ${StatusReasonNewSub}    Get Text    xpath=//div[@id="header_jwl_status"]//div//span
    ${match}    Set Variable    ${StatusReasonNewSub}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    SK/FP sumbitted successfully and status is ${StatusReasonNewSub}
    ...    ELSE    Log    New Subscription \ sumbitted status is ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC7.2    SK/FP should be \ sumbitted successfully    SK/FP is submitted succesfully
    ...    SK/FP verification done
    ...    ELSE    Add Result    FAIL    UC7.2    SK/FP should be \ sumbitted successfully    SK/FP is not submitted succesfully
    ...    SK/FP verification done
    Run Keyword If    '${RETURNVALUE}'=='True'    VerifySKFP    ${MSISDN}    ${Screenshotpath}
    ...    ELSE    log    SKFP not submitted succesfully. Order status is :- {StatusReasonNewSub}

VerifySKFP
    [Arguments]    ${MSISDN}    ${Screenshotpath}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=subscriptions_header_h2
    click on element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Customer360Page
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[2]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to get contract id for customer ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${MSISDNFromWeb}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[2]
    \    log    ${MSISDNFromWeb}
    \    Run Keyword If    '${MSISDNFromWeb}'=='${MSISDN}'    Add Result    PASS    UC7.2    SK/FP should be done for correct MSISDN
    \    ...    SK/FP done for correct MSISDN    SK/FP verification
    \    ...    ELSE    Add Result    FAIL    UC7.2    SK/FP should be done for correct MSISDN
    \    ...    SK/FP done for correct MSISDN    SK/FP verification
    \    Exit For Loop If    '${MSISDNFromWeb}'=='${MSISDN}'
    ##### For loop ends ####
    Unselect Frame

CancelOrderAndVerify
    [Arguments]    ${Screenshotpath}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Order_ID}    Get text    //div[@id='FormTitle']
    log    ${Order_ID}
    Unselect Frame
    wait for element    //li[@id='etel_ordercapture|NoRelationship|Form|jwl.etel_ordercapture.CancelOrder.Button']
    click on element    //li[@id='etel_ordercapture|NoRelationship|Form|jwl.etel_ordercapture.CancelOrder.Button']
    sleep    20s
    ${OrderSubMsg}    Confirm Action
    log    ${OrderSubMsg}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //span[@id='TabNode_tab0Tab']
    click on element    //span[@id='TabNode_tab0Tab']
    wait for element    //a[@id='Node_nav_etel_contact_ordercapture_individualcustomerid']
    click on element    //a[@id='Node_nav_etel_contact_ordercapture_individualcustomerid']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    select IFrame    id=contentIFrame0
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    id=area_etel_contact_ordercapture_individualcustomeridFrame
    select IFrame    id=area_etel_contact_ordercapture_individualcustomeridFrame
    wait for element    //a[@id='crmGrid_etel_contact_ordercapture_individualcustomerid_SavedNewQuerySelector']
    click on element    //a[@id='crmGrid_etel_contact_ordercapture_individualcustomerid_SavedNewQuerySelector']
    wait for element    //li[@id='{36A67C39-5AD0-E311-B005-005056AE11FF}']//span[contains(text(),'All Orders')]
    click on element    //li[@id='{36A67C39-5AD0-E311-B005-005056AE11FF}']//span[contains(text(),'All Orders')]
    wait for element    //table[@id='gridBodyTable']
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//table[@id='gridBodyTable']//tbody//tr//td[3]
    ${count}=    Evaluate    ${functionBlockCount}+1
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${OrderFromPage}    Get text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    ${OrderStatus} =    Run Keyword If    '${OrderFromPage}'=='${Order_ID}'    get text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[5]
    \    log    ${OrderStatus}
    \    Run Keyword If    '${OrderStatus}'=='Cancel'    Add Result    PASS    UC_5.23 Cancel Order    Cancel Order submitted succesfully
    \    ...    Cancel Order has to be submitted succesfully    Cancel Order is succesfull
    \    ...    ELSE    log    Cancel order BI is not validated. Order status is :- ${OrderStatus}
    \    Exit For Loop If    '${OrderFromPage}'=='${Order_ID}'

ReviewFinHisAndVerify
    [Arguments]    ${From_date}    ${To_date}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait and click element    id=review_financial_history_header_h2
    sleep    10s
    Screenshots    ${Screenshotpath}    Review Financial history page
    wait for element    id=WebResource_review_financial_history
    Select IFrame    id=WebResource_review_financial_history
    Screenshots    ${Screenshotpath}    Review financial History frame
    wait for element    id=FromDate
    click on element    id=FromDate
    send text to element    //input[@id='FromDate']    ${From_date}
    wait for element    id=EndDate
    click on element    id=EndDate
    send text to element    //input[@id='EndDate']    ${To_date}
    wait for element    id=btnSearch
    click on element    id=btnSearch
    Screenshots    ${Screenshotpath}    Review financial history data
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Present}    Run Keyword And Return Status    Page Should Contain Element    //table[@id='financialHistGrid']//tbody//tr
    log    ${Present}
    Run Keyword If    '${Present}'=='True'    Log    Financial history is present for given dates
    Run Keyword If    '${Present}'=='True'    Add Result    PASS    UC5.13_ReviewFinancialHist    Financial History should be visible    Financial History is visible.
    ...    Financial history is validated successfully
    ...    ELSE    Add Result    FAILED    UC5.13_ReviewFinancialHist    Financial History should \ be visible    Financial History is not visible.
    ...    Financial history validated done

VerifyAccHistory
    FrameSelect
    Select IFrame    id=WebResource_ah
    wait for element    //button[@id='search']
    click on element    //button[@id='search']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Present}    Run Keyword And Return Status    Page Should Contain Element    //table[@id='accountHistoryGrid']//tbody//tr
    log    ${Present}
    Run Keyword If    '${Present}'=='True'    Log    Account History is present for selected subscription
    ...    ELSE    log    Account History is not present for selected subscription

CreateNewPostPaidSubscription
    [Arguments]    ${Offer}    ${MSISDN}    ${SIM}    ${User_profile_Language}    ${Summary_Billing_Lan}    ${Notes}
    ...    ${IMEI}    ${Screenshotpath}
    #Products#
    sleep    10s
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    wait for element    id=newCustomerSearchButtonGroup
    Comment    Wait And Click Element    xpath=//div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'SEARCH')]
    Click Element    //button[@class='formButton ng-binding']
    Comment    Wait And Click Element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]
    Wait And Click Element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]
    wait and click element    xpath=//span[@class='cmdBarBtnSpan ng-binding'][@ng-click='!rootScopeData.currentStage.isActive || scopeData.planSelection.addBasket()']
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=orderBasket
    ${Offer_Pass}    ${Offer_Fail}    Run Keyword And Ignore Error    Page Should Contain Element    //div[@id='orderBasket']
    Run Keyword If    '${Offer_Pass}'=='PASS'    Add Result    PASS    UC    Offer should be added in order basket    Offer is added in order basket
    ...    Offer validation done
    ...    ELSE    Add Result    FAILED    UC    Offer should be added in order basket    Offer is added in order basket
    ...    Offer validation done
    Comment    ${alloffers}    Get Text    xpath=//div[@id='orderBasket']//div[@class='dataGridBodyScroll']//tr[@class='dataGridBodyRow ng-scope']//td[1]//span[@class='lowerCellSection ng-binding']
    Comment    log    ${alloffers}
    Comment    Run Keyword If    '${alloffers}'=='${Offer}'    Log    Correct offer is added in Basket
    ...    ELSE    log    Offer is not added in bascket. Offer in bascket is = ${alloffers}
    Comment    Run Keyword If    '${alloffers}'=='${Offer}'    Add Result    PASS    UC7.9_NewSubWithDev    Correct offer should be added in order basket
    ...    Correct offer is added in Basket    Offer validation done
    ...    ELSE    Add Result    FAILED    UC7.9_NewSubWithDev    Correct offer should be added in order basket    Correct offer is not added in Basket
    ...    Offer validation done
    Comment    Unselect Frame
    Screenshots    ${Screenshotpath}    Product page
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait for element    id=WebResource_processapp
    Comment    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    sleep    10s
    #Configuration#
    #wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    xpath=//table//tbody//tr[@class='dataGridBodyRow ng-scope']//div[@class='dataGridCellContent']//span[contains(text(),'${Offer}')]    40    1
    Sleep    5s
    Screenshots    ${Screenshotpath}    Configuration page
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Resources#
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@id='FormTitle']/h1[@class='ms-crm-TextAutoEllipsis']    40    1
    ${orderid}    Get Text    //div[@id='FormTitle']/h1[@class='ms-crm-TextAutoEllipsis']
    Log    ${orderid}
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock    60    1
    Comment    wait for element    //input[@id='txtMSISDN']
    Comment    click on element    //input[@id='txtMSISDN']
    Comment    send text to element    //input[@id='txtMSISDN']    ${MSISDN}
    Comment    click on element    //input[@id='txtSIM']
    Comment    send text to element    //input[@id='txtSIM']    ${SIM}
    Comment    click on element    //input[@id='txtMSISDN']
    #clickElement_sel    //input[@id='txtMSISDN']    30
    Comment    click on element    //input[@id='txtMSISDN']
    Comment    send text to element    //input[@id='txtMSISDN']    ${MSISDN}
    Comment    click on element    //input[@id='txtSIM']
    Comment    send text to element    //input[@id='txtSIM']    ${SIM}
    Comment    click on element    //input[@id='txtMSISDN']
    ###Reserve MSISDN###
    Click Element    //button[@id='btnSearchMSISDN']
    #Wait And Click Element    //button[@id='btnSearchMSISDN']
    Comment    Unselect Frame
    sleep    10s
    @{Allwindows}    Get Window Titles
    Log Many    @{Allwindows}
    Select Window    title=Resource Administration
    Comment    sleep    30s
    Comment    @{Allwindows}    Get Window Titles
    Comment    log    @{Allwindows}
    Comment    ${Countofwindows}    Get Length    @{Allwindows}
    Comment    ${CountForLoop}    Evaluate    ${Countofwindows}+1
    Comment    :FOR    ${I}    IN RANGE    0    ${Countofwindows}
    Comment    ${title}    Get From List    @{Allwindows}    ${I}
    Comment    log    ${title}
    Comment    Run Keyword If    '${title}'=='undefined'    Select Window    ${Allwindows[${I}]}
    Comment    SelectTitleOfWindow    @{Allwindows}
    Comment    Run Keyword If    '${title}'!='undefined'    SelectTitleOfWindow    @{Allwindows}
    Comment    Exit For Loop
    Comment    Wait Until Keyword Succeeds    40    1    Select Window    @{Allwindows}[1]
    wait for element    //h5[contains(text(),'CRITERIA')]    40    2
    wait for element    //span[@aria-controls='msisdnCriteria_listbox']
    Click on element    //span[@aria-controls='msisdnCriteria_listbox']
    Sleep    1s
    wait for element    //ul[@id='msisdnCriteria_listbox']//li[contains(text(),'Equals')]
    click on element    //ul[@id='msisdnCriteria_listbox']//li[contains(text(),'Equals')]
    #Wait And Click Element    //span[@aria-controls='filterByCriteria_listbox']
    #Wait And Click Element    //ul[@id='filterByCriteria_listbox']//li[contains(text(),'Only Reserved Numbers')]
    wait for element    id=msisdnValue
    click on element    id=msisdnValue
    send text to element    id=msisdnValue    ${MSISDN}
    Run Keyword And Ignore Error    click on element    id=query
    Run Keyword And Ignore Error    Wait And Click Element    //div[@id='QueryGrid']//input[@type='checkbox']
    Focus    //button[@id='select']
    Comment    Choose Ok On Next Confirmation
    Comment    ${element_xpath}    set variable    xpath=//button[@id='select']
    Comment    ${element_xpath}    Replace String    ${element_xpath}    \"    \\\"
    Comment    log    ${element_xpath}
    Comment    Execute Javascript    document.evaluate(//button[@id='select'], document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
    Comment    Execute JavaScript    document.evaluate("${element_xpath}", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null).snapshotItem(0).click();
    Comment    Click Element    //button[@id='select']
    Comment    Focus    //button[@id='select']
    Comment    Choose Ok On Next Confirmation
    Comment    Click Element    //button[@id='select']    True
    sleep    2s
    Comment    Execute JavaScript    window.document.evaluate("//button[@id='select']", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.scrollIntoView(true);
    Click Button    xpath=//button[@id='select']    True
    sleep    5s
    ${MSISDNmsg}    Confirm Action
    log    ${MSISDNmsg}
    Sleep    2s
    Comment    Close Window
    Comment    Run Keyword If    '${MSISDNmsg}'=='MSISDN Reservation(Update) has completed successfully'    Add Result    PASS
    Select Window    title=Order Capture: ${orderid} - Microsoft Dynamics CRM
    Comment    sleep    10s
    Comment    Run Keyword And Ignore Error    Confirm Action
    Comment    Run Keyword And Ignore Error    Wait Until Keyword Succeeds    20    1    Select Window    title=Order Capture: ${orderid} - Microsoft Dynamics CRM
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    wait for element    //input[@id='txtSIM']
    send text to element    //input[@id='txtSIM']    ${SIM}
    click on element    //button[@id='btnAssignSIM']
    sleep    50s
    ${SIMAssignmessage}    Confirm Action
    log    ${SIMAssignmessage}
    Comment    Log To Console    ${SIMAssignmessage}
    Comment    Run Keyword If    '${SIMAssignmessage}'=='Sim has reserved successfully'    Log    SIM Reservered successfully
    ...    ELSE
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    ##Reserve MSISDN Ends##
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Add On Expiration#
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #User Profile#
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='left']//select[@name='language']
    wait and click element    //div[@id='left']//select[@name='language']//option[contains(text(),'${User_profile_Language}')]
    Unselect Frame
    Comment    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Handset with Installment#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='createInstallment']/select    40    1
    Wait And Click Element    //div[@id='createInstallment']/select
    Wait And Click Element    //div[@id='createInstallment']//select//option[@label='Commitment and Installments 12Months']
    Wait And Click Element    id=btn_createInstallment    40    2
    Wait And Click Element    //div[@id='existingOfferingsGridBody']//tr[@class='dataGridBodyRow ng-scope']/td[1]//a[@class='ng-binding']
    unselect frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Sleep    5s
    Run Keyword And Continue On Failure    Confirm Action
    Sleep    5s
    wait for element    //div[@id='jwl_offeringproductid']
    click on element    //div[@id='jwl_offeringproductid']
    wait for element    //input[@id='jwl_offeringproductid_ledit']
    click on element    //input[@id='jwl_offeringproductid_ledit']
    Input Text    //input[@id='jwl_offeringproductid_ledit']    Alcalet 10.16
    Sleep    5s
    Comment    wait for element    //img[@id='jwl_offeringproductid_i']
    Comment    click on element    //img[@id='jwl_offeringproductid_i']
    Comment    wait for element    //ul[@id='jwl_offeringproductid_i_IMenu']/li[1]
    Comment    click on element    //ul[@id='jwl_offeringproductid_i_IMenu']/li[1]
    sleep    2s
    Run Keyword And Continue On Failure    Confirm Action
    sleep    3s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='jwl_handsettierid']
    click on element    //div[@id='jwl_handsettierid']
    wait for element    //input[@id='jwl_handsettierid_ledit']
    click on element    //input[@id='jwl_handsettierid_ledit']
    wait for element    //img[@id='jwl_handsettierid_i']
    click on element    //img[@id='jwl_handsettierid_i']
    wait for element    //ul[@id='jwl_handsettierid_i_IMenu']/li[1]
    click on element    //ul[@id='jwl_handsettierid_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=jwl_product_imei
    wait for element    id=jwl_product_imei_i
    send text to element    id=jwl_product_imei_i    1234657898765435
    sleep    2s
    Run Keyword And Ignore Error    wait for element    //div[@id='jwl_installmenttypegroupid']
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_installmenttypegroupid']
    Run Keyword And Ignore Error    wait for element    //input[@id='jwl_installmenttypegroupid_ledit']
    Run Keyword And Ignore Error    click on element    //input[@id='jwl_installmenttypegroupid_ledit']
    Run Keyword And Ignore Error    wait for element    //img[@id='jwl_installmenttypegroupid_i']
    Run Keyword And Ignore Error    click on element    //img[@id='jwl_installmenttypegroupid_i']
    Run Keyword And Ignore Error    wait for element    //ul[@id='jwl_installmenttypegroupid_i_IMenu']//li[1]
    Run Keyword And Ignore Error    click on element    //ul[@id='jwl_installmenttypegroupid_i_IMenu']//li[1]
    sleep    2s
    Run Keyword And Ignore Error    wait for element    //div[@id='jwl_installmenttypeid']
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_installmenttypeid']
    Run Keyword And Ignore Error    wait for element    //input[@id='jwl_installmenttypeid_ledit']
    Run Keyword And Ignore Error    click on element    //input[@id='jwl_installmenttypeid_ledit']
    Run Keyword And Ignore Error    wait for element    //img[@id='jwl_installmenttypeid_i']
    Run Keyword And Ignore Error    click on element    //img[@id='jwl_installmenttypeid_i']
    Run Keyword And Ignore Error    wait for element    //ul[@id='jwl_installmenttypeid_i_IMenu']//li[1]
    Run Keyword And Ignore Error    click on element    //ul[@id='jwl_installmenttypeid_i_IMenu']//li[1]
    Unselect Frame
    Run Keyword And Continue On Failure    click on element    //span[contains(text(),'Validate IMEI')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    8s
    Run Keyword And Continue On Failure    Confirm Action
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=jwl_bi_installmentorder|NoRelationship|Form|Mscrm.Form.jwl_bi_installmentorder.SaveAndClose
    wait for element    //span[contains(text(),'Documents')]    60    2
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=btn_createInstallment    40    2
    wait for element    //div[contains(text(),'Existing Installment Plan')]    40    1
    wait for element    //div[@id='existingOfferingsGridBody']    40    2
    sleep    10s
    ${Base}    Get Text    //div[@id='existingOfferingsGridBody']//table//tr//td[3]
    ${Discount}    Get Text    //div[@id='existingOfferingsGridBody']//table//tr//td[4]
    ${Total}    Get Text    //div[@id='existingOfferingsGridBody']//table//tr//td[5]
    ##check Installment##
    Comment    ${AfterDiscount}=    Evaluate    ${Base} - ${Discount}
    Comment    Run Keyword If    ${AfterDiscount}==${Total}    Log    Total Amount is calculated right
    ...    ELSE    Log    Total Amount is calculated wrong
    #Check Installment Ends#
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Guarantee#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='downloadDocsTitle']//div[contains(text(),'GUARANTEES')]    40    1
    ${GuaranteeCheck}    Get Text    //button[@id='guaranteeButton']
    Log    ${GuaranteeCheck}
    Run Keyword If    '${GuaranteeCheck}' == 'Edit Guarantee'    GuaranteeCheck    ${Notes}    ${orderid}
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Financials and payments#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Wait And Click Element    id=btnConfigure
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_collectionmethod    60    1
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    5s
    Wait And Click Element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    60    1
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //table[@id='formField_noSubscriptions']    40    1
    Wait And Click Element    //table[@id='formField_noSubscriptions']/tbody/tr[2]/td[2]/div/select
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    Wait And Click Element    //table[@id='formField_noSubscriptions']/tbody/tr[2]/td[2]/div/select
    Wait And Click Element    //table[@id='formField_noSubscriptions']//option[contains(text(),'English')]
    ##Submit Check##
    Wait And Click Element    id=stageAdvanceActionContainer
    sleep    20s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyCreateNewPostPaidSubscription    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyCreateNewPostPaidSubscription    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ##Submit Check Ends##

GuaranteeCheck
    [Arguments]    ${Notes}    ${orderid}
    Wait And Click Element    //button[@id='guaranteeButton']    40    2
    sleep    10s
    Unselect Frame
    #@{AllGuaWindows}    Get Window Titles
    Comment    log many    @{AllGuaWindows}
    Comment    Run Keyword If    '@{AllGuaWindows}[1]'=='undefined'    Select Window    New
    ...    ELSE    Select Window    @{AllGuaWindows}[1]
    Comment    Wait Until Keyword Succeeds    120    1    Select Window    new
    Comment    Run Keyword And Continue On Failure    closeexploreCMR
    @{Allwindows}    Get Window Titles
    SelectTitleOfWindow    @{Allwindows}
    Comment    ${Countofwindows}    Get Length    @{Allwindows}
    Comment    ${CountForLoop}    Evaluate    ${Countofwindows}+1
    Comment    : FOR    ${I}    IN RANGE    0    ${Countofwindows}
    Comment    \    ${title}    Get From List    @{Allwindows}    ${I}
    Comment    \    log    ${title}
    Comment    \    Run Keyword If    '${title}'=='undefined'    Select Window    New
    ...    ELSE    SelectTitleOfWindow    @{Allwindows}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    id=jwl_guaranteecheck    40    1
    Wait And Click Element    id=jwl_guaranteetype
    Wait And Click Element    //select[@id='jwl_guaranteetype_i']//option[contains(text(),'Good / Medium Credit Score')]
    wait for element    id=jwl_services
    click on element    id=jwl_services
    wait for element    //select[@id='jwl_services_i']
    click on element    //select[@id='jwl_services_i']/option[2]
    Wait And Click Element    id=jwl_notes
    send text to element    id=jwl_notes_i    ${Notes}
    Wait And Click Element    id=jwl_regionid
    Wait And Click Element    id=jwl_regionid_ledit
    Wait And Click Element    //img[@id='jwl_regionid_i']
    wait for element    id=jwl_regionid_i_IMenu
    Wait And Click Element    //ul[@id='jwl_regionid_i_IMenu']/li[1]
    sleep    2s
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    //li[@id='jwl_guarantee|NoRelationship|Form|Mscrm.Form.jwl_guarantee.SaveAndClose']
    Run Keyword And Continue On Failure    click on element    //li[@id='jwl_guarantee|NoRelationship|Form|Mscrm.Form.jwl_guarantee.SaveAndClose']
    wait for element    //li[@id='jwl_guarantee|NoRelationship|Form|jwl.jwl_guarantee.Button1.Button']
    Run Keyword And Ignore Error    Click Element    //li[@id='jwl_guarantee|NoRelationship|Form|jwl.jwl_guarantee.Button1.Button']
    Wait Until Keyword Succeeds    40    1    Select Window    title=Order Capture: ${orderid} - Microsoft Dynamics CRM
    sleep    7s
    wait for element    id=contentIFrame0    40    1
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    sleep    10s
    ${checked}    Get Value    //div[@id='guaranteeGridBody']//tr[@class='dataGridBodyRow ng-scope']//td[2]/div/span
    Run Keyword If    '${checked}' == 'Yes'    Log    Guarantee has been successfully checked
    ...    ELSE    Log    Guarantee has not been successfully checked

VerifyCreateNewPostPaidSubscription
    [Arguments]    ${MSISDN}
    Wait And Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button    60    2
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait and click element    //h2[@id='subscriptions_header_h2']    40    2
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']    40    2
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[2]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ '${MSISDN}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop    Log    PostPaidSubscription completed successfully
    \    ...    ELSE    log    PostPaid Subscription not completed successfully

Submit FamilyPackage
    [Arguments]    ${GovtID}    ${FatherSubs_Id}    ${MotherSubs_Id}    ${ChildSubs_Id}    ${Screenshotpath}
    #Father#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_SearchIndividual    40    1
    Select IFrame    id=WebResource_SearchIndividual
    wait for element    //button[contains(text(),'Search Father')]    40    1
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_palastineid    40    1
    click on element    id=jwl_palastineid
    wait for element    id=jwl_palastineid_i
    send text to element    id=jwl_palastineid_i    ${GovtID}
    wait for element    id=WebResource_SearchIndividual
    Select IFrame    id=WebResource_SearchIndividual
    click on element    //button[contains(text(),'Search Father')]
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${father}    Get Text    //div[@id='header_process_jwl_individualcustomerid']//span[@class='ms-crm-Lookup-Item']
    ${father_new}    Get Text    //div[@id='jwl_individualcustomerid']//span[@class='ms-crm-Lookup-Item']
    Run Keyword If    '${father}' == '${father_new}'    Log    father added successfully
    ...    ELSE    Log    father not added successfully
    Screenshots    ${Screenshotpath}    Add Father
    ${father_subs}    Get Text    //div[@id='header_process_jwl_subscriptionid']//span[@otypename='etel_subscription']
    ${father_offering}    Get Text    //div[@id='header_process_jwl_fatherofferingid']//span[@otypename='product']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Double Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Mother#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=header_process_jwl_mothersubscriptionidid_cl    40    1
    Wait And Click Element    id=header_process_jwl_mothersubscriptionidid
    Wait And Click Element    id=header_process_jwl_mothersubscriptionidid_ledit
    Wait And Click Element    id=header_process_jwl_mothersubscriptionidid_i
    Wait And Click Element    //ul[@id='header_process_jwl_mothersubscriptionidid_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_motheroffering
    Wait And Click Element    id=header_process_jwl_motheroffering_ledit
    Wait And Click Element    id=header_process_jwl_motheroffering_i
    Wait And Click Element    //ul[@id='header_process_jwl_motheroffering_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_thresholdmother
    Wait And Click Element    //select[@id='header_process_jwl_thresholdmother_i']/option[3]
    ${mother_sub}    Get Text    //div[@id='header_process_jwl_mothersubscriptionidid']//span[@otypename='etel_subscription']
    ${mother_offering}    Get Text    //div[@id='header_process_jwl_motheroffering']//span[@otypename='product']
    Screenshots    ${Screenshotpath}    Add mother
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Child#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=header_process_jwl_childoonesubscriptionid    40    1
    sleep    10s
    Wait And Click Element    id=header_process_jwl_childoonesubscriptionid
    Wait And Click Element    id=header_process_jwl_childoonesubscriptionid_ledit
    Wait And Click Element    id=header_process_jwl_childoonesubscriptionid_i
    Wait And Click Element    //ul[@id='header_process_jwl_childoonesubscriptionid_i_IMenu']/li[1]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_childoneofferingid
    Wait And Click Element    id=header_process_jwl_childoneofferingid_ledit
    Wait And Click Element    id=header_process_jwl_childoneofferingid_i
    Wait And Click Element    //ul[@id='header_process_jwl_childoneofferingid_i_IMenu']//span[contains(text(),'Child Package')]
    sleep    2s
    Wait And Click Element    id=header_process_jwl_thresholdchild1
    Wait And Click Element    //select[@id='header_process_jwl_thresholdchild1_i']/option[3]
    ${child_subs}    Get Text    //div[@id='header_process_jwl_childoonesubscriptionid']//span[@otypename='etel_subscription']
    ${child_offering}    Get Text    //div[@id='header_process_jwl_childoneofferingid']//span[@otypename='product']
    Screenshots    ${Screenshotpath}    Add child
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    wait for element    id=jwl_bi_familypackage|NoRelationship|Form|Mscrm.Form.jwl_bi_familypackage.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=jwl_subscriptionid_cl    40    1
    ${mother_subs_added}    Get Text    //div[@id='jwl_mothersubscriptionidid']//span[@otypename='etel_subscription']
    ${mother_offer_added}    Get Text    //div[@id='jwl_motheroffering']//span[@otypename='product']
    ${child_subs_added}    Get Text    //div[@id='jwl_childoonesubscriptionid']//span[@otypename='etel_subscription']
    ${child_offer_added}    Get Text    //div[@id='jwl_childoneofferingid']//span[@otypename='product']
    ${father_subs_added}    Get Text    //div[@id='jwl_subscriptionid']//span[@otypename='etel_subscription']
    ${father_offer_added}    Get Text    //div[@id='jwl_fatherofferingid']//span[@otypename='product']
    Run Keyword If    '${father_subs} ' == '${father_subs_added}'    Log    Father subscription added successfully
    ...    ELSE    Log    Father subscription not added successfully
    Run Keyword If    '${father_offering}' == '${father_offer_added}'    Log    Father Offering added successfully
    ...    ELSE    Log    Father Offering not added successfully
    Run Keyword If    '${mother_sub}' == '${mother_subs_added}'    Log    Mother subscription added successfully
    ...    ELSE    Log    Mother subscription not added successfully
    Run Keyword If    '${mother_offering}' == '${mother_offer_added}'    Log    Mother Offering added successfully
    ...    ELSE    Log    Mother Offering not added successfully
    Run Keyword If    '${child_subs}' == '${child_subs_added}'    Log    Child subscription added successfully
    ...    ELSE    Log    Child subscription not added successfully
    Run Keyword If    '${child_offering}' == '${child_offer_added}'    Log    Child Offering added successfully
    ...    ELSE    Log    Child Offering not added successfully
    Wait And Click Element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    Unselect Frame
    ##Verify Submit##
    Wait And Click Element    id=jwl_bi_familypackage|NoRelationship|Form|jwl.jwl_bi_familypackage.Submit.Button
    Sleep    30s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    5s
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifySubmit FamilyPackage    ${father_subs}    ${mother_sub}    ${child_subs}    ${mother_offer_added}
    ...    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifySubmit FamilyPackage    ${father_subs}    ${mother_sub}    ${child_subs}    ${mother_offer_added}
    ...    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Screenshots    ${Screenshotpath}    submit the family package
    ##Verify Submit ended##

VerifySubmit FamilyPackage
    [Arguments]    ${father_subs}    ${mother_sub}    ${child_subs}
    Wait And Click Element    //div[@id='jwl_subscriptionid']//span[@otypename='etel_subscription']/span
    Unselect Frame
    wait for element    id=etel_subscription|NoRelationship|Form|Mscrm.Form.etel_subscription.Save    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    8s
    wait for element    id=FamilyMembers_header_h2    40    1
    click on element    id=FamilyMembers_header_h2
    wait for element    //div[@id='Family_divDataArea']
    wait for element    //tr[@otypename='jwl_family']/td[2]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${Father}    Get Text    //tr[@otypename='jwl_family']/td[2]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${Mother}    Get Text    //tr[@otypename='jwl_family']/td[3]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${Child}    Get Text    //tr[@otypename='jwl_family']/td[4]//span[@class='gridLui']//span[@class='ms-crm-LookupItem-Name']
    ${F}    Convert To Integer    ${Father}
    ${M}    Convert To Integer    ${Mother}
    ${C}    Convert To Integer    ${Child}
    ${Father_MSISDN}    Convert To Integer    ${father_subs}
    ${Mother_MSISDN}    Convert To Integer    ${mother_sub}
    ${Child_MSISDN}    Convert To Integer    ${child_subs}
    Run Keyword If    '${F}' == '${Father_MSISDN}' and '${M}' == '${Mother_MSISDN}' and '${C}' == '${Child_MSISDN}'    Log    Family Package Added successfully
    ...    ELSE    Log    Family Package not added Successfully

SelectTitleOfWindow
    [Arguments]    @{AllWindows}
    #@{AllWindows}    Create List    Order Capture: ORD0063356 - Microsoft Dynamics CRM    GRE
    ${Len}    Get Length    @{AllWindows}
    : FOR    ${I2}    IN RANGE    ${Len}
    \    ${R}    Run Keyword And Return Status    Should Not Contain    ${AllWindows[${I2}]}    Order Capture
    \    log    ${R}
    \    Run Keyword If    '${R}'=='True'    Select Window    ${AllWindows[${I2}]}
    \    Comment    Run Keyword If    '${R}'=='True' and ${R}!='undefined'    Select Window    ${AllWindows[${I2}]}
    \    Comment    Run Keyword If    '${R}'=='True' and '${R}'=='undefined'    Select Window    ${AllWindows[${I2}]}
    \    Exit For Loop If    '${R}'=='True'
    sleep    5s

OfferChange
    [Arguments]    ${ChangeOffer}    ${PaymentType}    ${Screenshotpath}    ${UCNo}
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //table[@class='formSectionTable']//select[@id='paymentTypeSelect1']    60    2
    Wait And Click Element    //select[@id='paymentTypeSelect1']//option[contains(text(),'${PaymentType}')]
    sleep    10s
    Comment    wait for element    //div[@id='processStepsContainer']/div[3]//div[@class='processStepValue']    40    2
    Comment    wait for element    //div[@id='processStepsContainer']//div[@class='process_PmType']    20    2
    ${CO}    Get Text    //div[@id='processStepsContainer']//div[@class='process_PmType']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${CO}
    Run Keyword If    '${CO}' == '${ChangeOffer}'    Log    Change Offer type selected successfully.
    ...    ELSE    Log    Change Offer type not selected successfully.
    Run Keyword If    '${CO}' == '${ChangeOffer}'    Add Result    PASS    ${UCNo}    Type of offer which is to be changed should be selected successfully.    Change Offer type is selected successfully.
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    ${UCNo}    Type of offer which is to be changed should be selected successfully.    Change Offer type is not selected successfully.
    ...    Verification completed
    Screenshots    ${Screenshotpath}    Offer change page
    Comment    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']
    Comment    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']/option[2]    60    1
    Wait And Click Element    //div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'Search')]
    wait for element    //table[@id='eligibleOffersDataGridBody']    60    2
    Wait And Click Element    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//input[@type='checkbox']
    ${offer}    Get Text    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//div[@class='dataGridCellContent']/span
    Log    ${offer}
    sleep    5s
    Wait And Click Element    //div[@id='offerChange-eligibleOffers']//span[contains(text(),'ADD TO BASKET')]
    sleep    10s
    wait for element    //div[@id='orderBasket']//table[@class='dataGridBody']    60    2
    ${offer_added}    Get Text    //div[@id='orderBasket']//table[@class='dataGridBody']/tbody/tr[1]//span[@class='lowerCellSection ng-binding']
    Log    ${offer_added}
    Should Be Equal    ${offer}    ${offer_added}
    Run Keyword If    '${offer}' == '${offer_added}'    Log    New Offer added successfully
    ...    ELSE    Log    New Offer not added successfully
    Run Keyword If    '${offer}' == '${offer_added}'    Add Result    PASS    ${UCNo}    New Offer should be added successfully    New Offer added successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    ${UCNo}    New Offer should be added successfully    New Offer not added successfully
    ...    Verification completed
    ${package}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Run Keyword if    '${offer}' == '${package}'    Log    Offer added successfully
    ...    ELSE    Log    Offer not added successfully
    ${MSISDN}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${MSISDN}
    Wait And Click Element    //select[@name='Reason']    40    1
    Wait And Click Element    //select[@name='Reason']/option[2]
    sleep    5s
    wait for element    id=stageAdvanceActionContainer
    Focus    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Product Configuration#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=productsConfigurationDataGridBody    60    1
    wait for element    id=orderBasket    40    1
    Sleep    10s
    Screenshots    ${Screenshotpath}    Product Configuration Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Addon Expiration#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=addonExpirationTitle    120    1
    Screenshots    ${Screenshotpath}    Add On Expiration Page
    wait for element    id=stageAdvanceActionContainer
    Focus    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Guarantee#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=guarantee    60    1
    Screenshots    ${Screenshotpath}    Guarantee Page
    wait for element    id=stageAdvanceActionContainer
    Focus    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Payments
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    ${xpathcount}    Get Matching Xpath Count    //div[@id='paymentGridBody']//button[@id='btnConfigure']
    ${increment}    Set Variable    1
    : FOR    ${INDEX}    IN RANGE    1    10
    \    wait for element    //div[@id='paymentGridBody']//tr[${increment}]//button[@id='btnConfigure']    60    1
    \    sleep    6s
    \    Focus    //div[@id='paymentGridBody']//tr[${increment}]//button[@id='btnConfigure']
    \    Double Click Element    //div[@id='paymentGridBody']//tr[${increment}]//button[@id='btnConfigure']    skip_ready=True
    \    Unselect Frame
    \    sleep    30s
    \    wait for element    id=contentIFrame0
    \    Select IFrame    id=contentIFrame0
    \    Unselect Frame
    \    wait for element    id=contentIFrame1
    \    Select IFrame    id=contentIFrame1
    \    wait for element    id=jwl_collectionmethod    60    1
    \    Unselect Frame
    \    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    Sleep    5s
    \    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    wait for element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    60    1
    \    Focus    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose
    \    Double Click Element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    skip_ready=True
    \    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save    40    1
    \    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    wait for element    id=contentIFrame0    60    2
    \    Select IFrame    id=contentIFrame0
    \    Unselect Frame
    \    wait for element    id=contentIFrame1    60    2
    \    Select IFrame    id=contentIFrame1
    \    wait for element    id=WebResource_processapp    40    1
    \    select Iframe    id=WebResource_processapp
    \    ${increment}    Evaluate    ${increment}+2
    \    Exit For Loop If    '${INDEX}' == '${xpathcount}'
    Run Keyword if    '${INDEX}' == '${xpathcount}'    Add Result    PASS    Package Migration    Payment should be configured successfully    Payment configured successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    Package Migration    Payment should be configured successfully    Payment not configured successfully
    ...    Verification completed
    wait for element    //div[@id='paymentGridBody']//tr[1]//button[@id='btnConfigure']    40    2
    Screenshots    ${Screenshotpath}    Payment Page
    wait for element    id=stageAdvanceActionContainer
    Focus    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    60    2
    Screenshots    ${Screenshotpath}    Summary Page
    ${A}    Get Current Date
    ${New_Date}    Convert Date    ${A}    result_format=%d/%m/%Y
    log    ${A}
    log many    ${New_Date}
    ${Day}    Split String    ${New_Date}    /
    log    ${Day}
    ${New_Date}    Get From List    ${Day}    0
    log    ${New_Date}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Click Element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']
    sleep    3s
    ${AllDates}    Get Matching Xpath Count    //div[@class='k-animation-container']//table//tbody//tr//td
    ${ForCount}    Evaluate    ${AllDates}+1
    : FOR    ${INDEX}    IN RANGE    1    7
    \    Comment    ${Datavaluerow}    Get Text    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]//td//a
    \    ${columnxpath}    Set Variable    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]
    \    SelectDateColumn    ${columnxpath}    ${New_Date}
    \    #Press Key    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    \\13
    wait for element    id=stageAdvanceActionContainer    40    1
    Focus    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    sleep    40s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Unselect Frame
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword if    '${status}' == 'Submitted' or '${status}' == 'Closed'    Add Result    PASS    ${UCNo}    Order Should be submitted successfully.    Order is sumbitted succesfully. The success message is :- ${Submitmessage}
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    ${UCNo}    Order Should be submitted successfully.    Order is not sumbitted succesfully. The error message is :- ${Submitmessage}
    ...    Verification completed
    Run Keyword If    '${status}' == 'Submitted'    VerifyOfferChange    ${offer}    ${MSISDN}    ${ScreenshotpathW3UAT_UC10}    ${UCNo}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyOfferChange    ${offer}    ${MSISDN}    ${ScreenshotpathW3UAT_UC10}    ${UCNo}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is :- ${Submitmessage}

VerifyOfferChange
    [Arguments]    ${offer}    ${MSISDN}    ${Screenshotpath}    ${UCNo}
    sleep    10s
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']    40    1
    Focus    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    sleep    10s
    wait for element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    10s
    wait for element    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']    40    1
    ${o}    Get Text    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']
    Log    ${o}
    Run Keyword if    '${o}' == '${offer}'    Log    Offer changed successfully
    ...    ELSE    Log    Offer not changed successfully
    Run Keyword if    '${o}' == '${offer}'    Add Result    PASS    ${UCNo}    Offer changed successfully    Offer changed successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    ${UCNo}    Offer changed successfully    Offer not changed successfully
    ...    Verification completed
    Screenshots    ${Screenshotpath}    Verification whether offer is changed
    Unselect Frame

SelectDateColumn
    [Arguments]    ${columnxpath}    ${New_Date}
    : FOR    ${INDEX2}    IN RANGE    1    8
    \    ${xpathforolumn}    Set Variable    ${columnxpath}//td[${INDEX2}]//a
    \    log    ${xpathforolumn}
    \    ${column_value}    Get Text    ${xpathforolumn}
    \    Run Keyword If    '${column_value}'=='${New_Date}'    click on element    ${xpathforolumn}
    \    Exit For Loop If    '${column_value}'=='${New_Date}'
    sleep    7s

Bulk Data Creation
    [Arguments]    ${Bulkoperation}    ${Bulkdate}    ${Bulkname}    ${Bulktext}    ${FilePath}    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Bulk Data
    Sleep    5s
    Wait And Click Element    //div[@id='jwl_operationtype']
    Select From List By Label    //select[@id='jwl_operationtype_i']    ${Bulkoperation}
    Wait And Click Element    //div[@id='jwl_scheduleddate']
    Input Text    //input[@id='DateInput']    ${Bulkdate}
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bulkoperation|NoRelationship|Form|Mscrm.Form.jwl_bulkoperation.Save']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //textarea[@id='createNote_notesTextBox']
    Input Text    //textarea[@id='createNote_notesTextBox']    ${Bulktext}
    Choose File    //button[@id='attachButton']    ${FilePath}\\BulkOrder.xlsx
    Wait And Click Element    //div[@id='doneSpacer']
    ${BulkId}=    Get Text    //div[@id='FormTitle']
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bulkoperation|NoRelationship|Form|Mscrm.Form.jwl_bulkoperation.SaveAndClose']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    ${Bulk_data1}=    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[2]/nobr
    ${Bulk_data2}=    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[3]/nobr
    ${Bulk_datalist}=    Get Text    //table[@id='gridBodyTable']/tbody/tr
    Screenshots    ${Screenshotpath}    Bulk Data Generated
    Run Keyword If    '${Bulk_data1}'=='${BulkId}'    log to console    Bulk operation trigerred sucessfully
    Run Keyword If    '${Bulk_data2}'=='${Bulkname}'    log to console    Bulk operation trigerred sucessfully
    Run Keyword If    '${Bulk_data1}'=='${BulkId}'    Add Result    PASS    UC7.4    Bulk operation trigerred sucessfully    Bulk operation has been trigerred sucessfully
    ...    Bulk Data is trigerred
    ...    ELSE    Add Result    FAIL    UC7.4    Bulk operation is not trigerred sucessfully    Bulk operation has been trigerred sucessfully
    ...    Bulk Data is not trigerred

Deactivate the customer
    [Arguments]    ${Deactivatereason}    ${DeactServiCe}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Wait And Click Element    id=tab_16_header_h2
    Wait And Click Element    id=subscriptions_header_h2
    Run Keyword And Ignore Error    Wait And Click Element    id=subscriptions_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${countofrows}    Get Matching Xpath Count    //div[@id='Subscription_divDataArea']//div[@id='gridBodyTable']/tbody/tr
    Log to Console    ${countofrows}
    ${countforloop}    Evaluate    ${countofrows}+1
    ${DeactivatedSubscriptionid}    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[5]
    Wait And Click Element    //table[@id='gridBodyTable']/tbody/tr/td[2]/div
    : FOR    ${INDEX}    IN RANGE    2    ${countforloop}
    \    ${custtobedeactivated}    Get text    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[4]/div
    \    Run keyword if    '${DeactServiCe}'=='${custtobedeactivated}'    Click Element    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[2]/div
    \    Exit For Loop If    '${DeactServiCe}'=='${custtobedeactivated}'
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button67.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button67.Button
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //textarea[@name='notesTextArea']    60    2
    send text to element    //textarea[@name='notesTextArea']    ${Deactivatereason}
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=stageAdvanceActionContainer
    [Return]    ${DeactivatedSubscriptionid}

Check if subscription is deactivated
    [Arguments]    ${DeactivatedSubscriptionid}
    ${countofrows}    Get Matching Xpath Count    //div[@id='Subscription_divDataArea']//div[@id='gridBodyTable']/tbody/tr
    Log to Console    ${countofrows}
    ${countforloop}    Evaluate    ${countofrows}+1
    : FOR    ${INDEX}    IN RANGE    1    ${countforloop}
    \    ${Deactivated_sub}    Get text    //table[@id='gridBodyTable']/tbody/tr[${index}]/td[5]/div
    \    Run Keyword If    '${Deactivated_sub}'=='${DeactivatedSubscriptionid}'    log to console    Subscription not deactivated sucessfully
    \    Exit for loop    '${Deactivated_sub}'=='${DeactivatedSubscriptionid}'
    \    ...    ELSE    log    Subscription deactivated sucessfully

GenerateaCallDetail
    [Arguments]    ${Screenshotpath}    ${Call_detail_date1}    ${Call_detail_date2}
    Run Keyword And Ignore Error    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']
    Run Keyword And Ignore Error    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Run Keyword And Ignore Error    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.CallDetailStatement.Button']
    Run Keyword And Ignore Error    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.CallDetailStatement.Button']
    Log    Selected call details
    Sleep    10s
    Wait Until Keyword Succeeds    120s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Run Keyword And Ignore Error    click on element    xpath=(.//*[normalize-space(text()) and normalize-space(.)=':'])[1]/following::span[4]
    Run Keyword And Ignore Error    click on element    //a[contains(text(),'Jul')]
    Run Keyword And Ignore Error    click on element    xpath=(.//*[normalize-space(text()) and normalize-space(.)=':'])[2]/following::span[4]
    Run Keyword And Ignore Error    click on element    //a[contains(text(),'Aug')]
    Run Keyword And Ignore Error    Wait And Click Element    //button[@id='btnSaveDates']
    Sleep    20s
    click on element    //div[@id='jwl_paymentid']/div/span
    Sleep    60s
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']/span/a/span
    Sleep    60s
    Unselect Frame
    ${msisdn_call_detail}=    Get Text    //div[@id='FormTitle']
    Screenshots    ${Screenshotpath}    Call details
    Run Keyword And Ignore Error    Wait And Click Element    //li[@id='jwl_bi_calldetailstatement|NoRelationship|Form|jwl.jwl_bi_calldetailstatement.Submit.Button']/span/a/span
    Sleep    60s
    ${BISubmitMsg}    Confirm Action
    Screenshots    ${Screenshotpath}    Status of Invoice
    Run Keyword If    '${BISubmitMsg}'=='BI Call Detail Information Submitted Successfully.'    Add Result    PASS    UC5.27_PrintCallDetail    Call Details have been generated    Call Details has to be generated
    ...    Call Details ${msisdn_call_detail} has been Generated sucessfully    Else    Add Result    Fail    UC5.27_PrintCallDetail    Call Details have not been generated
    ...    Call Details has to be generated    Call Details has not been Generated sucessfully

VerifyCallDetail
    [Arguments]    ${msisdn_call_detail}
    Open Browser    ${SIT_URL}    ie
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Wait And Click Element    //select[@id='slctPrimaryEntity']
    Click Element    //option[@value='jwl_bi_calldetailstatement']
    Unselect Frame
    Wait And Click Element    //a[@id='Mscrm.AdvancedFind.Groups.Show.Results-Large']
    Page Should Contain    Call Detail Statement Generation for ${msisdn_call_detail}

GenerateNonInvoicePayment
    [Arguments]    ${Non_Invoice_Amount}    ${Non_Invoice_discount}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_noninvoicepaymenttype']/div
    Run Keyword And Ignore Error    Select From List    //select[@id='jwl_noninvoicepaymenttype_i']    CashOn Account
    Run Keyword And Ignore Error    click on element    //div[@id='jwl_noninvoicepaymenttype']/div[2]/select
    Screenshots    ${Screenshotpath}    Non Invoice form
    sleep    5s
    wait for element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[2]
    Focus    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[2]
    Double Click Element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[2]
    Comment    Wait And Click Element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[2]
    Sleep    30s
    Run Keyword And Ignore Error    Confirm Action
    Sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Payment page
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //input[@id='jwl_collectionmethod_ledit']
    Wait And Click Element    //img[@id='jwl_collectionmethod_i']
    Wait And Click Element    //ul[@id='jwl_collectionmethod_i_IMenu']//li[1]
    Screenshots    ${Screenshotpath}    collection method adde
    Wait And Click Element    //div[@id='jwl_paymentamount_ils']
    Input Text    //input[@id='jwl_paymentamount_ils_i']    ${Non_Invoice_Amount}
    Wait And Click Element    //div[@id='jwl_discount']
    Input Text    //input[@id='jwl_discount_i']    ${Non_Invoice_discount}
    Wait And Click Element    //div[@id='jwl_discountpercentage']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Screenshots    ${Screenshotpath}    collection method added2
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    sleep    20s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Submit BI
    wait for element    //li[@id='jwl_bi_noninvoicepayment|NoRelationship|Form|jwl.jwl_bi_noninvoicepayment.NonInvoiceButton.Button']
    Choose Ok On Next Confirmation
    Click Element    //li[@id='jwl_bi_noninvoicepayment|NoRelationship|Form|jwl.jwl_bi_noninvoicepayment.NonInvoiceButton.Button']
    sleep    5s
    Wait Until Keyword Succeeds    30s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    50s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    BI Submit msg
    ${Non_Invoice_Status}    Get text    xpath=//div[@id='header_statuscode']/div/span
    Run Keyword If    '${Non_Invoice_Status}'=='Error Occured'    Log    Non Invoice Payment UnSuccessful
    ...    ELSE IF    '${Non_Invoice_Status}'=='Submitted succesful'    Log    Non Invoice Payment Successful
    Run Keyword If    '${Non_Invoice_Status}'=='Submitted Successfully'    Add Result    PASS    UC5.26_NonInvPay    BI status should be Submitted/closed    BI status is :-${Non_Invoice_Status}
    ...    BI status verification done
    ...    ELSE    Add Result    FAIL    UC5.26_NonInvPay    BI status should be Submitted/closed    BI status is :-${Non_Invoice_Status}
    ...    BI status verification done

GenerateInvoicePayment
    [Arguments]    ${MSISDN}    ${Invoice_Amount}    ${Invoice_discount}    ${Cust_name}    ${Screenshotpath}
    sleep    5s
    Wait Until Keyword Succeeds    50s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_invoicegrid
    sleep    5s
    Wait And Click Element    //input[@id='searchSubs']
    Input Text    //input[@id='searchSubs']    ${MSISDN}
    Wait And Click Element    //button[@id='searchbtn']
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //input[@type='checkbox']
    ${Open_Amount}    Get text    //div[@id='grid']/div[2]/table/tbody/tr/td[6]
    Add Result    PASS    UC5.25_InvPay    Total invoice amount should be available    Available invoice amount is :- ${Open_Amount}    Invoice amount is validated
    Wait And Click Element    //button[@id='applytop']
    Unselect Frame
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    Screenshots    ${Screenshotpath}    Invoice page
    wait for element    //div[@id='jwl_paymentid']/div/span
    Focus    //div[@id='jwl_paymentid']/div/span
    Double Click Element    //div[@id='jwl_paymentid']/div/span
    Comment    Wait And Click Element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[3]
    #Wait And Click Element    //div[@id='jwl_paymentid']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Payment page
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //input[@id='jwl_collectionmethod_ledit']
    Wait And Click Element    //img[@id='jwl_collectionmethod_i']
    Wait And Click Element    //ul[@id='jwl_collectionmethod_i_IMenu']//li[1]
    Wait And Click Element    //div[@id='jwl_paymentamount_ils']
    Input Text    //input[@id='jwl_paymentamount_ils_i']    ${Invoice_Amount}
    Wait And Click Element    //div[@id='jwl_discount']
    Input Text    //input[@id='jwl_discount_i']    ${Invoice_discount}
    Wait And Click Element    //div[@id='jwl_discountpercentage']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Screenshots    ${Screenshotpath}    Payment method selected
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    Wait Until Keyword Succeeds    50s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    BI submit page
    Wait And Click Element    //li[@id='jwl_bi_payinvoice|NoRelationship|Form|jwl.jwl_bi_payinvoice.Submit.Button']
    sleep    20s
    ${BISubmitMsg}    Run Keyword And Ignore Error    Confirm Action
    log    ${BISubmitMsg}
    sleep    1m
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    ${Non_Invoice_Status}    Get text    //div[@id='header_statuscode_d']//div//span
    ${Amount_for_verification}=    Evaluate    ${Open_Amount}-${Invoice_Amount}
    Screenshots    ${Screenshotpath}    BI status
    Run Keyword If    '${Non_Invoice_Status}'=='Error Occured'    Log    Non Invoice Payment UnSuccessful
    ...    ELSE IF    '${Non_Invoice_Status}'=='Submitted Successfully'    VerifyInvoicePayment    ${Cust_name}    ${MSISDN}    ${Amount_for_verification}
    ...    ${Screenshotpath}
    Run Keyword If    '${Non_Invoice_Status}'=='Error Occured'    Add Result    FAIL    UC5.25_InvPay    Invoice payment BI should be submitted successfully    Invoice payment BI is not submitted successfully. The error detail is :- ${BISubmitMsg}
    ...    BI submit verification done
    ...    ELSE    Add Result    PASS    UC5.25_InvPay    Invoice payment BI should be submitted successfully    Invoice payment BI is submitted successfully. The success detail is :- ${BISubmitMsg}
    ...    BI submit verification done

OfferChangePM
    [Arguments]    ${ChangeOffer}    ${PaymentType}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //table[@class='formSectionTable']//select[@id='paymentTypeSelect1']    60    2
    Wait And Click Element    //select[@id='paymentTypeSelect1']//option[contains(text(),'${PaymentType}')]
    wait for element    //div[@id='processStepsContainer']/div[3]//div[@class='processStepValue']    40    2
    wait for element    //div[@id='processStepsContainer']//div[@class='process_PmType']    20    2
    wait for element    //div[@id='processStepsContainer']//div[@class='process_PmType']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    ${CO}    Get Text    //div[@id='processStepsContainer']//div[@class='process_PmType']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${CO}
    Should Be Equal    ${CO}    ${ChangeOffer}
    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']
    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']/option[2]
    Wait And Click Element    //div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'Search')]
    wait for element    //table[@id='eligibleOffersDataGridBody']    60    2
    Wait And Click Element    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//input[@type='checkbox']
    ${offer}    Get Text    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//div[@class='dataGridCellContent']/span
    Log    ${offer}
    Wait And Click Element    //div[@id='offerChange-eligibleOffers']//span[contains(text(),'ADD TO BASKET')]
    wait for element    //div[@id='orderBasket']//table[@class='dataGridBody']    60    2
    ${offer_added}    Get Text    //div[@id='orderBasket']//table[@class='dataGridBody']/tbody/tr[1]//span[@class='lowerCellSection ng-binding']
    Log    ${offer_added}
    Should Be Equal    ${offer}    ${offer_added}
    ${package}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Run Keyword if    '${offer}' == '${package}'    Log    Offer added successfully
    ...    ELSE    Log    Offer not added successfully
    ${MSISDN}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${MSISDN}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Product Configuration#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=productsConfigurationDataGridBody    60    1
    wait for element    id=orderBasket    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Addon Expiration#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=addonExpirationTitle    120    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Guarantee#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=guarantee    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Payments
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    ${present}=    Run Keyword And Return Status    Page Should Contain Element    id=btnConfigure
    Run Keyword If    ${present}    ConfiguringPaymentPM
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    60    2
    ${A}    Get Current Date
    ${New_Date}    Convert Date    ${A}    result_format=%d/%m/%Y
    log    ${A}
    log many    ${New_Date}
    @{Day}    Split String    ${New_Date}    /
    log    ${Day}
    ${New_Date}    Get From List    ${Day}    0
    log    ${New_Date}
    Comment    ${DateForCode}    Evaluate    ${New_Date}
    click on element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']
    ${AllDates}    Get Matching Xpath Count    //div[@class='k-animation-container']//table//tbody//tr//td
    ${ForCount}    Evaluate    ${AllDates}+1
    : FOR    ${INDEX}    IN RANGE    1    7
    \    Comment    ${Datavaluerow}    Get Text    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]//td//a
    \    ${columnxpath}    Set Variable    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]
    \    SelectDateColumnPM    ${columnxpath}    ${New_Date}
    \    #Press Key    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    \\13
    Wait And Click Element    id=stageAdvanceActionContainer
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword If    '${status}' == 'Submitted'    VerifyOfferChangePM    ${offer}    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyOfferChangePM    ${offer}    ${MSISDN}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}

VerifyOfferChangePM
    [Arguments]    ${offer}
    Wait And Click Element    //div[@id='processStepsContainer']/div[1]//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']
    ${o}    Get Text    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']
    Log    ${o}
    Should Be Equal    ${o}    ${offer}
    Unselect Frame

SelectDateColumnPM
    [Arguments]    ${columnxpath}    ${New_Date}
    : FOR    ${INDEX2}    IN RANGE    1    8
    \    ${xpathforolumn}    Set Variable    ${columnxpath}//td[${INDEX2}]//a
    \    log    ${xpathforolumn}
    \    ${column_value}    Get Text    ${xpathforolumn}
    \    Run Keyword If    '${column_value}'=='${New_Date}'    click on element    ${xpathforolumn}
    \    Exit For Loop If    '${column_value}'=='${New_Date}'

ConfiguringPaymentPM
    wait for element    id=btnConfigure    60    1
    click on element    id=btnConfigure
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=jwl_collectionmethod    60    1
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    60    1
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save    40    1
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=btnConfigure    60    1

PaymentVerificationInvoice
    [Arguments]    ${INDEX}    ${Invoice_Amount}    ${Open_Amount}    ${MSISDN}
    ${Open_Amount_Invoice_doc}    Get Text    xpath=//div[@id='grid']//table//tbody//tr[${INDEX}]//td[5]
    ${new_open_amt}    Evaluate    ${Open_Amount}-${Invoice_Amount}
    Run Keyword If    '${Open_Amount_Invoice_doc}'=='${new_open_amt}'    log    Invoice payment done
    ...    ELSE    log    Invoice payment is not correctly performed

ConfiguringPaymentCOS
    Wait And Click Element    //button[@id='btnConfigure']
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp

GenerateReviewPrint
    [Arguments]    ${From_date}    ${End_date}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    #Wait And Click Element    //div[@id='jwl_invoicerequesttype']/div
    Wait And Click Element    //div[@id='jwl_invoicerequesttype']
    Sleep    10s
    Wait And Click Element    //div[@id="jwl_invoicerequesttype"]//div[contains(text(),'Invoice Request')]/parent::label/parent::div
    Sleep    2s
    Wait And Click Element    //select[@id='jwl_invoicerequesttype_i']/option[contains(text(),'Existing')]
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_InvoiceGrid
    Select IFrame    id=WebResource_InvoiceGrid
    Wait And Click Element    id=FromDate
    Input Text    id=FromDate    ${From_date}
    Wait And Click Element    id=EndDate
    Input Text    id=EndDate    ${End_date}
    Wait And Click Element    id=btnSearch
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Invoice Records
    Sleep    5s
    ${present}=    Run Keyword And Return Status    Page Should Contain Element    //div[@id='grid']/div[2]/table/tbody/tr/td[3]
    Run Keyword If    ${present}    VerifyInvoiceRecords    ${Screenshotpath}
    ...    ELSE    Add Result    FAIL    UC5.21_ReviewPrintInvoice    Review and print invoice should not be submitted successfully    Review and print invoice is not submitted successfully.
    ...    Review and print invoice is not processed

VerifyInvoiceRecords
    [Arguments]    ${Screenshotpath}
    Wait And Click Element    //div[@id='grid']/div[2]/table/tbody/tr/td[2]
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bi_reviewandprintinvoice|NoRelationship|Form|Mscrm.Form.jwl_bi_reviewandprintinvoice.Save']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_paymentid']/div/span[@otypename='jwl_payment']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Sleep    7s
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //li[2]/a[2]
    Unselect Frame
    wait for element    id=InlineDialog_Iframe
    Select IFrame    id=InlineDialog_Iframe
    Input Text    //input[@id='crmGrid_findCriteria']    Cash
    Wait And Click Element    //button[@id='butBegin']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    /li[@id='jwl_bi_reviewandprintinvoice|NoRelationship|Form|jwl.jwl_bi_reviewandprintinvoice.Buttons.submitButton']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${a}    Run Keyword And Ignore Error    Confirm Action
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${SubmitMsg}    Run Keyword And Ignore Error    Confirm Action
    Screenshots    ${Screenshotpath}    Records Submitted
    Add Result    PASS    UC5.21_ReviewPrintInvoice    Review and print invoice should be submitted successfully    Review and print invoice is submitted successfully.    Review and print invoice is processed

ChangeMSISDN
    [Arguments]    ${Changed_msisdn}    ${Screenshotpath}
    #Validate Open Invoices
    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Change MSISDN page
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']
    Run Keyword And Ignore Error    Confirm Action
    Unselect Frame
    #Blacklist Check
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']
    Run Keyword And Ignore Error    Confirm Action
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    #Select MSISDN
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    id=WebResource_allocatingmsisdn
    Select IFrame    id=WebResource_allocatingmsisdn
    Wait And Click Element    //button[@id='search']
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Get Window Titles
    Select Window    title=Resource Administration
    Wait And Click Element    css=span.k-icon.k-i-arrow-s
    Wait And Click Element    //ul[@id='msisdnCriteria_listbox']/li
    Run Keyword And Ignore Error    Input Text    //input[@id='msisdnValue']    ${Changed_msisdn}
    #temporary xpaths need to be removed later
    Wait And Click Element    //td[4]/span/span/span/span
    Wait And Click Element    //ul[@id='filterByCriteria_listbox']/li[2]
    Run Keyword And Ignore Error    Wait And Click Element    //button[@id='query']
    Run Keyword And Ignore Error    Wait And Click Element    //input[@type='checkbox']
    Screenshots    ${Screenshotpath}    New MSISDN
    Run Keyword And Ignore Error    Wait And Click Element    //button[@id='select']
    ${msisdn_search}    Run Keyword And Ignore Error    Confirm Action
    Select Window    title=BI MSISDN Change: MSISDN Change Request - Microsoft Dynamics CRM
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']
    Run Keyword And Ignore Error    Confirm Action
    Unselect Frame
    #direct Debit check
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']
    Run Keyword And Ignore Error    Confirm Action
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    #Collection
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[3]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Screenshots    ${Screenshotpath}    Payment page
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //input[@id='jwl_collectionmethod_ledit']
    Wait And Click Element    //img[@id='jwl_collectionmethod_i']
    Wait And Click Element    //ul[@id='jwl_collectionmethod_i_IMenu']//li[1]
    Wait And Click Element    //div[@id='jwl_paymentamount_ils']
    Input Text    //input[@id='jwl_paymentamount_ils_i']    1
    Screenshots    ${Screenshotpath}    Payment collections page
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    Wait Until Keyword Succeeds    50s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Confirm Action
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']
    Run Keyword And Ignore Error    Confirm Action
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    #Summary
    Wait And Click Element    //li[@id='etel_bimsisdnchange|NoRelationship|Form|etel.etel_bimsisdnchange.Submit.Command']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Msisdn change steps completed
    ${msisdn_change_alert}    Run Keyword And Ignore Error    Confirm Action
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    ${msisdn_change_status}    Get text    //div[@id='header_statuscode_d']//div//span
    Unselect Frame
    Wait And Click Element    //li[@id='etel_bimsisdnchange|NoRelationship|Form|Mscrm.Form.etel_bimsisdnchange.SaveAndClose']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword If    '${msisdn_change_status}'=='Submitted Successfully'    Add Result    PASS    UC7.1_ChangeMsisdn    ChangeMsisdn should be submitted successfully    Change MSISDN is submitted successfully. The success detail is :- ${msisdn_change_status}
    ...    Change Msisdn is processed
    ...    ELSE    Add Result    FAILED    UC7.1_ChangeMsisdn    ChangeMsisdn should be submitted successfully    ChangeMsisdn is not submitted successfully. The error detail is :- ${msisdn_change_status}
    ...    Change Msisdn is not processed
    Run Keyword If    '${msisdn_change_status}'=='Submitted Successfully'    VerifyChangeMsisdn    ${Changed_msisdn}    ${Screenshotpath}
    ...    ELSE    VerifyChangeMsisdn    ${Changed_msisdn}    ${Screenshotpath}

AddMSDPService
    [Arguments]    ${MSDPService}    ${Screenshotpath}
    ##Modify Subscription##
    Screenshots    ${Screenshotpath}    Add MSDP Service page
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='existingOfferingsGridBody']    40    1
    Wait And Click Element    //div[@id='existingproductsTitle']//span[contains(text(),'Add new Products')]
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Input Text    //input[@name='addNewMSDP_RBTProductsFilterInput']    ${MSDPService}
    Run Keyword And Ignore Error    Wait And Click Element    xpath=(//input[@type='checkbox'])[2]
    Run Keyword And Ignore Error    Wait And Click Element    //div[@id='msdp_rbtProductsArea']/div[2]/div/a/span
    Sleep    20s
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Configuration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Resources##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock    60    1
    Comment    ${present}=    Run Keyword And Return Status    Page Should Contain Element    resource id
    Comment    Run Keyword If    ${present}    AddResources    ${Sim_Number}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Sleep    10s
    ##Addon Expiration##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    wait for element    id=addonExpirationTitle    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Sleep    10s
    ##Guarantees##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    wait for element    id=guarantee
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Payment##
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Comment    ${present}=    Run Keyword And Return Status    Page Should Contain Element    id=btnConfigure
    Comment    Run Keyword If    ${present}    ConfiguringPaymentCOS
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    Run Keyword And Ignore Error    wait for element    //div[@id='summaryResourcesContainer']//table//tbody//td//input
    ${SubDate}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    Run Keyword And Ignore Error    send text to element    //div[@id='summaryResourcesContainer']//table//tbody//td//input    ${SubDate}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    40s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    120s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Submitmessage}    Confirm Action
    #Order Submitted Sucessfully
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Status of MSDP service
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword And Ignore Error    '${status}' == 'Submitted'    VerifyMSDPService    ${MSDPService}    ${Screenshotpath}
    ...    ELSE    Log    MSDP Service should be submitted successfully
    Run Keyword And Ignore Error    '${status}' == 'Closed'    VerifyMSDPService    ${MSDPService}    ${Screenshotpath}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Submitted'    Add Result    PASS    UC10.15_MSDPService    MSDP Service should be submitted successfully    MSDP Service has to be submitted sucessfully
    ...    MSDP is sucessfully submitted.
    ...    ELSE    Add Result    FAIL    UC10.15_MSDPService    MSDP Service should be submitted successfully    MSDP Service is not submitted successfully. The error detail is :- ${status}
    ...    MSDP Service is not processed

VerifyMSDPService
    [Arguments]    ${MSDPService}    ${Screenshotpath}
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button1.Button']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button13.Button']
    Sleep    40s
    Select Window    title=360 Degree View - Assets
    Page Should Contain    ${MSDPService}
    Screenshots    ${Screenshotpath}    MSDP service

AddMCBService
    [Arguments]    ${MCBService}    ${Screenshotpath}
    ##Modify Subscription##
    Screenshots    ${Screenshotpath}    Add MCB Service page
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='existingOfferingsGridBody']    40    1
    Wait And Click Element    //div[@id='existingproductsTitle']//span[contains(text(),'Add new Products')]
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Input Text    //div[@id='newProductsArea']/div[2]/div[2]/input    ${MCBService}
    Run Keyword And Ignore Error    Wait And Click Element    //input[@type='checkbox']
    Run Keyword And Ignore Error    Wait And Click Element    //div[@id='newProductsArea']/div[2]/div/a/span
    Sleep    20s
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Configuration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Resources##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    id=resourcesBlock    60    1
    Comment    ${present}=    Run Keyword And Return Status    Page Should Contain Element    resource id
    Comment    Run Keyword If    ${present}    AddResources    ${Sim_Number}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Sleep    10s
    ##Addon Expiration##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    wait for element    id=addonExpirationTitle    60    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Sleep    10s
    ##Guarantees##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Comment    wait for element    id=guarantee
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Payment##
    wait for element    id=contentIFrame0    60    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Comment    ${present}=    Run Keyword And Return Status    Page Should Contain Element    id=btnConfigure
    Comment    Run Keyword If    ${present}    ConfiguringPaymentCOS
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    Run Keyword And Ignore Error    wait for element    //div[@id='summaryResourcesContainer']//table//tbody//td//input
    ${SubDate}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    Run Keyword And Ignore Error    send text to element    //div[@id='summaryResourcesContainer']//table//tbody//td//input    ${SubDate}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    40s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    wait for element    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    120s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${Submitmessage}    Confirm Action
    #Order Submitted Sucessfully
    Unselect Frame
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Status of MCB service
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword And Ignore Error    '${status}' == 'Submitted'    VerifyMCBService    ${MCBService}    ${Screenshotpath}
    ...    ELSE    Log    MCB Service should be submitted successfully
    Run Keyword And Ignore Error    '${status}' == 'Closed'    VerifyMCBService    ${MCBService}    ${Screenshotpath}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Submitted'    Add Result    PASS    UC10.15_MCBService    MCB Service should be submitted successfully    MCB is sucessfully submitted.
    ...    MCBverification done
    ...    ELSE    Add Result    FAILED    UC10.15_MCBService    MCB Service should be submitted successfully    MCB Service is not submitted successfully. The error detail is :- ${status}
    ...    MCB Service is not processed

VerifyMCBService
    [Arguments]    ${MCBService}    ${Screenshotpath}
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button1.Button']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button13.Button']
    Sleep    40s
    Select Window    title=360 Degree View - Assets
    Page Should Contain    ${MCBService}
    Screenshots    ${Screenshotpath}    MSDP service

CMSubmit
    [Arguments]    ${Screenshotpath}
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    Wait Until Element Is Visible    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']    20s
    Focus    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']
    Choose Ok On Next Confirmation
    Click Element    id=jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button    skip_ready=${true}
    Confirm Action
    Comment    Click Element    id=jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button
    Comment    Confirm Action
    sleep    30s
    Screenshots    ${Screenshotpath}    BI Submit msg
    ${CMSubmitMsg}    Confirm Action
    ${match}    Set Variable    ${CMSubmitMsg}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC8.14_CM-CA    CM BI should submitted succesfully    CM BI is submitted succesfully, the Success Msg is :- ${CMSubmitMsg}
    ...    CM BI Verification done
    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    CM BI should submitted succesfully    CM BI is not submitted succesfully, the error Msg is :- ${CMSubmitMsg}
    ...    CM BI Verification done
    Run Keyword If    '${RETURNVALUE}'=='True'    VerifyCM    ${Screenshotpath}

VerifyCM
    [Arguments]    ${Screenshotpath}
    FrameSelect
    wait for element    //div[@id='header_process_jwl_customerid1']//div//span
    Double Click Element    //div[@id='header_process_jwl_customerid1']//div//span
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button    60s    2
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Vustomer 360 page for verification
    FrameSelect
    Wait And Click Element    id=review_financial_history_header_h2
    Unselect Frame
    Screenshots    ${Screenshotpath}    Review financial page
    FrameSelect
    wait for element    id=WebResource_review_financial_history
    Select IFrame    id=WebResource_review_financial_history
    wait for element    id=FromDate
    send text to element    id=FromDate    01/03/2018
    wait for element    id=EndDate
    ${Current_Date}    Get Current Date
    ${Current_DateInFormate}    Convert Date    ${Current_Date}    result_format=%d/%m/%Y
    send text to element    id=EndDate    ${Current_DateInFormate}
    Wait And Click Element    id=btnSearch
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Financial details

InitiateInvoiceWriteOff
    [Arguments]    ${invoice_msisdn}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_invoicegrid
    Select IFrame    id=WebResource_invoicegrid
    Input Text    //input[@id='searchSubs']    ${invoice_msisdn}
    Wait And Click Element    //button[@id='searchbtn']
    Wait And Click Element    //input[@type='checkbox']
    Wait And Click Element    //button[@id='applytop']
    Comment    ${document_id}    Get text    //div[@id='grid']/div[2]/table/tbody/tr/td[4]
    sleep    30s
    Screenshots    ${Screenshotpath}    Invoice write off selection
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bi_invoicewriteoff|NoRelationship|Form|jwl.jwl_bi_invoicewriteoff.Submit.Button']
    sleep    40s
    ${invoiceSubmitMsg}    Run Keyword And Ignore Error    Confirm Action
    sleep    40s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${Invoice_write_off_Status}    Get text    xpath=//div[@id='header_statuscode']/div/span
    Screenshots    ${Screenshotpath}    Invoice write off status
    Run Keyword If    '${Invoice_write_off_Status}'=='SubmittedSuccessfully'    Add Result    PASS    UC5.1_InvWriteOff    Invoice write off should be submitted successfully    Invoice write off is submitted successfully.
    ...    Invoice write off verification done
    ...    ELSE    Add Result    FAIL    UC5.1_InvWriteOff    Invoice write off should be submitted successfully    Invoice write off is not submitted successfully.
    ...    Invoice write off verification done

InitiateInvoiceNonWriteOff
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Reverse write off tabs
    wait for element    id=WebResource_financialhistorylist
    Select IFrame    id=WebResource_financialhistorylist
    Wait And Click Element    //div[@id='grid']/div[2]/table/tbody/tr/td[2]
    Run Keyword And Ignore Error    Confirm Action
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Confirm Action
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='header_process_jwl_paymentid']
    Wait And Click Element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[3]
    Run Keyword And Ignore Error    Confirm Action
    Unselect Frame
    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Confirm Action
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //input[@id='jwl_collectionmethod_ledit']
    Wait And Click Element    //img[@id='jwl_collectionmethod_i']
    Wait And Click Element    //li[@id='item0']/a[2]
    Screenshots    ${Screenshotpath}    Reverse writeoff Payment type
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']
    ${a}    Run Keyword And Ignore Error    Confirm Action
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${b}    Run Keyword And Ignore Error    Confirm Action
    Sleep    30s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${reverse_write_status}    Get text    //div[@id='header_statuscode_d']//div//span
    Screenshots    ${Screenshotpath}    Reverse writeoff Status
    Unselect Frame
    Run Keyword If    '${reverse_write_status}'=='Submitted Successfully'    Add Result    PASS    UC8.13_ReverseWriteoff    Reverse write off should be submitted successfully    Reverse write off is submitted successfully. The success detail is :- ${b}
    ...    Reverse write off is processed
    ...    ELSE    Add Result    FAIL    UC8.13_ReverseWriteoff    Reverse write off should be submitted successfully    Reverse write off is not submitted successfully. The error detail is :- ${b}
    ...    Reverse write off is not processed

OfferChangePreToPre
    [Arguments]    ${ChangeOffer}    ${PaymentType}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //table[@class='formSectionTable']//select[@id='paymentTypeSelect1']    60    2
    Wait And Click Element    //select[@id='paymentTypeSelect1']//option[contains(text(),'${PaymentType}')]
    wait for element    //div[@id='processStepsContainer']/div[3]//div[@class='processStepValue']    40    2
    wait for element    //div[@id='processStepsContainer']//div[@class='process_PmType']    20    2
    sleep    7s
    ${CO}    Get Text    //div[@id='processStepsContainer']//div[@class='process_PmType']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${CO}
    Run Keyword If    '${CO}' == '${ChangeOffer}'    Log    Change Offer type selected successfully.
    ...    ELSE    Log    Change Offer type not selected successfully.
    Run Keyword If    '${CO}' == '${ChangeOffer}'    Add Result    PASS    UC7.16_Package Migration Pre2Pre    Type of offer which is to be changed should be selected successfully.    Change Offer type is selected successfully.
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.16_Package Migration Pre2Pre    Type of offer which is to be changed should be selected successfully.    Change Offer type is not selected successfully.
    ...    Verification completed
    Screenshots    ${Screenshotpath}    Offer change page
    Comment    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']
    Comment    Wait And Click Element    //table[@class='formSectionTable']//select[@name='Reason']/option[2]    60    1
    Wait And Click Element    //div[@id='newCustomerSearchButtonGroup']//button[contains(text(),'Search')]
    wait for element    //table[@id='eligibleOffersDataGridBody']    60    2
    Wait And Click Element    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//input[@type='checkbox']
    ${offer}    Get Text    //table[@id='eligibleOffersDataGridBody']/tbody/tr[1]//div[@class='dataGridCellContent']/span
    Log    ${offer}
    Wait And Click Element    //div[@id='offerChange-eligibleOffers']//span[contains(text(),'ADD TO BASKET')]
    wait for element    //div[@id='orderBasket']//table[@class='dataGridBody']    60    2
    ${offer_added}    Get Text    //div[@id='orderBasket']//table[@class='dataGridBody']/tbody/tr[1]//span[@class='lowerCellSection ng-binding']
    Log    ${offer_added}
    Should Be Equal    ${offer}    ${offer_added}
    Run Keyword If    '${offer}' == '${offer_added}'    Log    New Offer added successfully
    ...    ELSE    Log    New Offer not added successfully
    Run Keyword If    '${offer}' == '${offer_added}'    Add Result    PASS    UC7.16_Package Migration Pre2Pre    New Offer should be added successfully    New Offer added successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.16_Package Migration Pre2Pre    New Offer should be added successfully    New Offer not added successfully
    ...    Verification completed
    ${package}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@class='process_accountNumber']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Run Keyword if    '${offer}' == '${package}'    Log    Offer added successfully
    ...    ELSE    Log    Offer not added successfully
    Run Keyword if    '${offer}' == '${package}'    Add Result    PASS    UC7.16_Package Migration Pre2Pre    New Offer should be added successfully    New Offer added successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.16_Package Migration Pre2Pre    New Offer should be added successfully    New Offer not added successfully
    ...    Verification completed
    ${MSISDN}    Get Text    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Log    ${MSISDN}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Product Configuration#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=productsConfigurationDataGridBody    60    1
    wait for element    id=orderBasket    40    1
    Sleep    10s
    Screenshots    ${Screenshotpath}    Product Configuration Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Addon Expiration#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=addonExpirationTitle    120    1
    Screenshots    ${Screenshotpath}    Add On Expiration Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Guarantee#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=guarantee    60    1
    Screenshots    ${Screenshotpath}    Guarantee Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Payments
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    ${xpathcount}    Get Matching Xpath Count    //div[@id='paymentGridBody']//button[@id='btnConfigure']
    ${increment}    Set Variable    1
    : FOR    ${INDEX}    IN RANGE    1    10
    \    wait for element    //div[@id='paymentGridBody']//tr[${increment}]//button[@id='btnConfigure']    40    1
    \    Focus    //div[@id='paymentGridBody']//tr[${increment}]//button[@id='btnConfigure']
    \    click on element    //div[@id='paymentGridBody']//tr[${increment}]//button[@id='btnConfigure']
    \    Unselect Frame
    \    wait for element    id=contentIFrame0
    \    Select IFrame    id=contentIFrame0
    \    Unselect Frame
    \    wait for element    id=contentIFrame1
    \    Select IFrame    id=contentIFrame1
    \    wait for element    id=jwl_collectionmethod    60    1
    \    Unselect Frame
    \    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    Sleep    5s
    \    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    Focus    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose
    \    Wait And Click Element    id=jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose    60    1
    \    wait for element    id=etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save    40    1
    \    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    wait for element    id=contentIFrame1    60    2
    \    Select IFrame    id=contentIFrame1
    \    wait for element    id=WebResource_processapp    40    1
    \    select Iframe    id=WebResource_processapp
    \    ${increment}    Evaluate    ${increment}+2
    \    Exit For Loop If    '${INDEX}' == '${xpathcount}'
    Run Keyword if    '${INDEX}' == '${xpathcount}'    Add Result    PASS    UC7.16_Package Migration Pre2Pre    Payment should be configured successfully    Payment configured successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.16_Package Migration Pre2Pre    Payment should be configured successfully    Payment not configured successfully
    ...    Verification completed
    wait for element    //div[@id='paymentGridBody']//tr[1]//button[@id='btnConfigure']    40    2
    Screenshots    ${Screenshotpath}    Payment Page
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    #Summary#
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    60    2
    Screenshots    ${Screenshotpath}    Summary Page
    Comment    ${A}    Get Current Date
    Comment    ${New_Date}    Convert Date    ${A}    result_format=%d/%m/%Y
    Comment    log    ${A}
    Comment    log many    ${New_Date}
    Comment    @{Day}    Split String    ${New_Date}    /
    Comment    log    ${Day}
    Comment    ${New_Date}    Get From List    ${Day}    0
    Comment    log    ${New_Date}
    Comment    Comment    ${DateForCode}    Evaluate    ${New_Date}
    Comment    click on element    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']
    Comment    ${AllDates}    Get Matching Xpath Count    //div[@class='k-animation-container']//table//tbody//tr//td
    Comment    ${ForCount}    Evaluate    ${AllDates}+1
    Comment    : FOR    ${INDEX}    IN RANGE    1    7
    Comment    \    Comment    ${Datavaluerow}    Get Text    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]//td//a
    Comment    \    ${columnxpath}    Set Variable    //div[@class='k-animation-container']//table//tbody//tr[${INDEX}]
    Comment    \    SelectDateColumn    ${columnxpath}    ${New_Date}
    Comment    \    #Press Key    //div[@id='offerSummaryContent']//span[@class='k-icon k-i-calendar']    \\13
    wait for element    id=stageAdvanceActionContainer    40    1
    Focus    id=stageAdvanceActionContainer
    Double Click Element    id=stageAdvanceActionContainer
    sleep    40s
    ${Submitmessage}    Confirm Action
    Log    Submit message is-${Submitmessage}
    Sleep    2m
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    id=header_jwl_status
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Run Keyword if    '${status}' == 'Submitted' or '${status}' == 'Closed'    Add Result    PASS    UC7.16_Package Migration Pre2Pre    Order Should be submitted successfully.    Order is sumbitted succesfully. The success message is \ :- ${Submitmessage}
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.16_Package Migration Pre2Pre    Order Should be submitted successfully.    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    ...    Verification completed
    Run Keyword If    '${status}' == 'Submitted'    VerifyOfferChange    ${offer}    ${MSISDN}    ${ScreenshotpathW3UAT_UC10}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}
    Run Keyword If    '${status}' == 'Closed'    VerifyOfferChange    ${offer}    ${MSISDN}    ${ScreenshotpathW3UAT_UC10}
    ...    ELSE    Log    Order is not sumbitted succesfully. The error message is \ :- ${Submitmessage}

VerifyOfferChangePreToPre
    [Arguments]    ${offer}    ${MSISDN}    ${Screenshotpath}
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']    40    1
    Focus    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Unselect Frame
    wait for element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Sleep    10s
    wait for element    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']    40    1
    ${o}    Get Text    //td[@id='etel_subscriptionofferingid_d']//span[@otypename='product']
    Log    ${o}
    Run Keyword if    '${o}' == '${offer}'    Log    Offer changed successfully
    ...    ELSE    Log    Offer not changed successfully
    Run Keyword if    '${o}' == '${offer}'    Add Result    PASS    UC7.16_Package Migration Pre2Pre    Offer changed successfully    Offer changed successfully
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    UC7.16_Package Migration Pre2Pre    Offer changed successfully    Offer not changed successfully
    ...    Verification completed
    Screenshots    ${Screenshotpath}    Verification whether offer is changed
    Unselect Frame

CMForm
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    id=WebResource_financialhistorylist
    Select IFrame    id=WebResource_financialhistorylist
    Screenshots    ${ScreenshotpathW2UAT_UC10}    Credit Memo Form
    ${CM_Present}    Run Keyword And Return Status    Page Should Contain Element    //div[@id='example']
    ${countofrows}    Get Matching Xpath Count    //div[@id='grid']//table//tbody//tr//td[@class='k-detail-cell']
    ${countforloop}    Evaluate    ${countofrows}+1
    : FOR    ${Index}    IN RANGE    1    ${countforloop}
    \    ${Document_Type}    Get Text    //div[@id='grid']//table//tbody//tr//td[@class='k-detail-cell']//table//tbody//tr[${Index}]//td[1]
    \    ${value}    Set Variable    CM
    \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${Document_Type}    ${value}
    \    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    \    Run Keyword If    '${RETURNVALUE}'=='True'    click element    //div[@id='grid']//table//tbody//tr[${Index}]
    \    Run Keyword And Ignore Error    confirm action
    \    sleep    5s
    \    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    \    Screenshots    ${Screenshotpath}    CMFormPage
    \    Run Keyword If    '${RETURNVALUE}'=='True'    CMSubmit    ${ScreenshotpathW2UAT_UC10}
    \    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    CM data should be present
    \    ...    CM data is not present    CM data verification done
    \    Exit For Loop If    '${RETURNVALUE}'=='True'
    \    Comment    Run Keyword If    ${CM_Present}    CMSubmit    ${ScreenshotpathW2UAT_UC10}
    \    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    CM data should be present
    \    ...    CM data is not present    CM data verification done

VerifyChangeMsisdn
    [Arguments]    ${Changed_msisdn}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //h2[@id='tab_5_header_h2']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${a}    Get Text    //table[@id='gridBodyTable']/tbody/tr/td[3]
    Screenshots    ${Screenshotpath}    Changed MSISDN page
    Run Keyword If    ${a}=${Changed_msisdn}    Add Result    PASS    UC7.1_ChangeMsisdn    ChangeMsisdn is completed successfully    ChangeMsisdn
    ...    is completed successfully. The Msisdn has been changed    Change Msisdn is verified
    ...    ELSE    Add Result    FAIL    UC7.1_ChangeMsisdn    ChangeMsisdn is completed successfully    ChangeMsisdn is completed successfully. The Msisdn has not been changed
    ...    Change Msisdn is not verified

SwapTheSim
    [Arguments]    ${msisdn}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Screenshots    ${Screenshotpath}    Swap Sim Page
    Run Keyword And Ignore Error    wait for element    WebResource_etel_simchange
    Run Keyword And Ignore Error    Select IFrame    WebResource_etel_simchange
    Wait And Click Element    //input[@type='text']
    Input Text    //input[@type='text']    ${msisdn}
    Wait And Click Element    //button
    Unselect Frame
    Run Keyword And Ignore Error    Wait And Click Element    //li[@id='etel_bisimchange|NoRelationship|Form|jwl.etel_bisimchange.Submit.Button']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Run Keyword And Ignore Error    Wait And Click Element    css=option[title="Damaged SIM"]
    Unselect Frame
    Run Keyword And Ignore Error    Wait And Click Element    //li[@id='etel_bisimchange|NoRelationship|Form|Mscrm.Form.etel_bisimchange.Save']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Wait And Click Element    //div[@id='jwl_paymentid']/div/span
    Run Keyword And Ignore Error    Confirm Action
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Payment page
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']    60s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Wait And Click Element    //li[@id='etel_bisimchange|NoRelationship|Form|jwl.etel_bisimchange.Submit.Button']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Confirm Action
    Screenshots    ${Screenshotpath}    Swap sim Status
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='header_statuscode']    40s
    ${msisdn_change_status}    Get Text    xpath=//div[@id="header_statuscode"]//div//span
    Run Keyword And Ignore Error    Wait And Click Element    //li[@id='etel_bisimchange|NoRelationship|Form|Mscrm.Form.etel_bisimchange.SaveAndClose']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword If    '${msisdn_change_status}'=='Submitted Successfully'    Add Result    PASS    UC7.28_SwapSim    Swap Sim should be submitted successfully    Swap Sim
    ...    is submitted successfully. The success detail is :- ${msisdn_change_status}    Swap Sim is processed
    ...    ELSE    Add Result    FAIL    UC7.28_Swap Sim    Swap Sim should be submitted successfully    Swap Sim is not submitted successfully. The error detail is :- ${msisdn_change_status}
    ...    Swap Sim is not processed
    Run Keyword If    '${msisdn_change_status}'=='Submitted Successfully'    VerifySwapSim    ${msisdn}    ${Screenshotpath}
    ...    ELSE    Log To Console    Swap Sim has failed

VerifySwapSim
    [Arguments]    ${msisdn}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Wait And Click Element    //h2[@id='tab_5_header_h2']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${a}    Get Text    //table[@id='gridBodyTable']/tbody/tr[2]/td[3]
    Screenshots    ${Screenshotpath}    Swap sim page
    Run Keyword If    '${a}'=='${msisdn} '    Log To Console    Swap Sim has passed
    ...    ELSE    Log To Console    Swap Sim has failed
    Run Keyword If    '${a}'=='${msisdn} '    Add Result    PASS    UC7.28_SwapSim    Swap Sim should be successfully    Swap Sim
    ...    is done successfully.    Swap Sim is successfull
    ...    ELSE    Add Result    FAIL    UC7.28_Swap Sim    Swap Sim should be successfully    Swap Sim is not successfull.
    ...    Swap Sim is not successfull

AddTwinSim
    [Arguments]    ${MCBService}    ${Screenshotpath}    ${Sim_Number}
    ##Modify Subscription##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    wait for element    //div[@id='existingOfferingsGridBody']    40    1
    Wait And Click Element    //div[@id='existingproductsTitle']//span[contains(text(),'Add new Products')]
    Run Keyword And Ignore Error    wait for element    //div[@id='newProductsArea']//input[@name='addNewProductsFilterInput']    60    1
    Run Keyword And Ignore Error    send text to element    //div[@id='newProductsArea']//input[@name='addNewProductsFilterInput']    ${MCBService}
    Run Keyword And Ignore Error    Wait And Click Element    //table[@id='configDataGridBody']//span[contains(text(),'- Prepaid Twin SIM')]
    Run Keyword And Ignore Error    Wait And Click Element    //div[@id='newProductsArea']//span[contains(text(),'ADD TO BASKET')]
    Screenshots    ${Screenshotpath}    Order twin sim
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    ##Configuration##
    Sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Resources##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Run Keyword And Ignore Error    Input Text    //input[@type='text']    ${Sim_Number}
    Run Keyword And Ignore Error    Click Element    //button[@name='assingSIM']
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Addon Expiration##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Guarantees##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    10s
    Unselect Frame
    ##Payment##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    select Iframe    id=WebResource_processapp
    wait for element    id=divPayments    40    1
    Run Keyword And Ignore Error    ConfiguringPaymentCOS
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Sleep    10s
    Unselect Frame
    ##Summary##
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp    40    1
    select Iframe    id=WebResource_processapp
    Comment    wait for element    id=stageAdvanceActionContainer
    Comment    Double Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    120s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    Status of MSDP service
    ${status}    Get Text    //div[@id='header_jwl_status']/div/span
    Screenshots    ${Screenshotpath}    Status of Twin sim
    Run Keyword If    '${status}' == 'Submitted'    Add Result    PASS    UC10.13_ChangeTwinSim    Change Twin Sim should be submitted successfully    Change Twin Sim is submitted successfully.
    ...    Change Twin Sim is done
    ...    ELSE    Add Result    FAIL    UC10.13_ChangeTwinSim    Change Twin Sim should be submitted successfully    Change Twin Sim is not submitted successfully.
    ...    Change Twin Sim is done

VerifyTwinSim
    [Arguments]    ${MCBService}    ${Screenshotpath}
    wait for element    id=WebResource_processapp    60    2
    select Iframe    id=WebResource_processapp
    ${RatePlan}    Get Text    //div[@id='processStepsContainer']/div[2]//div[@id='processStep_corporateCustomer']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    wait for element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    Double Click Element    //div[@id='processStepsContainer']/div[1]//div[@id='processStep_AccountNo']//div[@class='processStepValueText processStepLookupValueText ng-binding']
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    wait for element    id=contentIFrame0    40    1
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=tab_7_header_h2    60    1
    Wait And Click Element    id=tab_7_header_h2
    Select IFrame    id=WebResource_productsview
    Screenshots    ${Screenshotpath}    Twin sim added successully
    ${count}=    Set Variable    1
    : FOR    ${INDEX}    IN RANGE    1    20
    \    Log    ${count}
    \    ${a}    Get Text    //div[@id='planningPlanSection']//tr[${count}]/td[1]/div/span[2]
    \    Run Keyword If    '${a}' == ' ${MCBService}'    Add Result    PASS    UC10.13_ChangeTwinSim    Change Twin Sim should be completed successfully
    \    ...    Change Twin Sim is completed successfully.    Change Twin Sim is done
    \    ...    ELSE    Add Result    FAIL    UC10.13_ChangeTwinSim    Change Twin Sim should be completed successfully
    \    ...    Change Twin Sim is not completed successfully.    Change Twin Sim is done
    \    Exit For Loop If    '${a}' == ' ${MCBService}'
    \    ${count}=    Evaluate    ${count}+4

InitiateFCPayment
    [Arguments]    ${Screenshotpath}    ${Date1}    ${Date2}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    Comment    Screenshots    ${Screenshotpath}    FC Payment screen
    Focus    //div[@id='header_process_jwl_paymentid_d']//div//span
    Double Click Element    //div[@id='header_process_jwl_paymentid_d']//div//span
    Comment    Wait And Click Element    xpath=(//span[@onclick='Mscrm.ReadFormUtilities.openLookup(true, new Sys.UI.DomEvent(event));'])[3]
    Comment    Run Keyword And Ignore Error    Confirm Action
    Unselect Frame
    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Run Keyword And Ignore Error    Confirm Action
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='jwl_collectionmethod']
    Wait And Click Element    //input[@id='jwl_collectionmethod_ledit']
    Wait And Click Element    //img[@id='jwl_collectionmethod_i']
    Run Keyword And Ignore Error    Wait And Click Element    //ul[@id='jwl_collectionmethod_i_IMenu']//a[@title='Cash']
    sleep    5s
    Screenshots    ${Screenshotpath}    FC Payment type
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_payment|NoRelationship|Form|Mscrm.Form.jwl_payment.SaveAndClose']
    sleep    20s
    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    sleep    10s
    Choose Ok On Next Confirmation
    Click Element    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']    skip_ready=True
    Confirm Action
    Comment    sleep    10s
    Comment    sleep    30s
    Comment    Confirm Action
    ${a}    Run Keyword And Ignore Error    Confirm Action
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${b}    Run Keyword And Ignore Error    Confirm Action
    sleep    20s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${FC_Payment_status}    Get text    //div[@id='header_statuscode_d']//div//span
    Screenshots    ${Screenshotpath}    FC Payment Status
    Unselect Frame
    Wait And Click Element    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|Mscrm.Form.jwl_bi_financialtransaction.SaveAndClose']
    sleep    10s
    Comment    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword If    '${FC_Payment_status}'=='Submitted Successfully'    Add Result    PASS    FC_Payment    FC_Payment should be submitted successfully    FC_Payment
    ...    is submitted successfully. The success detail is :- ${FC_Payment_status}    FC_Payment is processed
    ...    ELSE    Add Result    FAILED    FC_Payment    FC_Payment should be submitted successfully    FC_Payment is not submitted successfully. The error detail is :- ${FC_Payment_status}
    ...    FC_Payment is not processed
    Run Keyword If    '${FC_Payment_status}'=='Submitted Successfully'    VerifyFCPaymentWriteoff    ${Screenshotpath}    ${Date1}    ${Date2}
    ...    ELSE    Log To Console    FC Payment has failed

InitiateFCWriteoff
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Screenshots    ${Screenshotpath}    FC Writeoff screen
    Select IFrame    id=WebResource_financialhistorylist
    ${CM_Present}    Run Keyword And Return Status    Page Should Contain Element    id=example
    ${countofrows}    Get Matching Xpath Count    //div[@id='grid']//table//tbody//tr//td[@class='k-detail-cell']
    ${countforloop}    Evaluate    ${countofrows}+1
    : FOR    ${Index}    IN RANGE    1    ${countforloop}
    \    ${Document_Type}    Get Text    //div[@id='grid']//table//tbody//tr//td[@class='k-detail-cell']//table//tbody//tr[${Index}]//td[1]
    \    ${value}    Set Variable    FC
    \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${Document_Type}    ${value}
    \    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    \    Run Keyword If    '${RETURNVALUE}'=='True'    click element    //div[@id='grid']//table//tbody//tr[${Index}]
    \    Run Keyword And Ignore Error    confirm action
    \    Exit For Loop If    '${RETURNVALUE}'=='True'
    Run Keyword And Ignore Error    confirm action
    sleep    8s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@id='stageAdvanceActionContainer']/div
    Double Click Element    //div[@id='stageAdvanceActionContainer']/div
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|Mscrm.Form.jwl_bi_financialtransaction.SaveAndClose']    40    1
    sleep    5s
    wait for element    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']
    Click Element    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']    skip_ready=True
    sleep    5s
    Comment    Choose Ok On Next Confirmation
    ${a}    Confirm Action
    sleep    30s
    ${b}    Confirm Action
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    ${FC_writeoff_status}    Get text    //div[@id='header_statuscode_d']//div//span
    Unselect Frame
    Screenshots    ${Screenshotpath}    FC Writeoff Status
    wait for element    //div[@id=’processStepColumn_0_1’]//div[@id=’processStep_jwl_customerid1’]//div[@id=’header_process_jwl_customerid1_d’]
    Double Click Element    //div[@id=’processStepColumn_0_1’]//div[@id=’processStep_jwl_customerid1’]//div[@id=’header_process_jwl_customerid1_d’]
    Wait Until Keyword Succeeds    90s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword If    '${FC_Writeoff_status}'=='Submitted Successfully'    Add Result    PASS    FC_Writeoff    FC_Writeoff should be submitted successfully    FC_Writeoff
    ...    is submitted successfully. The success detail is :- ${FC_Writeoff_status}    FC_Writeoff is processed
    ...    ELSE    Add Result    FAILD    FC_Writeoff    FC_Writeoff should be submitted successfully    FC_Writeoff is not submitted successfully. The error detail is :- ${FC_Writeoff_status}
    ...    FC_Writeoff is not processed
    Run Keyword If    '${FC_Writeoff_status}'=='Submitted Successfully'    VerifyFCPaymentWriteoff    ${Screenshotpath}
    ...    ELSE    Log To Console    FC Writeoff has failed

VerifyFCPaymentWriteoff
    [Arguments]    ${Screenshotpath}
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button    60s    2
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Vustomer 360 page for verification
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=review_financial_history_header_h2
    wait for element    //div[@id='review_financial_history_header_image_div']//img
    Focus    //div[@id='review_financial_history_header_image_div']//img
    Click on element    //div[@id='review_financial_history_header_image_div']//img
    wait for element    id=WebResource_review_financial_history
    Select IFrame    id=WebResource_review_financial_history
    wait for element    id=FromDate
    wait for element    id=EndDate
    ${Current_Date}    Get Current Date
    ${Current_DateInFormate}    Convert Date    ${Current_Date}    result_format=%d/%m/%Y
    send text to element    id=FromDate    ${Current_DateInFormate}
    send text to element    id=EndDate    ${Current_DateInFormate}
    Wait And Click Element    id=btnSearch
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${countofrows}    Get Matching Xpath Count    //table[@id='financialHistGrid']//tbody//tr
    Run Keyword If    '${countofrows}' > '1'    Log    FC entry is present
    ...    ELSE    Log    FC entry is not present
    Run Keyword If    '${countofrows}' > '1'    Add Result    PASS    UC8.16 FCWriteOff    FC WriteOff entry should be present.    FC WriteOff entry is present.
    ...    FC WriteOff \ Verification done
    ...    ELSE    Add Result    FAILED    UC8.16 FC WriteOff    FC WriteOff entry should be present.    FC WriteOff entry is not present.
    ...    FC WriteOff Verification done
    Screenshots    ${Screenshotpath}    Financial details

Submit Cash on Account
    [Arguments]    ${Screenshotpath}
    wait for element    ${OR_SaveandClose_COA}    50    1
    sleep    7s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    ${OR_FinencialHistoryList_COA}    40    1
    Select IFrame    ${OR_FinencialHistoryList_COA}
    Screenshots    ${Screenshotpath}    Cash on Account Form
    ${CM_Present}    Run Keyword And Return Status    Page Should Contain Element    ${OR_Example_COA}
    ${countofrows}    Get Matching Xpath Count    ${OR_CashOnAccountCount_COA}
    ${countforloop}    Evaluate    ${countofrows}+1
    : FOR    ${Index}    IN RANGE    1    ${countforloop}
    \    ${Document_Type}    Get Text    ${OR_CashOnAccountName_COA}
    \    ${value}    Set Variable    CO
    \    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${Document_Type}    ${value}
    \    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    \    Run Keyword If    '${RETURNVALUE}'=='True'    click element    ${OR_CashOnAccountRow_COA}
    \    Run Keyword And Ignore Error    confirm action
    \    Run Keyword If    '${RETURNVALUE}'=='True'    CMSubmit    ${Screenshotpath}
    \    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    CM data should be present
    \    ...    CM data is not present    CM data verification done
    \    Exit For Loop If    '${RETURNVALUE}'=='True'
    \    Run Keyword If    ${CM_Present}    CMSubmit    ${ScreenshotpathW2UAT_UC10}
    \    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    CM data should be present
    \    ...    CM data is not present    CM data verification done

COSubmit
    [Arguments]    ${Screenshotpath}
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    Wait Until Element Is Visible    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']    20s
    Focus    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']
    Choose Ok On Next Confirmation
    Click Element    id=jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button    skip_ready=${true}
    Confirm Action
    Comment    Click Element    id=jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button
    Comment    Confirm Action
    sleep    15s
    Comment    Screenshots    ${Screenshotpath}    BI Submit msg
    ${CMSubmitMsg}    Confirm Action
    ${match}    Set Variable    ${CMSubmitMsg}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC8.14_CM-CA    Cash on Account BI should submitted succesfully    Cash on Account BI is submitted succesfully, the Success Msg is :- ${CMSubmitMsg}
    ...    CO BI Verification done
    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    Cash on Account BI should submitted succesfully    Cash on Account BI is not submitted succesfully, the Success Msg is :- ${CMSubmitMsg}
    ...    CO BI Verification done
    Run Keyword If    '${RETURNVALUE}'=='True'    VerifyCM    ${Screenshotpath}

VerifyCO
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    ${OR_CustomerName_COA}
    Double Click Element    ${OR_CustomerName_COA}
    Unselect Frame
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    ${OR_Actions_COA}    60s    2
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Vustomer 360 page for verification
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    ${OR_RFH_COA}
    wait for element    ${OR_RFH_Image_COA}
    Focus    ${OR_RFH_Image_COA}
    Click on element    ${OR_RFH_Image_COA}
    wait for element    ${OR_RFH_Frame_COA}
    Select IFrame    ${OR_RFH_Frame_COA}
    wait for element    ${OR_FromDate_COA}
    wait for element    ${OR_EndDate_COA}
    ${Current_Date}    Get Current Date
    ${Current_DateInFormate}    Convert Date    ${Current_Date}    result_format=%d/%m/%Y
    send text to element    ${OR_FromDate_COA}    ${Current_DateInFormate}
    send text to element    ${OR_EndDate_COA}    ${Current_DateInFormate}
    Wait And Click Element    ${OR_SearchButton_COA}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ${countofrows}    Get Matching Xpath Count    ${OR_CashOnAccountEntry_COA}
    Run Keyword If    '${countofrows}' > '1'    Log    CO entry is present
    ...    ELSE    Log    CO entry is not present
    Run Keyword If    '${countofrows}' > '1'    Add Result    PASS    UC8.14_CM-CA    Cash on Account entry should be present.    Cash on Account entry is present.
    ...    CO BI Verification done
    ...    ELSE    Add Result    FAIL    UC8.14_CM-CA    Cash on Account entry should be present.    Cash on Account entry is not present.
    ...    CO BI Verification done
    Screenshots    ${Screenshotpath}    Financial details

ChangeOwnership-CustomerRequest
    [Arguments]    ${National_Id}    ${Cust_Name}    ${msisdn}    ${Screenshotpath}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Screenshots    ${Screenshotpath}    Change Ownership page
    Wait And Click Element    //select[@ng-change='changeCustomerType()']    60    2
    Wait And Click Element    //select[@ng-change='changeCustomerType()']//option[contains(text(),'Individual')]
    sleep    3s
    Wait And Click Element    id=selectSearchTypesId
    sleep    3s
    Wait And Click Element    //select[@id='selectSearchTypesId']//option[contains(text(),'National Id')]
    sleep    3s
    Wait And Click Element    id=param1Id
    send text to element    id=param1Id    ${National_Id}
    Wait And Click Element    id=btn_searchCustomer
    sleep    7s
    Click Element    //div[@id='existingOfferingsGridBody']//table//tbody//tr//td//div//input[@type='checkbox']
    sleep    30s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Screenshots    ${Screenshotpath}    Changing Ownership
    Comment    Log    ${name}
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    ##Open Invoices##
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    #wait for element    //div[@class='wizard-body ng-scope']//p[starts-with(.,'Subscription')]    40    1
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    ##Installment##
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    ##Guarantee##
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    #Wait And Click Element    xpath=(//input[@name='{"__metadata":{"type":"Microsoft.Crm.Sdk.Data.Services.EntityReference"},"Id":"cbd4e156-2249-e811-80d9-00155d9ebf65","LogicalName":"jwl_guarantee","Name":"GRT00117","RowVersion":null}'])[2]
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame
    Wait And Click Element    //li[@id='etel_ordercapture|NoRelationship|Form|Mscrm.Form.etel_ordercapture.Save']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    ##Payments#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    ${present}=    Run Keyword And Return Status    Page Should Contain Element    id=btnConfigure
    Run Keyword If    ${present}    ConfiguringPaymentCOS
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    ##User Profile##
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    //select[@name='language']
    Wait And Click Element    //select[@name='language']//option[contains(text(),'English')]
    Wait And Click Element    id=stageAdvanceActionContainer
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    ##Summary##
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait for element    //div[@class='wizard-body ng-scope']
    Wait And Click Element    //div[@class='wizard-body ng-scope']//select[contains(@ng-model,'selectReasons')]
    Wait And Click Element    //div[@class='wizard-body ng-scope']//option[contains(text(),'Customer request')]
    sleep    5s
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Screenshots    ${Screenshotpath}    Change of ownership REason
    Unselect Frame
    #Submit Button#
    sleep    10s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=WebResource_processapp
    select Iframe    id=WebResource_processapp
    Wait And Click Element    id=stageAdvanceActionContainer
    Sleep    30s
    Confirm Action
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    id=header_jwl_status    40    1
    sleep    10s
    ${a}    Get Text    //div[@id='header_jwl_status']/div[1]/span
    Run Keyword If    '${a}' =='Submitted'    log    Order Submitted successfully
    Run Keyword If    '${a}' == 'Closed'    Log    order submitted successfully
    Run Keyword If    '${a}' =='Submitted'    Add Result    PASS    UC7.8_ChangeOwnership    Change Ownership should be submitted successfully    Change Ownership is submitted successfully.
    ...    Change Ownership is done
    ...    ELSE    Add Result    FAILED    UC7.8_ChangeOwnership    Change Ownership should be submitted successfully    Change Ownership is not submitted successfully.
    ...    Change Ownership is done
    Unselect Frame
    Comment    Run Keyword And Ignore Error    wait for element    id=crmRibbonManager    60    2
    sleep    10s
    wait for element    //span[@id='TabSearch']//input[@id='search']
    send text to element    //span[@id='TabSearch']//input[@id='search']    ${msisdn}
    Wait And Click Element    //span[@id='TabSearch']//img[@id='findCriteriaImg']
    sleep    10s
    Comment    wait for element    id=crmRibbonManager    40    2
    wait for element    id=contentIFrame0    40    2
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='contentResult']//div[starts-with(@id,'Record_')]/div    40    2
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    ${n}    Get Text    //div[@id='etel_individualcustomerid']//span[@otypename='contact']
    log    ${n}
    Screenshots    ${Screenshotpath}    Change Ownership Status
    Run Keyword If    '${n}'=='${Cust_Name}'    Add Result    PASS    UC7.8_ChangeOwnership    Change Ownership should be successfull    Change Ownership is successfull.
    ...    Change Ownership is done
    ...    ELSE    Add Result    FAILED    UC7.8_ChangeOwnership    Change Ownership should be successfull    Change Ownership is not successfull
    ...    Change Ownership is done
    Unselect Frame

CreateUsageHistory
    [Arguments]    ${No_of_days}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Usage History page
    FrameSelect
    wait for element    //div[@id='jwl_reason']
    click on element    //div[@id='jwl_reason']
    wait for element    //div[@id='jwl_reason']//select[@id='jwl_reason_i']//option[@title='Campaigns']
    click on element    //div[@id='jwl_reason']//select[@id='jwl_reason_i']//option[@title='Campaigns']
    wait for element    //div[@id='jwl_datefrom']
    click on element    //div[@id='jwl_datefrom']
    ${Current_Date}    Get Current Date
    ${Current_DateInFormate}    Convert Date    ${Current_Date}    result_format=%d/%m/%Y
    ${End_Date}    Add Time To Date    ${Current_Date}    ${No_of_days}
    ${End_Date_Final}    Convert Date    ${End_Date}    result_format=%d/%m/%Y
    send text to element    //input[@id='DateInput']    ${End_Date_Final}
    wait for element    //div[@id='jwl_dateto']
    click on element    //div[@id='jwl_dateto']
    send text to element    //input[@id='DateInput']    ${Current_DateInFormate}
    wait for element    //button[@id='search']
    click on element    //button[@id='search']
    Sleep    30s
    Screenshots    ${Screenshotpath}    Usage History page

NavigateandExecuteToCallBarringPassword
    [Arguments]    ${Screenshotpath}
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button15.Button']
    Screenshots    ${Screenshotpath}    Reset Call barring confirmation alert
    Confirm Action
    Screenshots    ${Screenshotpath}    Reset Call barring completed
    Alert Should Be Present    "Reset call barring password" submitted successfully
    Confirm Action
