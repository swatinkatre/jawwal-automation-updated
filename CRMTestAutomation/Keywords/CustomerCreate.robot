*** Settings ***
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library
Library           FakerLibrary
Library           DateTime
Library           ../CustomLibrary/UpdateExcelSheet.py

*** Keywords ***
ClickNewIndividual
    Wait And Click Element    xpath=//*[@id="navTabGlobalCreateImage"]
    Wait And Click Element    xpath=//*[@id="{608861bc-50a4-4c5f-a02c-21fe1943e2cf}"]/span[2]
    #Unselect Frame

CreateIndividualForm
    ##### Navigate to the frame where we will insert all the manditory parameters ####
    Select IFrame    id=NavBarGloablQuickCreate
    ##### First Name Data ####
    Wait And Click Element    xpath=//div[@id='firstname']/div
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=firstname_i    ${FirstName}
    Wait And Click Element    id=lastname
    ##### Last Name Data ####
    ${LastName}    FakerLibrary.Last Name Male
    send text to element    id=lastname_i    ${LastName}
    ##### ID Type ###
    Wait And Click Element    //div[@id='jwl_idtypeindividual']/div
    click on element    xpath=//select/option[contains(text(),'${ID_Type}')]
    Wait And Click Element    //div[@id='governmentid']/div
    ${ID_Type_Value}    FakerLibrary.Random Int
    send text to element    id=governmentid_i    ${ID_Type_Value}
    Wait And Click Element    //div[@id='jwl_accounttype']/div
    #Select From List By Label    id=jwl_accounttype_i    label=Individual
    click on element    xpath=//select/option[contains(text(),'${Customer_category}')]
    Unselect Frame
    wait for element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    focus    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    click on element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    Comment    Close All Browsers
    [Return]    ${FirstName}

ClickNewCorporate
    Wait And Click Element    xpath=//*[@id="navTabGlobalCreateImage"]
    Wait And Click Element    xpath=//a[@id='{70816501-edb9-4740-a16c-6a5efbc05d84}']/span[2]

CreateCorporateForm
    #### Select Frame to enter details ###
    Select IFrame    id=NavBarGloablQuickCreate
    #### Enter Corporate Name ####
    Wait And Click Element    xpath=//div[@id='name']/div
    ${CorporateName}    FakerLibrary.Company
    send text to element    id=name_i    ${CorporateName}
    #### Select Corporate from "Customer Category" ###
    Wait And Click Element    css=input#jwl_firstname_i
    click on element    xpath=//select[@id='jwl_accounttype_i']/option[contains(text(),'${Corp_Customer_Category}')]
    #Wait And Click Element    id=jwl_accounttype_cl
    #Wait And Click Element    //option[@value='1']
    #### Select ID type    ####
    Wait And Click Element    //div[@id='jwl_idtypecorporate']/div/span
    click on element    xpath=//select/option[contains(text(),'${Corp_ID_Type}')]
    ##### Enter random geneated ID ###
    ${CorpID_Type_Value}    FakerLibrary.Random Int
    click on element    //div[@id='etel_taxnumber']/div
    send text to element    id=etel_taxnumber_i    ${CorpID_Type_Value}
    ### Select communication method ####
    Wait and click Element    //div[@id='jwl_preferredchannel']/div
    click on element    xpath=//select/option[contains(text(),'${Corp_Commu_method}')]
    Unselect Frame
    #### save the changes ###
    wait for element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    focus    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    click on element    id=globalquickcreate_save_button_NavBarGloablQuickCreate
    #
    #Wait And Click Element    xpath=(//option[@value='1'])[2]
    #Wait And Click Element    //button[@id='globalquickcreate_save_button_NavBarGloablQuickCreate']
    [Return]    ${CorporateName}

VerifyNewCustomerCreated
    [Arguments]    ${Name}
    Wait And Click Element    //div[@id='navStatusArea']/table/tbody/tr/td[3]/a/span
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ${pass}    Run Keyword And Return Status    Element Should Be Visible    xpath=//h1[contains(text(),'${Name}')]
    Run Keyword If    ${pass}    log    Individual customer is created succesfully
    ...    ELSE    Individual customer is not created succesfully
    Unselect Frame

NewCustomerBICustomerInfoPage
    [Arguments]    ${Screenshotpath}
    sleep    30s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    Click Element    xpath=//a[@id='btnIndividualCustomerAcquisition']/span/span/span
    sleep    30s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #wait for element    //div[@id='stage_0']/div[2]/div/div/div/span
    ##### First Name Data ####
    wait for element    id=jwl_firstname
    Wait Until Element Is Visible    id=jwl_firstname    40s
    click on element    //div[@id='jwl_firstname']
    ${FirstName}    First Name Male
    Sleep    10s
    send text to element    id=jwl_firstname_i    ${FirstName}
    Write Excel    ${FilePath}/InputData.xls    0    1    2    ${FirstName}
    ### Last name data ####
    Wait And Click Element    id=jwl_lastnamecompanyname
    ${LastName}    FakerLibrary.Last Name Male
    send text to element    id=jwl_lastnamecompanyname_i    ${LastName}
    sleep    10s
    Write Excel    ${FilePath}/InputData.xls    0    1    3    ${LastName}
    ### Gender data ####
    wait for element    //div[@id='jwl_gender']
    Sleep    10s
    click on element    //div[@id='jwl_gender']/div[1]
    Sleep    10s
    click on element    //select[@id='jwl_gender_i']/option[contains(text(),'Male')]
    ###Date of Birth###
    wait for element    //div[@id='jwl_dateofbirth']
    Sleep    5s
    Click Element    //div[@id='jwl_dateofbirth']
    Sleep    5s
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d/%m/%Y
    Sleep    5s
    send text to element    //div[@id='jwl_dateofbirth']//input[@id='DateInput']    ${date1}
    ###### customer category ####
    wait and click element    id=jwl_customercategory
    #Select From List By Label    id=jwl_customercategory_i    label=Individual
    click on element    xpath=//select[@id='jwl_customercategory_i']/option[@title='Individual']
    Write Excel    ${FilePath}/InputData.xls    0    1    4    Individual
    ##### ID Type ###
    Wait And Click Element    id=jwl_idtypeid
    wait and click element    id=jwl_idtypeid_i
    Wait And Click Element    id=Dialog_jwl_idtypeid_i_IMenu
    Run Keyword And Ignore Error    Click Element    xpath=//select[@id='jwl_idtype_i']/option[@title='Passport Number']
    ##### ID Type value ###
    Wait And Click Element    id=jwl_idnumber
    Wait Until Element Is Visible    id=jwl_idnumber    20s
    ${ID_Type_Value}    FakerLibrary.Random Int
    Sleep    5s
    Input Text    xpath=//input[@id='jwl_idnumber_i']    ${ID_Type_Value}
    Write Excel    ${FilePath}/InputData.xls    0    1    5    ${ID_Type_Value}
    ####Language ###
    Wait And Click Element    id=jwl_languagecode
    click on element    xpath=//select[@id='jwl_languagecode_i']/option[@title='English']
    Write Excel    ${FilePath}/InputData.xls    0    1    6    English
    ####Main phone number####
    Wait And Click Element    id=jwl_mainphone
    ${a}    Generate Random String    8    [NUMBERS]
    log    ${a}
    ${MobileNumber} =    Catenate    SEPARATOR=    0    ${a}
    send text to element    id=jwl_mainphone_i    ${MobileNumber}
    Write Excel    ${FilePath}/InputData.xls    0    1    7    ${MobileNumber}
    Wait And Click Element    id=jwl_placeofwork
    ${placeofwork}    FakerLibrary.City
    send text to element    id=jwl_placeofwork_i    ${placeofwork}
    Write Excel    ${FilePath}/InputData.xls    0    1    8    ${placeofwork}
    Wait And Click Element    id=jwl_placeofbirth
    ${placeofbirth}    FakerLibrary.City
    send text to element    id=jwl_placeofbirth_i    ${placeofbirth}
    Write Excel    ${FilePath}/InputData.xls    0    1    9    ${placeofbirth}
    ### Customer Type ###
    Wait And Click Element    id=jwl_profileclassificationid
    Wait And Click Element    id=jwl_profileclassificationid_i
    sleep    1s
    Wait And Click Element    //ul[@id='jwl_profileclassificationid_i_IMenu']//a[@title='A']
    Comment    Wait And Click Element    id=jwl_profileclassificationid_i_IMenu
    click on element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
    wait and click element    id=stageAdvanceActionContainer
    sleep    10s
    Unselect Frame
    Screenshots    ${Screenshotpath}    New Customer Cust Info Page
    [Return]    ${FirstName}

NewCustomerBIAddressInfoPage
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//div[@id='jwl_address1']/div
    Comment    ${address1}    FakerLibrary.Address
    Comment    send text to element    id=jwl_address1_i    ${address1}
    send text to element    id=jwl_address1_i    Test1
    Write Excel    ${FilePath}/InputData.xls    6    1    0    Test1
    Wait And Click Element    xpath=//div[@id='jwl_address2']/div
    Comment    ${address2}    FakerLibrary.Address
    Comment    send text to element    id=jwl_address2_i    ${address2}
    send text to element    id=jwl_address2_i    Test2
    Write Excel    ${FilePath}/InputData.xls    6    1    1    Test2
    Wait And Click Element    xpath=//div[@id='jwl_address3']/div
    Comment    ${address3}    FakerLibrary.Address
    Comment    send text to element    id=jwl_address3_i    ${address3}
    send text to element    id=jwl_address3_i    Test3
    Write Excel    ${FilePath}/InputData.xls    6    1    2    Test3
    Wait And Click Element    id= jwl_cityid
    Wait And Click Element    id=jwl_cityid_i
<<<<<<< HEAD
    Run Keyword And Ignore Error    Wait And Click Element    //ul[@id='jwl_cityid_i_IMenu']//a[@title='Abasan al-Kabira']
    Comment    ${address3}    FakerLibrary.Address
    Comment    send text to element    id=jwl_address3_i    ${address3}
    send text to element    id=jwl_address3_i    test3
    Write Excel    ${FilePath}/InputData.xls    6    1    2    test3
    Comment    ${address3}    FakerLibrary.Address
    Sleep    20s
    Wait And Click Element    id= jwl_cityid
    Wait And Click Element    id=jwl_cityid_i
    Run Keyword And Ignore Error    Wait And Click Element    //ul[@id='jwl_cityid_i_IMenu']//a[@title='Bethlehem']
    Run Keyword And Ignore Error    Wait And Click Element    //ul[@id='jwl_cityid_i_IMenu']//a[@title='Abu Dis']
=======
    Wait And Click Element    //ul[@id='jwl_cityid_i_IMenu']//a[@title='Bethlehem']
    click on element    id=savefooter_statuscontrol
    wait for element    id=savefooter_statuscontrol
>>>>>>> 8863de1075aafc4ca3faa00c8b79508e02b7ddd2
    wait and click element    id=stageAdvanceActionContainer
    sleep    10s
    Unselect Frame
    Screenshots    ${Screenshotpath}    New Customer Addr Info Page

NewCustomerBICorpCustomerInfoPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    click on element    id=btnCorporateCustomerCreation
    Wait and Click Element    id=jwl_lastnamecompanyname    timeout=40 sec
    ${LastName}    FakerLibrary.Last Name Male
    send text to element    id=jwl_lastnamecompanyname_i    ${LastName}
    Wait and Click Element    id=jwl_shortname
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=jwl_shortname_i    ${FirstName}
    Wait and Click Element    id=jwl_idtypeid
    #wait for element    id=Dialog_jwl_idtypeid_i_IMenu
    Sleep    5s
    Wait And Click Element    Xpath=//ul[@id='jwl_idtypeid_i_IMenu']/li[2]/a[2]/span/nobr/span
    #Press Key    id=Dialog_jwl_idtypeid_i_IMenu
    #Wait and Click Element    id=jwl_idnumber
    #{ID_Type_Value}    FakerLibrary.Random Int
    #send text to element    id=jwl_idnumber_i    ${ID_Type_Value}
    Wait and Click Element    id=jwl_taxoffice
    Input Text    id=jwl_taxoffice_i    Istanbul
    Wait and Click Element    id=jwl_idnumber
    ${ID_Type_Value}    FakerLibrary.Random Int
    send text to element    id=jwl_idnumber_i    ${ID_Type_Value}
    Wait and Click Element    id=jwl_mainphone
    ${a}    Generate Random String    8    [NUMBERS]
    log    ${a}
    ${MobileNumber} =    Catenate    SEPARATOR=    0    ${a}
    send text to element    id=jwl_mainphone_i    ${MobileNumber}
    Wait and Click Element    id=jwl_natureofbusiness
    click on element    Xpath=//select[@id='jwl_natureofbusiness_i']/option[@title='Other']
    Wait And Click Element    id=jwl_servicecenter
    Input Text    id=jwl_servicecenter_i    Istanbul
    Wait And Click Element    id=jwl_dateofcontract
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    id=DateInput    ${date1}
    #Wait And Click Element    id=jwl_profileclassificationid
    #click on element    Xpath=//div[@id='Dialog_jwl_profileclassificationid_i_IMenu']/div/u1/li/a[@title='CC']
    Wait And Click Element    id=jwl_profileclassificationid
    wait for element    id=Dialog_jwl_profileclassificationid_i_IMenu
    Press Key    id=Dialog_jwl_profileclassificationid_i_IMenu    \\13
    Wait And Click Element    id=jwl_languagecode
    click on element    Xpath=//select[@id='jwl_languagecode_i']/option[@title='English']
    Wait And Click Element    id=jwl_linepayment
    sleep    5s
    click on element    Xpath=//select[@id='jwl_linepayment_i']/option[@title='SUBSCRIBER HIM SELF']
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

NewCustomerBICorpAddressContactInfoPage
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    Wait And Click Element    id=jwl_address1
    ${address1}    FakerLibrary.Address
    send text to element    id=jwl_address1_i    ${address1}
    Wait And Click Element    xpath=//div[@id='jwl_address2']/div
    ${address2}    FakerLibrary.Address
    send text to element    id=jwl_address2_i    ${address2}
    Wait And Click Element    id=jwl_address3
    ${address3}    FakerLibrary.Address
    send text to element    id=jwl_address3_i    ${address3}
    Wait And Click Element    id=jwl_contactfirstname
    ${FirstName}    FakerLibrary.First Name Male
    send text to element    id=jwl_contactfirstname_i    ${FirstName}
    Wait And Click Element    id=jwl_contactlastname
    ${LastName}    FakerLibrary.First Name Male
    send text to element    id=jwl_contactlastname_i    ${LastName}
    Wait and Click Element    id=jwl_contactidtypenewid
    wait for element    id=Dialog_jwl_contactidtypenewid_i_IMenu
    Sleep    10s
    Wait And Click Element    Xpath=//ul[@id='jwl_contactidtypenewid_i_IMenu']/li[2]/a[2]/span/nobr/span
    Wait and Click Element    id=jwl_contactidnumber
    ${ID_Type_Value}    FakerLibrary.Random Int
    send text to element    id=jwl_contactidnumber_i    ${ID_Type_Value}
    Wait And Click Element    id=jwl_contactgender
    click on element    Xpath=//select[@id='jwl_contactgender_i']/option[@title='Male']
    Wait And Click Element    id=jwl_contactbirthdate
    #send text to element    id=jwl_contactbirthdate_i    09/01/1988
    ${date1}    DateTime.Get Current Date    time_zone=local    increment=0    result_format=%d-%m-%Y
    send text to element    Xpath=//table[@id='jwl_contactbirthdate_i']/tr/td/input    ${date1}
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

GetCustomerNumFromCustName
    [Arguments]    ${CustomerName}    ${Screenshotpath}
    Comment    GoToIndividualCustomersList    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    Wait And Click Element    //div[@id='header_jwl_individualcustomerid']/div/span
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Run Keyword And Ignore Error    Click Element    //a[@id='header_crmFormSelector']//img
    Run Keyword And Ignore Error    Click Element    //a[@title='Customer 360']
    sleep    10s
    Click Element    //h2[@id='tab_11_header_h2']
    sleep    5s
    Comment    wait for element    id=crmGrid_findCriteria
    Comment    click on element    id=crmGrid_findCriteria
    Comment    send text to element    id=crmGrid_findCriteria    ${CustomerName}
    Comment    Wait And Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    Comment    wait for element    //table[@id='gridBodyTable']
    ${CustomerNameFromWeb}    Get text    //div[@id='etel_externalid']
    log    ${CustomerNameFromWeb}
    Comment    ${CustomerNameFromWeb}    Get text    //a[starts-with(@id,'gridBodyTable_primaryField')]
    ${match1}    ${value1}    Run Keyword And Ignore Error    Should Contain    ${CustomerNameFromWeb}    CUST
    Comment    ${match1}    ${value1}    Run Keyword And Ignore Error    Should Contain    ${CustomerNameFromWeb}    ${CustomerName}
    ${ReturnResult}    Set Variable If    '${match1}'=='PASS'    ${True}    ${False}
    Comment    ${CUSTOMER_ID}    Run Keyword If    '${ReturnResult}'=='True'    Get Text    //table[@id='gridBodyTable']//tbody//tr//td[6]
    ${CUSTOMER_ID}    Run Keyword If    '${ReturnResult}'=='True'    Get Text    //div[@id='etel_externalid']
    Run Keyword If    '${ReturnResult}'=='True'    Add Result    PASS    UC1.2    Customer ID should be created    Customer ID created
    ...    ${CustomerNameFromWeb}
    log    ${CUSTOMER_ID}
    Screenshots    ${Screenshotpath}    Customer Number
    [Return]    ${CUSTOMER_ID}
