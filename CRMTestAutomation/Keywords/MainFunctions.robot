*** Settings ***
Library           Collections
Library           String
Library           ExtendedSelenium2Library
Library           ../CustomLibrary/Test.py
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Resource          ../ObjectRepository/OR.txt

*** Keywords ***
Launch Browser
    Open Browser    ${CRM_URL}    ${IE_BROWSER}
    maximize browser window

wait for home page
    [Arguments]    ${elementName}
    Wait Until Keyword Succeeds    30 sec    1 sec    Page Should Contain Element    ${elementName}
    Log    Homepage is viewed    level=WARN
    Capture Page Screenshot

wait for element
    [Arguments]    ${element}    ${timeout}=20 sec    ${interval}=1 sec
    Wait Until Keyword Succeeds    ${timeout}    ${interval}    Page Should Contain Element    ${element}

click on element
    [Arguments]    ${element}
    Click Element    ${element}

Wait And Click Element
    [Arguments]    ${element}    ${timeout}=20 sec    ${interval}=1 sec
    wait for element    ${element}    ${timeout}    ${interval}
    click on element    ${element}

click text if exists
    [Arguments]    ${text1}=    ${text2}=    ${text3}=    ${text4}=    ${text5}=    ${text6}=
    ...    ${text7}=    ${text8}=    ${text9}=    ${text10}=
    : FOR    ${i}    IN RANGE    1    10
    \    ${status}    ${value}=    Run Keyword And Ignore Error    Page Should Contain Element    name=${text${i}}
    \    Run keyword if    '${status}'=='PASS'    Click Element    name=${text${i}}
    \    Run keyword if    '${status}'=='PASS'    log    clicked on ${text${i}}    level=WARN
    \    Exit For Loop If    '${status}'=='PASS'

switch windows
    [Arguments]    ${windowNumber}    @{windowNames}
    Get Window Names    select window    name=@{windowNames}[${windowNumber}]

wait for element by xpath
    [Arguments]    ${element}    ${timeout}=20 sec    ${interval}=1 sec
    Wait Until Keyword Succeeds    ${timeout}    ${interval}    Page Should Contain Element    xpath=${element}

click on element by xpath
    [Arguments]    ${element}
    Click Element    xpath=${element}

Wait And Click Element By Xpath
    [Arguments]    ${element}    ${timeout}=20 sec    ${interval}=1 sec
    wait for element by xpath    xpath=${element}    ${timeout}    ${interval}
    click on element by xpath    xpath=${element}

Wait And Send Text
    [Arguments]    ${element}    ${text}    ${timeout}=20 sec    ${interval}=1 sec
    wait for element    ${element}    ${timeout}    ${interval}
    send text to element    ${element}    ${text}

send text to element
    [Arguments]    ${element}    ${text}
    Input Text    ${element}    ${text}

pendingemailwarning
    Select IFrame    id=InlineDialog1_Iframe
    wait for element    id=butBegin
    Focus    id=butBegin
    click on element    id=butBegin
    Unselect Frame

closeexploreCMR
    Select IFrame    name=InlineDialog_Iframe
    wait for element    //img[@alt='Close']
    Focus    //img[@alt='Close']
    click on element    //img[@alt='Close']
    Unselect Frame

close open browser
    Close Browser

Check the element
    [Arguments]    ${element}    ${successMessage}    ${failMessage}
    wait for element    ${element}
    ${status}    ${value}=    Run Keyword And Ignore Error    Page Should Contain Element    ${element}
    Run Keyword If    '${status}'=='PASS'    log    ${successMessage}    level=WARN
    ...    ELSE    Fail    log    ${failMessage}    level=WARN
    Run Keyword If    '${status}'=='PASS'    Capture Page Screenshot

When Test Failed Take Screenhot
    Run Keyword If Test Failed    Capture Page Screenshot
    Run Keyword If Test Failed    Close Browser
    Run Keyword If Test Passed    Close Browser

Wait for element until disappear
    [Arguments]    ${element}    ${timeout}=20 sec    ${interval}=7 sec
    Run Keyword And Ignore Error    Wait Until Keyword Succeeds    ${timeout}    ${interval}    Element Should Not Be Visible    ${element}

wait for element until vanish
    [Arguments]    ${element}    ${timeout}=20 sec    ${interval}=1 sec
    Wait Until Keyword Succeeds    ${timeout}    ${interval}    Page Should Not Contain Element    ${element}

UploadDocument
    wait for element    ${NotesLink}
    Click Element    ${NotesLink}
    Sleep    5s
    wait for element    ${NotesBox}
    Click Element    ${NotesBox}
    Sleep    5s
    wait for element    ${AttachBttn}
    Click Element    ${AttachBttn}
    Sleep    5s
    #wait for element    ${FileUploadPath}
    #Click Element    ${FileUploadPath}
    Choose File    ${FileUploadPath}    ${DocumentPath}
    Sleep    5s
    wait for element    ${DoneTab}
    Click Element    ${DoneTab}
    Sleep    5s

SuiteStartKeyword
    Create Excel    ${Resultpath}
    Log To Console    Loggin to TCRM Application
    Go to CRM
    ${pendingemailpresent}    Run Keyword And Return Status    Page Should Contain Element    id=InlineDialog1_Iframe
    Run Keyword If    ${pendingemailpresent}    pendingemailwarning
    Log To Console    Closing Explore CRM
    ${explorecrmpresent}    Run Keyword And Return Status    Page Should Contain Element    name=InlineDialog_Iframe
    Run Keyword If    ${explorecrmpresent}    closeexploreCMR
    Comment    Run Keyword And Continue On Failure    closeexploreCMR

FrameSelect
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1

Screenshots
    [Arguments]    ${filepath}    ${fileName}
    @{CurrentTime}    Get Time    month day hour min
    Comment    Log Many    @{CurrentTime}
    ${Screenshot_Path}    Set Variable    ${filepath}
    ${Time}    Set Variable    @{CurrentTime}[0]-@{CurrentTime}[1]_@{CurrentTime}[2]h-@{CurrentTime}[3]m
    Capture Page Screenshot    ${filepath}/${fileName}_${Time}.png

TestTeardown
    Run Keyword If Test Failed    TestFailKeyword
    Run Keyword If Test Passed    TestPassKeyword

TestFailKeyword
    Close Browser
    Log To Console    Loggin to TCRM Application
    Go to CRM
    ${pendingemailpresent}    Run Keyword And Return Status    Page Should Contain Element    id=InlineDialog1_Iframe
    Run Keyword If    ${pendingemailpresent}    pendingemailwarning
    ${explorecrmpresent}    Run Keyword And Return Status    Page Should Contain Element    name=InlineDialog_Iframe
    Run Keyword If    ${explorecrmpresent}    closeexploreCMR
    Comment    Run Keyword And Continue On Failure    pendingemailwarning
    Log To Console    Closing Explore CRM
    #Run Keyword And Continue On Failure    closeexploreCMR

TestPassKeyword
    Unselect Frame
    wait for element    //span[@id='navTabLogoTextId']
    Click Element    //span[@id='navTabLogoTextId']
    wait for element    ${MainLink}

TimeExecutionCalculation
    [Arguments]    ${UCNO}    ${A}
    ${B1}=    Get Time    epoch
    ${StartTimeInNum}    Convert To Number    ${A}
    ${EndTimeInNum}    Convert To Number    ${B1}
    ${DifferentTime}=    Evaluate    ${EndTimeInNum}-${StartTimeInNum}
    ${TimeInMin}=    Evaluate    ${DifferentTime}/60
    ${TimeInMinInNum}    Convert To Number    ${TimeInMin}
    Comment    ${elapsed}=    Get Time    min    ${a}
    Log    ${TimeInMinInNum}
    Add Result    PASS    ${UCNO}    Total test execution time should be calculated    Total execution time is :- ${TimeInMinInNum} Min    Execution time noted
