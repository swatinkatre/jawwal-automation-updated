*** Settings ***
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library

*** Keywords ***
ClickSectionNameDropdownlist
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=jwl_sectionname_i
    Unselect Frame

ClickAndSelectSectionNameDropdownlist
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=jwl_sectionname
    Wait And Click Element    xpath=//select[@id='jwl_sectionname_i']/option[3]
    Unselect Frame
