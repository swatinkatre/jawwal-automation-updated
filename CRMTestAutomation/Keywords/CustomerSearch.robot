*** Settings ***
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library

*** Keywords ***
ClickLookForSelectField
    [Arguments]    ${Screenshotpath}
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//div[@id='SimpleSearch']/table/tbody/tr/td[2]
    Screenshots    ${Screenshotpath}    Look for field
    Unselect Frame

CheckSelectFieldOptions
    Select IFrame    id=contentIFrame0
    Unselect Frame
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//div[@id='SimpleSearch']/table/tbody/tr/td[2]/select/option[4]
    Unselect Frame

SendSearchValueAndSearch
    [Arguments]    ${value}    ${Screenshotpath}    # searchName
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=SearchText
    send text to element    id=SearchText    ${value}
    Wait And Click Element    xpath=//a[@onclick=' doSimpleSearch() ']
    Screenshots    ${Screenshotpath}    Search Item
    Unselect Frame

VerifyCustomerSerachPage
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #Select IFrame    id=contentIFrame1
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    css=span.tCustomerSearch    #Run Keyword If    ${present}    Add Result
    ...    # PASS    UC01    Verify that customer search page is displayed    Customer Search page is visible    # Customer search page verificatiom completed    # ELSE
    ...    # Add Result    FAIL    UC01    Verify that customer search page is displayed    Customer Search page is not visible    # Customer search page verificatiom completed
    Unselect Frame
    Screenshots    ${Screenshotpath}    Customer Search Page

ClickOnLookForSelectFieldOptions
    [Arguments]    ${LookFor}    ${Screenshotpath}
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #Select IFrame    id=contentIFrame1
    wait for element    xpath=//select[@id='LookFor']//option[contains(text(),'${LookFor}')]
    Sleep    10s
    Click Element    //select[@id='LookFor']//option[contains(text(),'${LookFor}')]
    Screenshots    ${Screenshotpath}    Look for field
    Unselect Frame

VerifySearchedElementIfMultiplePresent
    [Arguments]    ${Name}    ${Screenshotpath}
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    Select IFrame    id=contentIFrame1
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[2]/table/tbody/tr/td[4]/div
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${NamefromWeb}=    Get Text    xpath=//div[2]/table/tbody/tr[${INDEX}]/td[4]/div
    \    log    ${NamefromWeb}
    \    ${a}    Split String    ${NamefromWeb}
    \    Run Keyword If    '${a[0]}'=='${Name}'    Double Click Element    xpath=//div[2]/table/tbody/tr[${INDEX}]/td[4]/div
    \    Exit for loop
    #Unselect Frame
    #Select IFrame    id=contentIFrame0
    #wait for element    xpath=//img[@alt='FormSections_NavigationFlyOut_Button']
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    xpath=//h1
    Run Keyword If    ${present}    log    Searched Element is visible in page
    Run Keyword If    ${present}    Add Result    PASS    UC1.3    Searched element should be visible    Searched element is visible
    ...    Search element verification done
    ...    ELSE    Add Result    PASS    UC1.3    Searched element should be visible    Searched element is not visible
    ...    Search element verification done
    Unselect Frame
    Screenshots    ${Screenshotpath}    Search by name element

VerifySearchElement
    [Arguments]    ${Screenshotpath}
    Select IFrame    id=contentIFrame0
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//img[@alt='FormSections_NavigationFlyOut_Button']    1m
    ${present}=    Run Keyword And Return Status    Element Should Be Visible    xpath=//h1
    Screenshots    ${Screenshotpath}    Search element
    Run Keyword If    ${present}    Log To Console    Searched Element is visible in page    #
    Run Keyword If    ${present}    Add Result    PASS    UC1.3    Searched element should be visible    Searched element is visible
    ...    Search element verification done
    ...    ELSE    Add Result    PASS    UC1.3    Searched element should be visible    Searched element is not visible
    ...    Search element verification done
    Unselect Frame
