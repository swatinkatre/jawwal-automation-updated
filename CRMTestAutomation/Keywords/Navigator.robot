*** Settings ***
Library           ExtendedSelenium2Library
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt

*** Keywords ***
Go to CRM
    #Create Webdriver    Ie
    #Go To    ${CRM_URL}
    ExtendedSelenium2Library.Open Browser    ${CRM_URL}    ${BROWSER}
    Log To Console    Browser opened
    #Maximize Browser Window
    Maximize Browser Window
    ${title}    Get Title
    Log To Console    ${title}
    #Select IFrame    id=navTabGroupDiv
    #Wait And Click Element    id=buttonClose
    Comment    Unselect Frame

GoToCustomerSearch
    [Arguments]    ${Screenshotpathuc01}
    Wait And Click Element    ${MainLink}
    Wait And Click Element    id=WS
    Comment    Wait And Click Element    ${ServiceMenuTabButton}
    sleep    5s
    Wait And Click Element    ${ServiceToCustomerSearch}
    Screenshots    ${Screenshotpathuc01}    CustomerPage

GoToCorporateCustomersList
    [Arguments]    ${Screenshotpath}
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${SalesMenuTabButton}
    Wait And Click Element    ${ServiceToCorporate}
    Screenshots    ${Screenshotpath}    Corporate customer list page

GoToIndividualCustomersList
    [Arguments]    ${Screenshotpath}
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${SalesMenuTabButton}
    Wait And Click Element    ${SalesToIndividual}
    Screenshots    ${Screenshotpath}    Ind Cust Page

GoToCases
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${ServiceMenuTabButton}
    Wait And Click Element    ${ServiceToCases}

GoToSubscriptions
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${SalesMenuTabButton}
    Wait And Click Element    ${SalesToSubscriptions}

OpenIndividualCustomerViewPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenIndividualCustomer}
    Unselect Frame

OpenCorporateCustomerViewPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenCorporateCustomerView}
    Unselect Frame

OpenCustomerDataPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenCustomerData}
    Unselect Frame

OpenAccountSummaryPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    Wait And Click Element    ${OpenAccountSummary}
    Unselect Frame

OpenMonetaryTransactionsPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    Wait And Click Element    ${OpenMonetaryTransactions}
    Unselect Frame

OpenBillingForCorporatePage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenBillingForCorporate}
    Unselect Frame

OpenCustomer360Page
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    #Wait And Click Element    ${OpenCustomer360}
    Page Should Contain    Automation
    Capture Page Screenshot
    Unselect Frame
    Close Browser

OpenBillingAndPaymentPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=formSelectorDropdown
    Wait And Click Element    ${OpenBillingAndPayment}
    Unselect Frame

OpenCustomer360BlacklistPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=header_crmFormSelector
    #Wait And Click Element    ${OpenCustomer360BlackList}
    Unselect Frame

OpenAccountHistory
    [Arguments]    ${type}
    Comment    Run keyword if    '${type}'=='corporate'    Wait And Click Element    ${OpenAccountHistoryButtonCorp}
    Comment    Run keyword if    '${type}'=='individual'    Wait And Click Element    ${OpenAccountHistoryButtonIndv}
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.AccountHistory.Button

OpenAccHistoryFromSubsPage
    ClickMoreCommand
    Wait And Click Element    ${OpenAccountHistoryButtonSubs}

ClickMoreCommand
    Wait And Click Element    id=moreCommands

OpenAccountHistoryCorp
    Wait And Click Element    id=account|NoRelationship|Form|jwl.account.Actions.Button
    Wait And Click Element    id=account|NoRelationship|Form|jwl.account.BI.Button
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=account|NoRelationship|Form|jwl.account.AccountHistory.Button

OpenAccountHistoryIndv
    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.BI.Button
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.AccountHistory.Button

mine_open360page
    Select IFrame    id=contentIFrame1
    Sleep    40s
    Wait And Click Element    Xpath=.//*[@id='header_crmFormSelector']/nobr/span
    Wait And Click Element    ${OpenCustomer360}
    Wait Until Angular Ready
    Page Should Contain Element    id=subscriptions_header_h2
    Capture Page Screenshot
    Unselect Frame

GoToReverseWriteOff
    #wait for element    id=contentIFrame0
    #Select IFrame    id=contentIFrame0
    #Sleep    20s
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']/span/a/span
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']/span/a/span
    Sleep    10s
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Button6.Button']/span/a
    Wait And Click Element    Xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Button6.Buttonx']/span/a
    Unselect Frame

OpenIndCustomerWithCustID
    [Arguments]    ${CUSTOMER_ID}    ${Screenshotpath}
    #wait for element    id=contact|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.contact.DeleteMenu    60    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    sleep    5s
    wait for element    id=crmGrid_findCriteria
    Sleep    20s
    Focus    id=crmGrid_findCriteria
    Sleep    5s
    Click Element    id=crmGrid_findCriteria
    Sleep    10s
    Input Text    id=crmGrid_findCriteria    ${CUSTOMER_ID}
    Run Keyword And Ignore Error    wait for element    //img[@id='crmGrid_findCriteriaImg']
    Run Keyword And Ignore Error    Click Element    //img[@id='crmGrid_findCriteriaImg']
    Screenshots    ${Screenshotpath}    Cust to be open
    wait for element    //a[starts-with(@id,'gridBodyTable_primaryField_')]
    Click Element    //a[starts-with(@id,'gridBodyTable_primaryField_')]
    Unselect Frame
    Sleep    10s
    Comment    Write Excel    ${FilePath}/InputData.xls    0    1    0    ${CUSTOMER_ID}
    Comment    Wait And Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    Comment    Screenshots    ${Screenshotpath}    Cust to be open
    Comment    Wait And Click Element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField_')]
    Comment    Unselect Frame

OpenSubWithContractID
    [Arguments]    ${Subscription_Id}    ${Screenshotpath}
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    sleep    20s
    Comment    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Comment    Run Keyword And Ignore Error    wait and click element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    Sleep    5s
    wait for element    //h2[@id='subscriptions_header_h2']
    Click Element    //h2[@id='subscriptions_header_h2']
    Comment    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct subscription name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Screenshots    ${Screenshotpath}    Subscription
    \    Run Keyword If    '${c}'=='${d}'    Double Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}'
    ##### For loop ends ####
    Unselect Frame

OpenTempDeactivationForSub
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select Frame    id=contentIFrame0
    Unselect Frame
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]    40    1
    Focus    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    click on element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Sleep    2s
    wait for element    //span[contains(text(),'Order Capture')]
    click on element    //span[contains(text(),'Order Capture')]
    Wait for element until disappear    id=loading    20s    2s
    wait for element    //span[contains(text(),'Subscription Status Change')]    40    1
    sleep    2s
    click on element    //span[contains(text(),'Subscription Status Change')]
    #wait for element    //span[contains(text(),'Deactivate Subscription')]    40    1
    wait for element    //span[contains(text(),'Temporary Deactivation')]    40    1
    sleep    2s
    #Click on element    //span[contains(text(),'Deactivate Subscription')]
    Click on element    //span[contains(text(),'Temporary Deactivation')]
    Unselect Frame

TempDeactivationPageForRequestByCustomer
    [Arguments]    ${Deactivation_Reason}    ${Alternate_phone}    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    Sleep    50s
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Sleep    20s
    wait for element    //div[@id="header_process_jwl_alternativephone"]
    Click Element    //div[@id="header_process_jwl_alternativephone"]
    Sleep    10s
    send text to element    //input[@id="header_process_jwl_alternativephone_i"]    ${Alternate_phone}
    Sleep    20s
    wait for element    //div[@id="header_process_jwl_deactivationreasonid"]
    Click Element    //div[@id="header_process_jwl_deactivationreasonid"]
    Sleep    10s
    Click Element    //img[@id="header_process_jwl_deactivationreasonid_i"]
    Sleep    20s
    Click Element    //span[contains(text(),'${Deactivation_Reason}')]
    Sleep    30s
    Screenshots    ${Screenshotpath}    Temporary deactivation page
    Unselect Frame

SelectSubWithContractID
    [Arguments]    ${Subscription_Id}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    sleep    20s
    Comment    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Comment    Run Keyword And Ignore Error    wait and click element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    Sleep    10s
    Click Element    //h2[@id='subscriptions_header_h2']
    Comment    wait and click element    //h2[@id='subscriptions_header_h2']
    #Unselect Frame
    #wait for element    id=contentIFrame0
    #Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    Sleep    10s
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct subscription name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Screenshots    ${Screenshotpath}    Subscription
    \    Run Keyword If    '${c}'=='${d}'    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}'
    ##### For loop ends ####
    Unselect Frame

OpenUpdatedAccountForSub
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=crmRibbonManager
    wait for element    id=commandContainer2
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    10s
    Comment    Wait for element until disappear    //img[@id='loading']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    BI Selected
    wait for element    id=contact|NoRelationship|Form|jwl.contact.UpdateAccount.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.UpdateAccount.Button

UpdateAccForSubPage
    [Arguments]    ${BillingClass}    ${UPFirst_Name}    ${UPSec_Name}    ${UPThr_Name}    ${UPLast_Name}    ${UPFull_Name}
    ...    ${UPAuthSig_Name}    ${UP_Gender}    ${UP_Email}    ${UP_PhoneNum}    ${UP_FaxNum}    ${Screenshotpath}
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    ####Subscription profileData ####
    wait and click element    id=jwl_billingclassification
    Wait And Click Element    xpath=//select[@id='jwl_billingclassification_i']/option[contains(text(),'${BillingClass}')]
    Wait And Click Element    id=jwl_billingprofileaflg
    Wait And Click Element    id=jwl_billingprofilecflg
    Wait And Click Element    id=jwl_billingprofilepflg
    Wait And Click Element    id=jwl_billingprofilebflg
    Wait And Click Element    id=jwl_billingprofilejflg
    #### User profile data ###
    Wait And Click Element    id=jwl_usr_pro_firstname
    send text to element    id=jwl_usr_pro_firstname_i    ${UPFirst_Name}
    Write Excel    ${FilePath}/InputData.xls    3    1    1    ${UPFirst_Name}
    Wait And Click Element    id=jwl_usr_pro_secondname
    send text to element    id=jwl_usr_pro_secondname_i    ${UPSec_Name}
    Write Excel    ${FilePath}/InputData.xls    3    1    2    ${UPSec_Name}
    Wait And Click Element    id=jwl_usr_pro_thirdname
    send text to element    id=jwl_usr_pro_thirdname_i    ${UPThr_Name}
    Write Excel    ${FilePath}/InputData.xls    3    1    3    ${UPThr_Name}
    Wait And Click Element    id=jwl_usr_pro_lastname
    send text to element    id=jwl_usr_pro_lastname_i    ${UPLast_Name}
    Write Excel    ${FilePath}/InputData.xls    3    1    4    ${UPLast_Name}
    wait and click element    id=jwl_usr_pro_fullname
    send text to element    id=jwl_usr_pro_fullname_i    ${UPFull_Name}
    Write Excel    ${FilePath}/InputData.xls    3    1    5    ${UPFull_Name}
    Wait And Click Element    id=jwl_auth_per_signaturename
    send text to element    id=jwl_auth_per_signaturename_i    ${UPAuthSig_Name}
    Write Excel    ${FilePath}/InputData.xls    3    1    6    ${UPAuthSig_Name}
    #Wait And Click Element    id=jwl_usr_pro_birthdate
    #send text to element    id=jwl_usr_pro_birthdate_i    31/01/2018
    Wait And Click Element    id=jwl_usr_pro_gendercode
    Wait And Click Element    xpath=//select[@id='jwl_usr_pro_gendercode_i']//option[contains(text(),'${UP_Gender}')]
    Wait And Click Element    id=jwl_usr_pro_email
    send text to element    id=jwl_usr_pro_email_i    ${UP_Email}
    Write Excel    ${FilePath}/InputData.xls    3    1    8    ${UP_Email}
    Wait And Click Element    id=jwl_usr_pro_mainphonenumber
    send text to element    id=jwl_usr_pro_mainphonenumber_i    ${UP_PhoneNum}
    Write Excel    ${FilePath}/InputData.xls    3    1    9    ${UP_PhoneNum}
    Wait And Click Element    id=jwl_usr_pro_faxnum
    send text to element    id=jwl_usr_pro_faxnum_i    ${UP_FaxNum}
    Write Excel    ${FilePath}/InputData.xls    3    1    10    ${UP_FaxNum}
    Screenshots    ${Screenshotpath}    Details updated
    Wait And Click Element    id=stageAdvanceActionContainer
    Comment    wait for element    id=stageAdvanceActionContainer
    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Comment    SaveAndClickOnNextStage
    Unselect Frame

VerifyUpdateAccountStatus
    sleep    20s
    ${message}    Confirm Action
    log    ${message}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    wait for element    xpath=//div[@id='header_statuscode']/div/span    40s
    ${StatusReason}    Get Text    xpath=//div[@id='header_statuscode']/div/span
    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    Log    Customer status is ${StatusReason}
    ...    ELSE    Log    Customer status is ${StatusReason}
    Run Keyword If    '${message}'=='BI New Customer Submitted Successfully.'    Log    Customer created succesfully and has name =${CustomerName}
    ...    ELSE    Log    We got error while creating customer. Error detail is =${message}
    close browser

UpdateAccDownloadPage
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Screenshots    ${Screenshotpath}    DownloadPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

UpdateAccUploadPage
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    UploadPage
    Wait And Click Element    id=stageAdvanceActionContainer
    Unselect Frame

UpdateAccSummaryPage
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Screenshots    ${Screenshotpath}    SummaryPage
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame

UpdateAccSubmit
    [Arguments]    ${Screenshotpath}
    Wait Until Element Is Visible    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|jwl.jwl_bi_updateaccount.Submit.Button']    20s
    Focus    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|jwl.jwl_bi_updateaccount.Submit.Button']
    click on element    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|jwl.jwl_bi_updateaccount.Submit.Button']
    sleep    20s
    Screenshots    ${Screenshotpath}    BI Submit Msg
    ${Updatemessage}    Confirm Action
    sleep    4s
    Comment    wait for element    //li[@id='jwl_bi_updateaccount|NoRelationship|Form|Mscrm.Form.jwl_bi_updateaccount.NewRecord']
    log    ${Updatemessage}
    Run Keyword If    '${Updatemessage}'=='BI Update Account Submitted Successfully.'    Log    Update Account BI sumbitted succesfully
    ...    ELSE    Log    Update Account BI is not sumbitted succesfully Error message =${Updatemessage}
    Run Keyword If    '${Updatemessage}'=='BI Update Account Submitted Successfully.'    Add Result    PASS    1.3    BI Update Account Submitted Successfully    Popup message is as expected.
    ...    Verification Complete
    ...    ELSE    FAILED    1.3    Update Account BI is not sumbitted succesfully Error message =${Updatemessage}    Popup message is not as expected.    Verification Failed

OpenAddressInformation
    Comment    Wait for element    id=crmRibbonManager    40    5
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    sleep    20s
    Unselect Frame
    Select IFrame    id=contentIFrame1
    sleep    20s
    wait and click element    //h2[@id='tab_16_header_h2']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    Sleep    10s
    wait for element    Xpath=.//table[@id='gridBodyTable']/tbody/tr/td[2]/div    40    5
    Double Click Element    Xpath=.//table[@id='gridBodyTable']/tbody/tr/td[2]/div
    Wait And Click Element    Xpath=.//*[@id='etel_customeraddress|NoRelationship|Form|jwl.etel_customeraddress.UpdateAddress.Button']/span    60    5
    sleep    30s
    Comment    wait for element    xpath=.//*[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']/span/    40    2
    Wait And Click Element    //h2[@id='tab_16_header_h2']
    Wait And Click Element    Xpath=.//table[@id='gridBodyTable']/tbody/tr
    Double Click Element    Xpath=.//table[@id='gridBodyTable']/tbody/tr

OpenSubscription
    [Arguments]    ${Subscription_Id}    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Sleep    50s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Run Keyword And Ignore Error    Wait And Click Element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Run Keyword And Ignore Error    wait for element    id=Subscription_openAssociatedGridViewImageButtonImage
    Comment    Run Keyword And Ignore Error    click on element    id=Subscription_openAssociatedGridViewImageButtonImage
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[5]
    \    ${b}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    Screenshots    ${Screenshotpath}    Subscription
    Double Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div

OpenModifySubscription
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Comment    wait for element    id=contentIFrame1    60    1
    Comment    Select Frame    id=contentIFrame1
    Comment    Unselect Frame
    Comment    Sleep    25s
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]    60    1
    sleep    3s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    40    1
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    sleep    3s
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    Comment    Wait for element until disappear    id=loading    20s    2s
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.Form.etel_subscription.ModifySubscription']
    Comment    sleep    2s
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.Form.etel_subscription.ModifySubscription']
    Unselect Frame

Open Change Ownership
    sleep    10s
    Comment    wait for element    id=contentIFrame1    60    2
    Comment    Select Frame    id=contentIFrame1
    Comment    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Cos.Button']    40    1
    Sleep    10s
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Cos.Button']
    Unselect Frame

Open Reactivate Subscription
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select Frame    id=contentIFrame0
    Unselect Frame
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]    40    1
    Focus    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Click Element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Sleep    2s
    wait for element    //span[contains(text(),'Order Capture')]
    Click Element    //span[contains(text(),'Order Capture')]
    Comment    Wait for element until disappear    id=loading    20s    2s
    wait for element    //span[contains(text(),'Subscription Status Change')]    40    1
    sleep    2s
    Click Element    //span[contains(text(),'Subscription Status Change')]
    wait for element    //span[contains(text(),'Reactivate Subscription')]    40    1
    sleep    2s
    Click Element    //span[contains(text(),'Reactivate Subscription')]
    Unselect Frame

OpenCorrectionOCC
    [Arguments]    ${screenshot}
    wait for element    id=contentIFrame0
    Select Frame    id=contentIFrame0
    Unselect Frame
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    20s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Run Keyword And Ignore Error    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Screenshots    ${screenshot}    Subscription page
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Comment    Wait for element until disappear    id=loading    20s    2s
    Comment    sleep    10s
    Comment    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    7s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button3.Button']    30    1
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Button3.Button']
    Unselect Frame

SelectSubWithConID
    [Arguments]    ${Subscription_Id}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    sleep    20s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    Run Keyword And Ignore Error    wait and click element    id=tab_13_header_h2
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct subscription name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${Status}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    ${T1}    Convert To String    ${External_Id}
    \    ${T2}    Convert To String    ${Subscription_Id}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Run Keyword If    '${c}'=='${d}' AND '${Status}'=='Active'    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}' AND '${Status}'=='Active'
    ##### For loop ends ####
    Unselect Frame

OpenInvoiceAdjustmentBI-Wrong
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' Order Capture ')]
    sleep    10s
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' New Subscription')]
    wait for element    id=commandContainer3
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    select Iframe    id=WebResource_processapp
    wait for element    id=processStepsContainer
    wait for element    id=processStep_corporateCustomer
    Double Click Element    //div[@id='processStep_corporateCustomer']//div[@class='processStepValue']

OpenInvoiceAdjustmentBI
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoiceAdjustment.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoiceAdjustment.Button']
    sleep    30s

OpenFreezeSubscription
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Select Frame    id=contentIFrame1
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    20s
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']//img[@class='flyoutAnchorArrow']
    Sleep    20s
    wait for element    //span[contains(text(),'Subscription Status Change')]
    Sleep    10s
    click on element    //span[contains(text(),'Subscription Status Change')]//img[@class='flyoutAnchorArrow']
    Sleep    20s
    wait for element    xpath=//span[contains(text(),'Freeze Subscription')]
    Sleep    10s
    Screenshots    ${Screenshotpath}    FreezeSubOrder
    click on element    xpath=//span[contains(text(),'Freeze Subscription')]//img[@class='flyoutAnchorArrow']
    Sleep    20s

OpenInvoicePaymentBI
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Cust 360 page
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    3s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    Comment    Run Keyword And Ignore Error    Confirm Action
    sleep    10s
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoicePayment.Button']
    Comment    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoicePayment.Button']/span/a/span    20s
    Screenshots    ${Screenshotpath}    BI
    Comment    Focus    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoicePayment.Button']/span/a/span
    Comment    Click Link    link=Invoice Payment
    Comment    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.InvoicePayment.Button']/span/a/span

OpenNonInvoicePaymentBI
    [Arguments]    ${Screenshotpath}
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Comment    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    Comment    Run Keyword And Ignore Error    confirm action
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    Screenshots    ${Screenshotpath}    BI page
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.NonInvoicePayment.Button']
    Comment    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.NonInvoicePayment.Button']
    Comment    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.NonInvoicePayment.Button']
    Comment    sleep    20s

OpenBalanceRefillBI
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Screenshots    ${Screenshotpath}    Subscription Page
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    #Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button
    sleep    2s
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BalanceRefill.Button']

OpenVoucherRefillBI
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    2s
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    #Wait And Click Element    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button
    Comment    sleep    2s
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    sleep    7s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    BI
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.VoucherRefill.Button_1']
    Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.VoucherRefill.Button_1']
    Comment    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.VoucherRefill.Button_1']

OpenSKFPOrder
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.OrderCapture.Button']
    Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.OrderCapture.Button']
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.SalesSKFP.Button']
    Screenshots    ${Screenshotpath}    SKFPOrder
    Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.SalesSKFP.Button']
    sleep    10s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']

OpenNewSubscription
    [Arguments]    ${Screenshotpath}
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${Screenshotpath}    Customer 360 page
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' Order Capture ')]
    sleep    5s
    Wait And Click Element    //a[@class='ms-crm-Menu-Label']//span[contains(text(),' New Subscription')]

SelectSubWithAnyStatus
    [Arguments]    ${Contract_ID}    ${Screenshotpath}
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    sleep    10s
    Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_24_header_h2']
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait and click element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #### Get count of the webtable ###
    ${functionBlockCount}=    Get Matching Xpath Count    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    log    ${functionBlockCount}
    ${count}=    Evaluate    ${functionBlockCount}+1
    ##### For loop to evaluate the correct movie name ###
    : FOR    ${INDEX}    IN RANGE    1    ${count}
    \    ${External_id}=    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    log    ${External_id}
    \    ${T1}    Convert To String    ${External_id}
    \    ${T2}    Convert To String    ${Contract_ID}
    \    ${a}    Split String    ${T1}    CONTR
    \    ${b}    Split String    ${T2}    CONTR
    \    ${c}    Convert To String    ${b[1]}
    \    log    ${c}
    \    ${d}    Convert To String    ${a[1]}
    \    log    ${d}
    \    Run Keyword If    '${c}'=='${d}'    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]
    \    Exit For Loop If    '${c}'=='${d}'
    Wait for element until disappear    //img[@id='loading']    10s    2s
    Screenshots    ${Screenshotpath}    Sub with any status
    ##### For loop ends ####
    Unselect Frame

OpenAccHistory
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.AccHistory.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.AccHistory.Button']
    sleep    2s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']

OpenOfferChange
    [Arguments]    ${Screenshotpath}
    sleep    10s
    Screenshots    ${Screenshotpath}    Subscription page
    Comment    wait for element    id=contentIFrame0    60    2
    Comment    Select Frame    id=contentIFrame0
    Comment    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    2
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    Screenshots    ${Screenshotpath}    Subscription Page
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    sleep    10s
    Run Keyword And Ignore Error    Click Element    //span[contains(text(),'OFFER CHANGE')]
    Run Keyword And Ignore Error    Focus    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Form.OfferChange.Button']
    Run Keyword And Ignore Error    Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Form.OfferChange.Button']

Open FamilyPackage
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    60    1
    wait for element    id=contentIFrame1    40    1
    Select Frame    id=contentIFrame1
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']    40    1
    Run Keyword And Ignore Error    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Sleep    10s
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait for element until disappear    id=loading    20s    2s
    sleep    5s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.FamilyPack.Button']
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.FamilyPack.Button']
    Unselect Frame

Navigate to BulkOrder
    Wait And Click Element    //img[@id='homeButtonImage']
    Wait And Click Element    //a[@id='Settings']
    Wait And Click Element    //a[@id='rightNavLink']
    Wait And Click Element    //a[@id='jwl_bulkoperation']
    wait for element    //li[@id='jwl_bulkoperation|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.jwl_bulkoperation.NewRecord']
    Click Element    //li[@id='jwl_bulkoperation|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.jwl_bulkoperation.NewRecord']

OpenCorpCustomerWithCustID
    [Arguments]    ${CUSTOMER_ID}
    wait for element    id=account|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.account.DeleteMenu    70    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    id=crmGrid_findCriteria
    click on element    id=crmGrid_findCriteria
    send text to element    id=crmGrid_findCriteria    ${CUSTOMER_ID}
    Wait And Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    Wait And Click Element    //div[@id='crmGrid_divDataArea']//table[@id='gridBodyTable']//tr[@class='ms-crm-List-Row']
    wait for element    //img[starts-with(@id,'gridBodyTable_checkBox_Image')]
    Double Click Element    //img[starts-with(@id,'gridBodyTable_checkBox_Image')]

OpenCorpSubscription
    [Arguments]    ${Subscription_Id}
    wait for element    id=account|NoRelationship|Form|jwl.account.Actions.Button    60    2
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Comment    sleep    20s
    wait and click element    //h2[@id='subscriptions_header_h2']    40    2
    Unselect Frame
    Select IFrame    id=contentIFrame1
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']    40    2
    Comment    Wait And Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001496')]
    Comment    Double Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001496')]
    Comment    wait for element    //div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']/tbody/tr[2]/td[5]/div
    Comment    Double Click Element    //div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']/tbody/tr[2]/td[5]/div
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div
    \    ${b}    Get Text    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[3]/nobr/span
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    Double Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div

SearchSubscription
    [Arguments]    ${Call_detail_sub}    ${Screenshotpath}
    Input Text    //input[@id='search']    ${Call_detail_sub}
    Wait And Click Element    //img[@id='findCriteriaImg']
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Unselect Frame
    wait for element    contentIFrame1
    Select IFrame    contentIFrame1
    Wait And Click Element    //span[@id='attribone']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    Screenshots    ${Screenshotpath}    Subscription Page

OpenDirectDebitBI
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Focus    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.DirectDebitOrdernew.Button']
    Screenshots    ${Screenshotpath}    DDOption
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.DirectDebitOrdernew.Button']

Open360page_Corp
    [Arguments]    ${Screenshotpath}
    wait for element    id=crmRibbonManager    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //a[@id='header_crmFormSelector']    40    1
    wait for element    //a[@id='header_crmFormSelector']//span[@class='ms-crm-FormSelector']
    ${cust360}    Get Text    //a[@id='header_crmFormSelector']//span[@class='ms-crm-FormSelector']
    Run Keyword If    '${cust360}' == 'CORPORATE : CUSTOMER 360'    Log    Customer 360 form is selected
    ...    ELSE    OpenAndVerify360Page
    Screenshots    ${Screenshotpath}    Corporate customer 360 page
    Run Keyword If    '${cust360}' == 'CORPORATE : CUSTOMER 360'    Add Result    PASS    UC1.1    Corporate customerts 360 page should be visible    Corporate customerts 360 page is visible
    ...    Corporate customerts 360 page verification done
    ...    ELSE    Add Result    PASS    UC1.1    Corporate customerts 360 page should be visible    Corporate customerts 360 page is not visible
    ...    Corporate customerts 360 page verification not done

Open360page_Ind
    [Arguments]    ${Screenshotpath}
    wait for element    id=crmRibbonManager    60    2
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //a[@id='header_crmFormSelector']    40    1
    wait for element    //a[@id='header_crmFormSelector']//span[@class='ms-crm-FormSelector']
    ${cust360}    Get Text    //a[@id='header_crmFormSelector']//span[@class='ms-crm-FormSelector']
    Run Keyword If    '${cust360}' == 'INDIVIDUAL : CUSTOMER 360'    Log    Customer 360 form is selected
    ...    ELSE    OpenAndVerify360Page
    Screenshots    ${Screenshotpath}    Corporate customer 360 page
    Run Keyword If    '${cust360}' == 'INDIVIDUAL : CUSTOMER 360'    Add Result    PASS    UC1.1    Individual customerts 360 page should be visible    Individual customerts 360 page is visible
    ...    Individual customerts 360 page verification done
    ...    ELSE    Add Result    PASS    UC1.1    Individual customerts 360 page should be visible    Individual customerts 360 page is not visible
    ...    Individual customerts 360 page verification not done

OpenAndVerify360Page_If Found
    Wait And Click Element    //span[contains(text(),'Customer 360')]
    Unselect Frame
    wait for element    id=crmRibbonManager    60    2
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame1    60    2
    Select IFrame    id=contentIFrame1
    sleep    10s
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //a[@id='header_crmFormSelector']    40    1
    wait for element    //a[@id='header_crmFormSelector']//span[@class='ms-crm-FormSelector']
    ${cust360}    Get Text    //a[@id='header_crmFormSelector']//span[@class='ms-crm-FormSelector']
    Run Keyword If    '${cust360}' == 'CORPORATE : CUSTOMER 360'    Log    Customer 360 form is selected
    ...    ELSE    Log    Customer360 form is not available
    Run Keyword If    '${cust360}' == 'INDIVIDUAL : CUSTOMER 360'    Log    Customer 360 form is selected
    ...    ELSE    Log    Customer360 form is not available

Search a subscription
    [Arguments]    ${Call_detail_sub}
    Input Text    //input[@id='search']    ${Call_detail_sub}
    Wait And Click Element    //img[@id='findCriteriaImg']
    wait for element    contentIFrame0
    Select IFrame    contentIFrame0
    Wait And Click Element    //span[@id='attribtwo']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame

GoToCustomerPageAgain
    Wait And Click Element    ${MainLink}
    Wait And Click Element    ${ServiceMenuTabButton}
    Wait And Click Element    ${ServiceToIndividual}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    sleep    5s
    wait for element    id=crmGrid_findCriteria
    click on element    id=crmGrid_findCriteria
    Wait And Click Element    xpath=//a[starts-with(@id,'gridBodyTable_primaryField')]
    Unselect Frame

OpenModifySubscriptionTwinSim
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.OrderCapture.Button']
    Comment    Wait for element until disappear    id=loading    20s    2s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.Form.etel_subscription.ModifySubscription']
    Click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.Form.etel_subscription.ModifySubscription']
    Unselect Frame

NavigateToReviewandPrint
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.ReviewAndPrint.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.ReviewAndPrint.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']

NavigateToMSISDNChange
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|etel.etel_subscription.Form.MSISDNChange.Button']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']

SelectAndNavigatetoSwapSim
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Unselect Frame
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    click on element    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Focus    //li[@id='etel_subscription|NoRelationship|Form|etel.etel_subscription.Form.ChangeSIMCommand.Button']
    Click Element     //li[@id='etel_subscription|NoRelationship|Form|etel.etel_subscription.Form.ChangeSIMCommand.Button']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']

SelectMSISDN
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait And Click Element    //h2[@id='tab_5_header_h2']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //table[@id='gridBodyTable']/tbody/tr/td[2]
    Screenshots    ${Screenshotpath}    Select a MSISDN
    Unselect Frame

Select InvoiceWriteOff
    Run Keyword And Ignore Error    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    #Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Run Keyword And Ignore Error    click on element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Run Keyword And Ignore Error    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.BI.Button
    sleep    5s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Run Keyword And Ignore Error    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.InvoiceWriteoff.Button
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']

Select NonInvoiceWriteOff
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    #Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait And Click Element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Wait And Click Element    xpath=//li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']/span/a/span
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    xpath=//li[@id='contact|NoRelationship|Form|jwl.contact.Button6.Button']/span/a/span
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    xpath=//li[@id='contact|NoRelationship|Form|jwl.contact.Button6.Buttonx']/span/a/span
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']

OpenCMBI
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Focus    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    Comment    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    Run Keyword And Ignore Error    Wait And Click Element    //ul[@id='contact_NoRelationship_Form_jwl_contact_BI_ButtonMenu']//span[contains(text(),'Finacial Transactions')]
    Comment    Run Keyword And Ignore Error    wait for element    //ul[@id='contact_NoRelationship_Form_jwl_contact_BI_ButtonMenu']//span[contains(text(),'Finacial Transactions')]
    Comment    Run Keyword And Ignore Error    click on element    //ul[@id='contact_NoRelationship_Form_jwl_contact_BI_ButtonMenu']//span[contains(text(),'Finacial Transactions')]
    Comment    sleep    3s
    Screenshots    ${Screenshotpath}    BI
    Run Keyword And Ignore Error    Wait And Click Element    link= CM/CO Refund
    Run Keyword And Ignore Error    Wait And Click Element    id=contact|NoRelationship|Form|jwl.contact.Button7.Buttony
    sleep    7s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']

SelectFCPayment
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    #Focus    xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']//img[@class='flyoutAnchorArrow']
    Wait And Click Element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Wait And Click Element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.Button6.Button']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.Button666.Button']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']

SelectFCWriteoff
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Screenshots    ${Screenshotpath}    customer 360 view page
    Comment    Focus    xpath=//li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']//img[@class='flyoutAnchorArrow']
    sleep    5s
    Wait And Click Element    xpath=//span[@class='ms-crm-CommandBar-Menu'][contains(text(),'Actions')]
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    id=loading
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.Button6.Button']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait And Click Element    //li[@id='contact|NoRelationship|Form|jwl.contact.Button6.Buttony']
    Wait Until Keyword Succeeds    60s    1s    Element Should Not Be Visible    //img[@id='loading']

SelectSubscription
    [Arguments]    ${Subscription_Id}    ${screenshot}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']    60    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Wait Until Keyword Succeeds    50s    1s    Element Should Not Be Visible    //img[@id='loading']
    Screenshots    ${screenshot}    Customer 360 view
    Unselect Frame
    Select IFrame    id=contentIFrame1
    Sleep    80s
    wait for element    //h2[@id='subscriptions_header_h2']
    Sleep    5s
    Focus    //h2[@id='subscriptions_header_h2']
    Sleep    5s
    Click Element    //h2[@id='subscriptions_header_h2']
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Unselect Frame
    Sleep    10s
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Comment    sleep    20s
    #Run Keyword And Ignore Error    wait and click element    //h2[@id='tab_16_header_h2']
    #Run Keyword And Ignore Error    wait and click element    id=tab_13_header_h2
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #Unselect Frame
    #Select IFrame    id=contentIFrame1
    #wait and click element    //h2[@id='subscriptions_header_h2']
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    Sleep    20s
    wait for element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr/td[5]
    #For Loop will only open provided subscription if it is active##
    : FOR    ${INDEX}    IN RANGE    1    10
    \    Log    ${INDEX}
    \    ${a}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[5]
    \    ${b}    Get Text    xpath=//table[@id='gridBodyTable']//tbody//tr[${INDEX}]//td[3]
    \    Run Keyword If    '${a}' \ \ == \ \ '${Subscription_Id}' \ and \ \ '${b}' \ \ == \ \ 'Active'    Exit For Loop
    Click Element    xpath=//div[@id='Subscription_divDataArea']//table[@id='gridBodyTable']//tbody/tr[${INDEX}]/td[5]/div
    Unselect Frame

Open Cash on Account BI
    [Arguments]    ${Screenshotpath}
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    sleep    5s
    Screenshots    ${ScreenshotpathW2UAT_UC10}    Customer 360 page
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Focus    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    sleep    5s
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.BI.Button']
    sleep    7s
    Run Keyword And Ignore Error    Wait And Click Element    //ul[@id='contact_NoRelationship_Form_jwl_contact_BI_ButtonMenu']//span[contains(text(),'Finacial Transactions')]
    sleep    3s
    Wait And Click Element    //span[contains(text(),' CM/CO Refund')]

NavigateToUsageHistory
    #Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    #wait for element    id=crmRibbonManager
    #wait for element    id=commandContainer4
    Sleep    120s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']
    Sleep    100s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']
    Sleep    80s
    wait for element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.UsageHistory.Button']
    click on element    //li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.UsageHistory.Button']

NavigateToDeactivateContract
    wait for element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    click on element    //li[@id='contact|NoRelationship|Form|jwl.contact.Actions.Button']
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.OrderCapture.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button62.Button
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=contact|NoRelationship|Form|jwl.contact.Button67.Button
    click on element    id=contact|NoRelationship|Form|jwl.contact.Button67.Button
