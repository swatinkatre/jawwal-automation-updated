*** Settings ***
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library

*** Keywords ***
Select IFrame
    [Arguments]    ${element}
    wait for element    ${element}
    Select Frame    ${element}

Go To First Corporate Customer
    GoToCorporateCustomersList
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[3]/nobr/a

Go To First Individual Customer
    GoToIndividualCustomersList
    Select IFrame    id=contentIFrame0
    Wait And Click Element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[2]/nobr/a

Go To Spesific Individual Customer
    [Arguments]    ${Customer}    ${Screenshotpath}    # Customer name
    GoToIndividualCustomersList    ${Screenshotpath}
    wait for element    id=contact|NoRelationship|HomePageGrid|Mscrm.HomepageGrid.contact.Send    40    2
    wait for element    id=contentIFrame0    40    2
    Select IFrame    id=contentIFrame0
    Sleep    5s
    Wait And Click Element    id=crmGrid_findCriteria    60    2
    send text to element    id=crmGrid_findCriteria    ${Customer}
    Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    wait for element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[4]/nobr/span
    Double Click Element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[4]/nobr/span    skip_ready=${true}s
    Unselect Frame

Go to Spesific Corporate Customer
    [Arguments]    ${Customer}
    GoToCorporateCustomersList
    Sleep    10s
    Select IFrame    id=contentIFrame0
    Sleep    10s
    Wait And Click Element    id=crmGrid_findCriteria
    send text to element    id=crmGrid_findCriteria    ${Customer}
    Click Element    Xpath=.//*[@id='crmGrid_findCriteriaImg']
    Comment    Wait And Click Element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[3]/nobr/a
    wait for element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[3]/nobr/a
    Click Element    xpath=//table[@id='gridBodyTable']/tbody/tr/td[3]/nobr/a    skip_ready=${true}
    Unselect Frame

Go To Subscription With Status
    [Arguments]    ${customerType}    ${status}
    GoToSubscriptions
    Sleep    10s
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//div[@id='filterButton']/a/img
    Run Keyword If    "${customerType}" == "individual"    Wait And Click Element    id=lookupFilterPopupcrmGridetel_subscriptionetel_individualcustomerid
    Wait And Click Element    xpath=//span[@title='Contains Data']
