*** Settings ***
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library

*** Keywords ***
ClickResetCallBarringPassword
    Select IFrame    id=contentIFrame0
    Sleep    20s
    #Wait And Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']/span/a/span
    #Wait And Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']/span/a/span
    Comment    Select IFrame    id=contentIFrame1
    Wait And Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.ResetCallBarring.Button']/span/a/span
    Double Click Element    Xpath=.//*[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.ResetCallBarring.Button']/span/a/span
    Sleep    10s
    Confirm Action
    Sleep    10s
    Confirm Action
    Unselect Frame
