*** Settings ***
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt
Library           ExtendedSelenium2Library
Library           ../CustomLibrary/UpdateExcelSheet.py

*** Keywords ***
StartBISafely
    Select IFrame    id=contentIFrame1
    Select IFrame    id=WebResource_biheader
    Wait And Click Element    id=stop
    Sleep    5s
    Wait And Click Element    id=start
    Unselect Frame
    Unselect Frame

SelectSubsInIndvCustPage
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr/td[4]
    Unselect Frame

SelectSubsInCorpCustPage
    Select IFrame    id=contentIFrame1
    Sleep    15s
    Scroll Element Into View    id=subscriptions_header_h2
    Click Element    id=subscriptions_header_h2
    Wait And Click Element    xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr/td[2]
    Unselect Frame

GoToSubsDetailForSelectedIndv
    Select IFrame    id=contentIFrame1
    Sleep    15s
    Scroll Element Into View    id=subscriptions_header_h2
    Click Element    id=subscriptions_header_h2
    Wait Until Angular Ready
    Double Click Element    Xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr/td[2]
    Sleep    10s
    Wait And Click Element    Xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.Actions.Button']/span/a/span
    Wait And Click Element    Xpath=//li[@id='etel_subscription|NoRelationship|Form|jwl.etel_subscription.BI.Button']/span/a/span
    Unselect Frame

GoToSubsDetailForSelectedCorp
    Select IFrame    id=contentIFrame1
    Scroll Element Into View    id=Subscription_divDataArea
    Wait And Click Element    xpath=//div[@id='Subscription_divDataArea']/div/table/tbody/tr[4]/td[2]/nobr/a
    Unselect Frame

ChangeLanguage
    Sleep    10s
    Select IFrame    id=contentIFrame1
    Wait And Click Element    id=etel_languagecode
    Wait And Click Element    xpath=//select[@id='etel_languagecode_i']/option[3]
    Wait And Click Element    id=savefooter_statuscontrol
    Unselect Frame

ClickStartBI
    Select IFrame    id=contentIFrame1
    Select IFrame    id=WebResource_biheader
    Wait And Click Element    id=start
    Unselect Frame
    Unselect Frame

Change GenralInfo
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//*[@id="{0c4db541-d4a7-7326-3e2b-65bdba4ca80c}_Expander"]
    #wait and click Element    css=div.ms-crm-Inline-Value.ms-crm-Inline-EditHintState
    click on element    xpath=//*[@id="jwl_prepaidflag"]/div
    Unselect Frame

Change CustomerInfo
    Sleep    10s
    Select IFrame    id=contentIFrame1
    Wait And Click Element    xpath=//*[@id="{6ea626a5-453c-3799-575b-94b9853ad9ca}_Expander"]
    Wait And Click Element    xpath=//*[@id="jwl_prepaidflag"]/div[1]/span
    Capture Page Screenshot
    Unselect Frame

SaveChanges
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Unselect Frame

SaveAndClickOnNextStage
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    click on element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@class='inline-block save-status-icon status-icon']//img[@id='savefooter_statuscontrol']
    Comment    wait and click element    xpath=//div[@id='stageAdvanceActionContainer']/div
    Wait And Click Element    //div[@id='stageAdvanceActionContainer']//img[@title='Move to next stage']
    Unselect Frame

PaymentAndSummaryPage
    [Arguments]    ${Screenshotpath}
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    wait and click element    id=savefooter_statuscontrol
    Wait and click element    css=div.stageActionText
    wait and click element    id=savefooter_statuscontrol
    Sleep    10s
    #wait and click element    id=stageAdvanceActionContainer
    #Sleep    10s
    Unselect Frame
    Screenshots    ${Screenshotpath}    PaymentPageSS

SubmitCustomer
    [Arguments]    ${Screenshotpath}
    wait for element    xpath=//li[@id='jwl_bi_newcustomer|NoRelationship|Form|jwl.jwl_bi_newcustomer.Button1.Button']//span[contains(text(),'Submit')]    40s
    Sleep    10s
    Choose Ok On Next Confirmation
    Run Keyword And Ignore Error    Click Element    xpath=//li[@id='jwl_bi_newcustomer|NoRelationship|Form|jwl.jwl_bi_newcustomer.Button1.Button']//span[contains(text(),'Submit')]    skip_ready=True

VerifyTheSubmittedCustomerStatus
    [Arguments]    ${CustomerName}    ${Screenshotpath}
    sleep    50s
    ${message}    Confirm Action
    log    ${message}
    sleep    7s
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    #Unselect Frame
    #wait for element    id=contentIFrame1
    #Select IFrame    id=contentIFrame1
    sleep    10s
    wait for element    xpath=//div[@id='header_statuscode']/div/span    40s
    sleep    10s
    Screenshots    ${Screenshotpath}    Order status
    ${StatusReason}    Get Text    xpath=//div[@id='header_statuscode']/div/span
    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    Log    Customer status is ${StatusReason}
    ...    ELSE    Log    Customer status is ${StatusReason}
    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    Add Result    PASS    UC1.2    After clicking on Submit and "OK" to popup message, header status should change to "SubmittedSuccessfully"    Header status is ${StatusReason} which is as expected.
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    1.2    After clicking on Submit and "OK" to popup message, header status should change to "SubmittedSuccessfully"    Header status is ${StatusReason} which is not as expected.
    ...    Verification completed
    Run Keyword If    '${message}'=='BI New Customer Submitted Successfully.'    Log    Customer created succesfully and has name =${CustomerName}
    ...    ELSE    Log    We got error while creating customer. Error detail is =${message}
    Run Keyword If    '${message}'=='BI New Customer Submitted Successfully.'    Add Result    PASS    UC1.2    After clicking on submit Popup with successfull submission message should come.    Popup message is ${message} which is as expected.
    ...    Verification completed
    ...    ELSE    Add Result    FAILD    1.2    After clicking on submit Popup with successfull submission message should come.    Popup message is ${message} which is not as expected.
    ...    Verification completed
    Unselect Frame
    ${CUSTOMER_ID}    Run Keyword If    '${StatusReason}'=='SubmittedSuccessfully'    GetCustomerNumFromCustName    ${Screenshotpath}    ${CustomerName}
    Write Excel    ${FilePath}/InputData.xls    0    1    0    ${CUSTOMER_ID}
    Write Excel    ${FilePath}/InputData.xls    0    1    1    ${CustomerName}
    Write Excel    ${FilePath}/InputData.xls    1    1    0    ${CUSTOMER_ID}

OpenSelectedSubscriptionBasedOnContractID
    Select IFrame    id=contentIFrame1
    Sleep    120s
    #Scroll Element Into View    id=subscriptions_header_h2
    #Wait And Click Element    id=subscriptions_header_h2
    wait and click element    //h2[@id='subscriptions_header_h2']
    Wait Until Angular Ready
    Wait And Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001338')]
    #Double Click Element    Xpath=.//*[@id='gridBodyTable']/tbody/tr[1]/td[5]/div
    #Wait And Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001338')]
    Double Click Element    Xpath= .//*[@id='gridBodyTable']//div[contains(string(),'CONTR0000001338')]
    Unselect Frame

UpdateAddress
    wait for element    id=commandContainer4    60    5
    wait for element    id=contentIFrame1
    Select IFrame    id=contentIFrame1
    Unselect Frame
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    wait for element    id=contentIFrame0
    Comment    Select IFrame    id=contentIFrame0
    Comment    Wait And Click Element    xpath=//div[@id='jwl_address1']/div    60    5
    Sleep    20s
    wait for element    //td[@id='jwl_address_line1_d']
    Wait And Click Element    //div[@id='jwl_address_line1']
    send text to element    id=jwl_address_line1_i    Istanbul
    Wait And Click Element    xpath=//div[@id='footer_statuscontrol']//img[@id='savefooter_statuscontrol']
    wait for element    xpath=//div[@id='footer_statuscontrol']//img[@id='savefooter_statuscontrol']
    Unselect Frame
    Sleep    15s
    Wait And Click Element    Xpath=//li[@id='jwl_bi_customeraddressupdate|NoRelationship|Form|jwl.jwl_bi_customeraddressupdate.Button1.Button']/span

DirectDebitOrder
    [Arguments]    ${Screenshotpath}
    sleep    2s
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    Wait Until Keyword Succeeds    40s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    id=crmRibbonManager    60    1
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    Wait Until Keyword Succeeds    20s    1s    Element Should Not Be Visible    //img[@id='loading']
    wait for element    //div[@id='jwl_bankname']
    Sleep    5s
    Focus    //div[@id='jwl_bankname']
    click on element    //div[@id='jwl_bankname']
    wait for element    //input[@id='jwl_bankname_ledit']
    click on element    //input[@id='jwl_bankname_ledit']
    wait for element    //img[@id='jwl_bankname_i']
    click on element    //img[@id='jwl_bankname_i']
    wait for element    //ul[@id='jwl_bankname_i_IMenu']//li[1]
    click on element    //ul[@id='jwl_bankname_i_IMenu']//li[1]
    wait for element    //div[@id='jwl_branch']
    click on element    //div[@id='jwl_branch']
    wait for element    //input[@id='jwl_branch_ledit']
    click on element    //input[@id='jwl_branch_ledit']
    wait for element    //img[@id='jwl_branch_i']
    click on element    //img[@id='jwl_branch_i']
    wait for element    //ul[@id='jwl_branch_i_IMenu']//li[1]
    click on element    //ul[@id='jwl_branch_i_IMenu']//li[1]
    Wait And Click Element    //div[@id='jwl_bankaccountnumber']
    ${BankAccNumber}    FakerLibrary.Random Number    7
    send text to element    //input[@id='jwl_bankaccountnumber_i']    ${BankAccNumber}
    Wait And Click Element    //div[@id='jwl_accounttype']
    Wait And Click Element    //select[@id='jwl_accounttype_i']/option[@title='Savings']
    Select IFrame    id=WebResource_Subcriptions
    Select Checkbox    //div[@id='grid']//table//tbody//tr[1]//td[1]//input
    Wait And Click Element    //button[@id='applytop']
    Unselect Frame
    Screenshots    ${Screenshotpath}    DDO Form
    Wait And Click Element    id=jwl_bi_directdebitorder|NoRelationship|Form|Mscrm.Form.jwl_bi_directdebitorder.Save
    sleep    7s
    Focus    //div[@id='crmRibbonManager']//span[contains(text(),'Submit ')]
    Wait And Click Element    //div[@id='crmRibbonManager']//span[contains(text(),'Submit ')]
    Sleep    20s
    Screenshots    ${Screenshotpath}    SubMsg
    ${SumMesg}    Confirm Action
    log    ${SumMesg}
    sleep    1m
    wait for element    id=contentIFrame0
    Select IFrame    id=contentIFrame0
    Comment    Unselect Frame
    Comment    wait for element    id=contentIFrame1
    Comment    Select IFrame    id=contentIFrame1
    wait for element    //div[@id='header_statuscode']/div[1]/span    40s
    ${StatusCode}    Get Text    //div[@id='header_statuscode']/div[1]/span
    ${match}    Set Variable    ${StatusCode}
    ${value}    Set Variable    Submitted
    ${match}    ${value}    Run Keyword And Ignore Error    Should Contain    ${match}    ${value}
    ${RETURNVALUE}    Set Variable If    '${match}' == 'PASS'    ${True}    ${False}
    log    ${RETURNVALUE}
    Run Keyword If    '${RETURNVALUE}'=='True'    Log    Direct Debit Order submitted succesfully and status is ${StatusCode}
    ...    ELSE    Log    Direct Debit Order not submitted succesfully and status is ${StatusReasonNewSub}
    Run Keyword If    '${RETURNVALUE}'=='True'    Add Result    PASS    UC7.27    DDO order should be submitted successfully    DDO order submitted successfully
    ...    DDO verification done
    ...    ELSE    Add Result    FAIL    UC7.27    DDO order should be submitted successfully    DDO order submitted successfully
    ...    DDO verification done
