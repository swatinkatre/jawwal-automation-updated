*** Settings ***
Suite Setup       SuiteStartKeyword
Library           ExtendedSelenium2Library    #Suite Setup | SuiteStartKeyword | #Suite Teardown | Close All Browsers | #Test Teardown | When Test Failed Take Screenhot | #Suite Teardown | Close All Browsers | #Test Teardown | When Test Failed Take Screenhot | #Test Setup | Go to CRM | # | #Test Teardown | When Test Failed Take Screenhot
Library           FakerLibrary
Library           JiraRobot
Library           requests
Library           ../CustomLibrary/Test.py
Library           ../CustomLibrary/UpdateExcelSheet.py
Library           ExcelLibrary
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt

*** Test Cases ***
UC01- Print Call Detail Statement
    [Tags]    5.27
    @{Call_Detail_Values}    Readvalue All    1    ${FilePath}/InputData.xls    CallDetail
    ${Call_detail_sub}    Set Variable    @{Call_Detail_Values}[0]
    ${CUSTOMER_Id}    Set Variable    @{Call_Detail_Values}[1]
    ${Call_detail_date1}    Set Variable    @{Call_Detail_Values}[2]
    ${Call_detail_date2}    Set Variable    @{Call_Detail_Values}[3]
    ${A}    Get Time    epoch
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC01}
    Log To Console    Search and open Customer with customer ID as input
    Sleep    10s
    Comment    OpenIndCustomerWithCustID    ${CUSTOMER_Id}    ${ScreenshotpathW4UAT_UC01}
    Log To Console    Opens Subscription from 360 view on the basis of Contract ID
    Comment    OpenSubWithContractID    ${Call_detail_sub}    ${ScreenshotpathW4UAT_UC01}
    GenerateaCallDetail    ${ScreenshotpathW4UAT_UC01}    ${Call_detail_date1}    ${Call_detail_date2}
    TimeExecutionCalculation    UC5.27    ${A}

UC02-Deactivate Subscription
    [Tags]    7.11
    #@{Deactivate_Values}    Readvalue All    1    ${FilePath}/InputData.xls    DeactSub
    ${Deactivatedcust}    Set Variable    CUST00000011
    ${Deactivatereason}    Set Variable    test
    ${DeactServiCe}    Set Variable    326732
    ${A}    Get Time    epoch
    Log To Console    Navigating to Customer List Window
    Sleep    30s
    Comment    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC02}
    Comment    SelectSubWithContractID    ${DeactServiCe}    ${ScreenshotpathW4UAT_UC02}
    Comment    NavigateToDeactivateContract
    Comment    OpenIndCustomerWithCustID    ${Deactivatedcust}    ${ScreenshotpathW4UAT_UC02}
    ${DeactivatedSubscriptionid}=    DeactivateTheSubscription    ${Deactivatereason}    ${DeactServiCe}
    Comment    Check if subscription is deactivated    ${DeactivatedSubscriptionid}
    TimeExecutionCalculation    UC7.11    ${A}

UC03- Freeze Subscription-MIX
    [Tags]    7.82
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    FreezeSubscription
    ${CUSTOMER_Id}    Set Variable    @{Values}[0]
    ${Subscription_Id}    Set Variable    @{Values}[1]
    ${Notes}    Set Variable    @{Values}[2]
    ${A}    Get Time    epoch
    Log To Console    Search and open Customer with customer ID as input
    Go To Spesific Individual Customer    ${CUSTOMER_Id}    ${ScreenshotpathW2UAT_UC17}
    Log To Console    Opens Subscription from 360 view on the basis of Contract ID
    OpenSubscription    ${Subscription_Id}    ${ScreenshotpathW2UAT_UC17}
    Log To Console    Navigates to Freeze Subscription Link and open BI
    OpenFreezeSubscription    ${ScreenshotpathW2UAT_UC17}
    Log to console    Navigates to Summary page by filling all mendatory fileds and submits the order
    FreezeSubscription    ${Subscription_Id}    ${Notes}
    TimeExecutionCalculation    UC7.32    ${A}

UC04- Family Package
    [Tags]    7.3
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    FamilyPackage
    ${CUSTOMER_Id}    Set Variable    @{Values}[0]
    ${FatherSubs_Id}    Set Variable    @{Values}[1]
    ${MotherSubs_Id}    Set Variable    @{Values}[2]
    ${ChildSubs_Id}    Set Variable    @{Values}[3]
    ${GovtID}    Set Variable    @{Values}[4]
    ${A}    Get Time    epoch
    Log To Console    Navigate to Individual Customer
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC04}
    Log To Console    Open Individual customer provided in Input Data
    Comment    Log    ${CustomerName}
    OpenIndCustomerWithCustID    ${CUSTOMER_Id}    ${ScreenshotpathW4UAT_UC04}
    Log To Console    Opens Subscription from 360 view on the basis of Contract ID
    OpenSubscription    ${FatherSubs_Id}    ${ScreenshotpathW4UAT_UC04}
    Log To Console    Navigates to Family Package Link and opens BI
    Open FamilyPackage
    Log To Console    Fills all mendator fields and submits the BI and validates it
    Submit FamilyPackage    ${GovtID}    ${FatherSubs_Id}    ${MotherSubs_Id}    ${ChildSubs_Id}    ${ScreenshotpathW4UAT_UC04}
    TimeExecutionCalculation    UC7.3    ${A}

UC05-Review Financial History
    [Tags]    5.13
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    ReviewFinHist
    ${CUSTOMER_ID}    Set Variable    @{Values}[0]
    ${From_date}    Set Variable    @{Values}[1]
    ${To_date}    Set Variable    @{Values}[2]
    ${A1}=    Get Time    epoch
    Log To Console    Navigating to Customer List Window
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC05}
    OpenIndCustomerWithCustID    ${CUSTOMER_ID}    ${ScreenshotpathW4UAT_UC05}
    ReviewFinHisAndVerify    ${From_date}    ${To_date}    ${ScreenshotpathW4UAT_UC05}
    TimeExecutionCalculation    UC5.13    ${A1}

UC06 - Account History
    [Tags]    5.16
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    AccHistory
    ${CUSTOMER_ID}    Set Variable    @{Values}[0]
    ${Contract_ID}    Set Variable    @{Values}[1]
    ${A1}=    Get Time    epoch
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC06}
    OpenIndCustomerWithCustID    ${CUSTOMER_ID}    ${ScreenshotpathW4UAT_UC06}
    OpenAccHistory
    SelectSubWithAnyStatus    ${Contract_ID}    ${ScreenshotpathW4UAT_UC06}
    TimeExecutionCalculation    UC5.16    ${A1}

UC07-Review and Print Invoice
    [Tags]    5.21
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    PrintInvoice
    ${CUSTOMER_Id}    Set Variable    @{Values}[0]
    ${Subscription_Id}    Set Variable    @{Values}[1]
    ${From_date}    Set Variable    @{Values}[2]
    ${End_date}    Set Variable    @{Values}[3]
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC07}
    OpenIndCustomerWithCustID    ${CUSTOMER_Id}    ${ScreenshotpathW4UAT_UC07}
    SelectSubWithContractID    ${Subscription_Id}    ${ScreenshotpathW4UAT_UC07}
    NavigateToReviewandPrint
    GenerateReviewPrint    ${From_date}    ${End_date}    ${ScreenshotpathW4UAT_UC07}

UC08- Change MSISDN
    [Tags]    7.1
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    MSISDNChange
    ${msisdn}    Set Variable    @{Values}[0]
    ${changed_msisdn}    Set Variable    @{Values}[1]
    ${A1}=    Get Time    epoch
    SearchSubscription    ${msisdn}    ${ScreenshotpathW4UAT_UC08}
    SelectMSISDN    ${ScreenshotpathW4UAT_UC08}
    NavigateToMSISDNChange
    ChangeMSISDN    ${changed_msisdn}    ${ScreenshotpathW4UAT_UC08}
    TimeExecutionCalculation    UC7.1    ${A1}

UC09- ReviewUsageHistory
    [Tags]    5.2
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    ReviewUsageHist

UC10-FCPayment
    [Tags]    8.15
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    FCPayment
    ${CUSTOMER_Id}    Set Variable    @{Values}[0]
    ${From_date}    Set Variable    @{Values}[1]
    ${To_date}    Set Variable    @{Values}[2]
    ${A1}=    Get Time    epoch
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC10}
    OpenIndCustomerWithCustID    ${CUSTOMER_Id}    ${ScreenshotpathW4UAT_UC10}
    SelectFCPayment
    InitiateFCPayment    ${ScreenshotpathW4UAT_UC10}    ${From_date}    ${To_date}
    TimeExecutionCalculation    UC8.15    ${A1}

UC11-FCWriteOff
    [Tags]    8.16
    @{Values}    Readvalue All    1    ${FilePath}/InputData.xls    FCWriteOff
    ${CUSTOMER_Id}    Set Variable    @{Values}[0]
    ${From_date}    Set Variable    @{Values}[1]
    ${To_date}    Set Variable    @{Values}[2]
    ${A1}=    Get Time    epoch
    GoToIndividualCustomersList    ${ScreenshotpathW4UAT_UC11}
    OpenIndCustomerWithCustID    ${CUSTOMER_Id}    ${ScreenshotpathW4UAT_UC11}
    SelectFCWriteoff
    InitiateFCWriteoff    ${ScreenshotpathW4UAT_UC11}    ${From_date}    ${To_date}
    TimeExecutionCalculation    UC8.16    ${A1}
