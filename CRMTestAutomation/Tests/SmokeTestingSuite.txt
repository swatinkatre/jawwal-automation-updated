*** Settings ***
Suite Teardown    Close All Browsers
Test Teardown     When Test Failed Take Screenhot    #Test Setup    Go to CRM    #
Library           ExtendedSelenium2Library    #Test Teardown    When Test Failed Take Screenhot
Library           Collections
Library           Process
Library           FakerLibrary
Resource          ../Keywords/AccountHistoryFacilities.robot
Resource          ../Keywords/BaseCustomerFacilities.robot
Resource          ../Keywords/CallBarringPassword.robot
Resource          ../Keywords/CustomerCreate.robot
Resource          ../Keywords/CustomerFacilities.robot
Resource          ../Keywords/CustomerSearch.robot
Resource          ../Keywords/Financial_TransactionFacilitied.robot
Resource          ../Keywords/MainFunctions.robot
Resource          ../Keywords/Navigator.robot
Resource          ../Keywords/SubscriptionFacilities.robot
Resource          ../Variable/configuration.txt

*** Test Cases ***
TC01 - Login to CRM
    [Tags]    Login To TCRM
    Go to CRM

TC02-CreateNewIndCustomer
    [Tags]    1.2 - Create Account
    Go to CRM
    Comment    Run Keyword And Continue On Failure    pendingemailwarning
    Run Keyword And Continue On Failure    closeexploreCMR
    GoToCustomerSearch
    VerifyCustomerSerachPage
    ${CustomerName}    NewCustomerBICustomerInfoPage
    NewCustomerBIAddressInfoPage
    ###Payment Page and summary page ###
    PaymentAndSummaryPage
    SubmitCustomer
    VerifyTheSubmittedCustomerStatus

TC03- CreateNewSubForIndCustomer
    [Tags]    7.12 - New Subscription for Existing Customer
    @{Values}    Readvalue All    1    ${FilePath}/New_Subscription.xlsx    ${SheetNameNewSubscription}
    ${CUSTOMER_ID}    Set Variable    @{Values}[0]
    ${Offer}    Set Variable    @{Values}[1]
    ${MSISDN}    Set Variable    @{Values}[2]
    ${SIM}    Set Variable    @{Values}[3]
    ${User_profile_Language}    Set Variable    @{Values}[4]
    ${Summary_Billing_Lan}    Set Variable    @{Values}[5]
    Log To Console    Loggin to TCRM Application
    Go to CRM
    #Run Keyword And Continue On Failure    pendingemailwarning
    Log To Console    Closing Explore CRM
    Run Keyword And Continue On Failure    closeexploreCMR
    Log To Console    Navigate to Individual Customer
    GoToIndividualCustomersList
    Log To Console    Open Individual customer provided in Input Data
    OpenIndCustomerWithCustID    ${CUSTOMER_ID}
    Log To Console    Start New subscription and select product given in Input data
    OpenNewSubAndSelectProduct    ${Offer}
    ####Configuration page
    Log To Console    Configuration Page
    Sleep    10s
    NewSubConfigurationPage
    #### Resource page ####
    Log To Console    Resource page
    NewSubResoursePage    ${MSISDN}    ${SIM}
    ###Addon page
    Log To Console    Add-On Page
    NewSubAddOnPage
    ## user profile
    Log To Console    User profile page
    NewSubUserProfilePage    ${User_profile_Language}
    ##Guarantee Page ###
    Log To Console    Guarantee page
    NewSubGuaranteePage
    ###Financial and payments
    Log To Console    Financial and payment page
    NewSubFinancialAndPaymentPage
    ##Summary page
    Log To Console    Summary Page, We will not save the order to retain resources
    NewSubSummaryPage    ${Summary_Billing_Lan}
