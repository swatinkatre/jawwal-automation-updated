*** Variables ***
${MainLink}       //*[@id="homeButtonImage"]
#${MainLink}      id=homeButtonImage
#${ServiceLink}    //img[@alt='Service']
${ServiceMenuTabButton}    id=CS
${ServiceToCorporate}    id=nav_accts
${ServiceToIndividual}    id=nav_contacts
${ServiceToCustomerSearch}    id=etel_customersearch
${ServiceToCases}    id=nav_cases
${SalesMenuTabButton}    id=SFA
${SalesToSubscriptions}    id=etel_subscription
${OpenAccountSummary}    xpath=//span[@title='Account Summary']
${OpenBillingAndPayment}    xpath=//span[@title='Billing&Payment Data']
${OpenIndividualCustomer}    xpath=//span[@title='Individual Customer']
${OpenCustomerData}    xpath=//span[@title='Customer Data']
${OpenMonetaryTransactions}    xpath=//span[@title='Monetary Transactions']
${OpenCustomer360}    xpath=//span[@title='Customer 360']
${OpenCustomer360BlackList}    xpath=//span[@title='Customer 360 - BlackList']
${OpenCorporateCustomerView}    xpath=//span[@title='Corporate Customer']
${OpenBillingForCorporate}    xpath=//span[@title='Billing']
${OpenAccountHistoryButtonCorp}    id=account|NoRelationship|Form|jwl.account.AccountHistory.Button
${OpenAccountHistoryButtonIndv}    id=contact|NoRelationship|Form|jwl.contact.AccountHistory.Button
${OpenAccountHistoryButtonSubs}    id=etel_subscription|NoRelationship|Form|jwl.etel_subscription.AccountHistory.Button
##OR for Cash on account refund##
${OR_SaveandClose_COA}    //li[contains(@title,'Save & Close')]
${OR_FinencialHistoryList_COA}    id=WebResource_financialhistorylist
${OR_Example_COA}    //div[@id='example']
${OR_CashOnAccountCount_COA}    //div[@id='grid']//table//tbody//tr//td[@class='k-detail-cell']
#${OR_CashOnAccountName_COA}    //div[@id='grid']//table//tbody//tr//td[@class='k-detail-cell']//table//tbody//tr[${Index}]//td[1]
#${OR_CashOnAccountRow_COA}    //div[@id='grid']//table//tbody//tr[${Index}]
${OR_NextStage_COA}    id=stageAdvanceActionContainer
${OR_Submit_COA}    //li[@id='jwl_bi_financialtransaction|NoRelationship|Form|jwl.jwl_bi_financialtransaction.Button2.Button']
${OR_CustomerName_COA}    //div[@id='header_process_jwl_customerid1']/div[1]/span
${OR_Actions_COA}    id=contact|NoRelationship|Form|jwl.contact.Actions.Button
${OR_RFH_COA}     id=review_financial_history_header_h2
${OR_RFH_Image_COA}    //div[@id='review_financial_history_header_image_div']//img
${OR_RFH_Frame_COA}    id=WebResource_review_financial_history
${OR_FromDate_COA}    id=FromDate
${OR_EndDate_COA}    id=EndDate
${OR_SearchButton_COA}    id=btnSearch
${OR_CashOnAccountEntry_COA}    //table[@id='financialHistGrid']//tbody//tr
${SalesToIndividual}    id=nav_conts
##OR for Document Upload on Orders and Payment page
${NotesLink}      //a[@title='NOTES']
${NotesBox}       //textarea[@title='Enter a note']
${AttachBttn}     //button[@title='Attach']
${FileUploadPath}    //input[@name='userFile']
${DoneTab}        //button[@title='Done']
